const fs = require('fs');
const path = require("path");
const {httpError} = require("../../../utils/http/response");
const ImageCollection = require("../../../models/image");
const {getComicBookPageFolderDir} = require("../../../utils/index/comicBook");
const {paramCheck, paramType} = require("../../../utils/paramUtil");




/**
 * Serve comic book link thumbnail
 * */
exports.serveComicBookPageImage = async (req, res) => {
  const pageType = req.params[0];
  const imgDataNumber = req.params[1];
  const imgName = req.params[2];


  if (
      !paramCheck(imgDataNumber, paramType.Number)
  ) return httpError(res, 403);


  const imageData = await ImageCollection.findImageWithNumber(imgDataNumber);


  const {file, status} = imageData;

  if (!status.publish) return httpError(res, 403);


  const pageFilePath = path.join(getComicBookPageFolderDir(), file.directory, file.name);



  /**
   * Start read stream
   * */
  const stream = fs.createReadStream(pageFilePath);

  stream.on("open", () => {
    res.set("Content-Type", file.mime);
    stream.pipe(res);
  })

  stream.on("error", (e) => {
    return httpError(res, 404);
  })
}