const { httpRespond, httpError } = require('../../../utils/http/response');
const ComicsCollection = require("../../../models/comics");
const {formatComicsDataList} = require("../../../utils/index/comics");
const _ = require("underscore");



/**
 * Comics translating page data
 * */
exports.comicsTranslatePageData = async (req, res) => {

    const [totalCount, itemsPerPage, pageToSearch, comicsList] = await ComicsCollection.findAllComicsDataMatchTitleIndexChar(undefined, true);

    if (!_.isEmpty(comicsList)) formatComicsDataList(comicsList);


    return httpRespond(res, 200, {
        user: req.userData,
        comicsList: comicsList,
        totalCount: totalCount,
        itemsPerPage: itemsPerPage,
        pageToSearch: pageToSearch
    })
}