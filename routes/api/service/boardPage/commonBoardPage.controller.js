const {paramCheck, paramType} = require("../../../../utils/paramUtil");
const { httpRespond, httpError } = require('../../../../utils/http/response');
const PostCollection = require("../../../../models/post");
const BoardNoticePostIndexCollection = require("../../../../models/boardNoticePostIndex");
const BoardHotPostListCollection = require("../../../../models/boardHotPostList");
const {POST_PER_PAGE, formatPostList, POST_TYPE} = require("../../../../utils/index/post");
const {SITE_CATEGORY_INDEX, SITE_BOARD_INDEX} = require("../../../../utils/index/siteIndex");
const {BOARD_POST_LIST_TYPE} = require("../../../../utils/index/board");
const _ = require("underscore");
const time = require("../../../../utils/time");
const {formatBoardHotPostList} = require("../../../../utils/index/boardHotPostList");
const {parseDocFromModel} = require("../../../../utils/mongoose");
const {checkBoardHotPostListDataIsExpired} = require("../../../../utils/index/boardHotPostList");
const {BOARD_HOT_POST_LIST_RANGE_TYPE} = require("../../../../utils/index/boardHotPostList");




/**
 * Board pages
 * */
exports.boardPage = async (req, res) => {
  const userData = req.userData;
  const {pageNumber} = req.query;
  let {category, board} = req.params;
  let {listType} = req.query;


  if (
      !Object.values(SITE_CATEGORY_INDEX).some(index => index.code===category) ||
      !Object.values(SITE_BOARD_INDEX).some(boardIndex => Object.values(boardIndex).some(index => index.code===board)) ||
      (pageNumber && !paramCheck(pageNumber, paramType.Number)) ||
      (listType && !Object.values(BOARD_POST_LIST_TYPE).some(type => type===listType))
  ) return httpError(res, 400);




  const {postList, totalPageCount} = await PostCollection.findPublishedPostListAndPageInfoWithPageNumber(pageNumber, category, board, POST_PER_PAGE, listType);

  const isBoardEmpty = _.isEmpty(postList) && !totalPageCount;


  const resFormat = {
    postList: [],
    searchedPage: 0,
    totalPageCount: 0
  }


  if (!isBoardEmpty && !_.isEmpty(postList)) {
    resFormat.postList = postList;
    resFormat.searchedPage = Number(pageNumber);
    resFormat.totalPageCount = totalPageCount;
  }
  /**
   * Find last page info if the given post page is empty due to prev page's post has been deleted
   * */
  else if (!isBoardEmpty) {
    const {postList, lastPageNumber, totalPageCount} = await PostCollection.findPublishedPostListAndPageInfoOfLastPage(category, board, POST_PER_PAGE);
    resFormat.postList = postList;
    resFormat.searchedPage = Number(lastPageNumber);
    resFormat.totalPageCount = totalPageCount;

  }

  await formatPostList(req, resFormat.postList);



  /**
   * Find board notice posts
   * */
  const boardNoticePosts = await BoardNoticePostIndexCollection.findBoardNoticePostList(category, board);



  /**
   * Find/Create board's hot post list
   * */
  let boardHotPostListData_24h = await BoardHotPostListCollection.findLatestBoardHotPostListData(category, board, BOARD_HOT_POST_LIST_RANGE_TYPE["24h"]);
  let boardHotPostListData_7d = await BoardHotPostListCollection.findLatestBoardHotPostListData(category, board, BOARD_HOT_POST_LIST_RANGE_TYPE["7d"]);
  boardHotPostListData_24h = parseDocFromModel(boardHotPostListData_24h);
  boardHotPostListData_7d = parseDocFromModel(boardHotPostListData_7d);

  if (
      !boardHotPostListData_24h ||
      (boardHotPostListData_24h && checkBoardHotPostListDataIsExpired(boardHotPostListData_24h, BOARD_HOT_POST_LIST_RANGE_TYPE["24h"]))
  ) {
    boardHotPostListData_24h = await BoardHotPostListCollection.createBoardHotPostListData(category, board, BOARD_HOT_POST_LIST_RANGE_TYPE["24h"], 15, 3, undefined, 24);
    boardHotPostListData_24h = parseDocFromModel(boardHotPostListData_24h);
  }

  if (
      !boardHotPostListData_7d ||
      (boardHotPostListData_7d && checkBoardHotPostListDataIsExpired(boardHotPostListData_7d, BOARD_HOT_POST_LIST_RANGE_TYPE["7d"]))
  ) {
    boardHotPostListData_7d = await BoardHotPostListCollection.createBoardHotPostListData(category, board, BOARD_HOT_POST_LIST_RANGE_TYPE["7d"], 9, 3, time.todayStart(), 24*7);
    boardHotPostListData_7d = parseDocFromModel(boardHotPostListData_7d);
  }


  let boardHotPostList_24h = boardHotPostListData_24h && _.isEmpty(boardHotPostListData_24h.postList)? []: boardHotPostListData_24h.postList;
  let boardHotPostList_7d = boardHotPostListData_7d && _.isEmpty(boardHotPostListData_7d.postList)? []: boardHotPostListData_7d.postList;
  boardHotPostList_24h = boardHotPostList_24h.map(post => parseDocFromModel(post));
  boardHotPostList_7d = boardHotPostList_7d.map(post => parseDocFromModel(post));


  if (!_.isEmpty(boardHotPostList_24h)) formatBoardHotPostList(boardHotPostList_24h);
  if (!_.isEmpty(boardHotPostList_7d)) formatBoardHotPostList(boardHotPostList_7d);




  return httpRespond(res, 200, {
    user: userData,
    board: {
      hotPostListInfo: {
        "24h": boardHotPostList_24h,
        "7d": boardHotPostList_7d
      },
      noticePostList: boardNoticePosts,
      postList: resFormat.postList,
      searchedPage: resFormat.searchedPage,
      totalPageCount: resFormat.totalPageCount,
      postPerPage: POST_PER_PAGE
    }
  });
}
