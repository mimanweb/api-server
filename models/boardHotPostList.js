const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PostCollection = require("./post");
const _ = require("underscore");
const time = require("../utils/time");




const BoardHotPostListSchema = new Schema({
    index: {
        category: String,
        board: String,
        rangeType: String,
    },
    postNumberList: [Number],
    time: {
        created: Date
    }
})


BoardHotPostListSchema.index({"time.created": -1});




/**
 * Find latest post list data
 * */
BoardHotPostListSchema.statics.findLatestBoardHotPostListData = async function (category, board, rangeType) {
    const aggregationRes = await this.aggregate([
        {
            '$match': {
                'index.category': category,
                'index.board': board,
                'index.rangeType': rangeType
            }
        }, {
            '$sort': {
                'time.created': -1
            }
        }, {
            '$limit': 1
        }, {
            '$unwind': {
                'path': '$postNumberList',
                preserveNullAndEmptyArrays: true
            }
        }, {
            '$lookup': {
                'from': 'posts',
                'let': {
                    'post_number': '$postNumberList'
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$and': [
                                    {
                                        '$eq': [
                                            '$number', '$$post_number'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$status', 'PUBLISH'
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                ],
                'as': 'post'
            }
        }, {
            '$project': {
                'post': {
                    '$arrayElemAt': [
                        '$post', 0
                    ]
                },
                time: 1,
                index: 1
            }
        }, {
            '$group': {
                '_id': '$_id',
                'postList': {
                    '$push': '$post'
                },
                time: {$first: "$time"},
                index: {$first: "$index"},
            }
        }
    ])

    return _.isEmpty(aggregationRes)? null: aggregationRes.shift();
}



/**
 * Create post list data
 * */
BoardHotPostListSchema.statics.createBoardHotPostListData = async function (category, board, rangeType, countToQuery, minUpvote, rangeStartTime, timeRangeInHour) {

    let postNumberList;

    const searchStartInitHourTime = time.getInitHourTime(rangeStartTime? rangeStartTime: time.now());

    const searchBaseTime = time.getHourSubtractedTime(searchStartInitHourTime, timeRangeInHour);


    let postList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(category, board, countToQuery, minUpvote, rangeStartTime? searchStartInitHourTime: undefined, searchBaseTime);

    /**
     * Prev search list is not enough,
     * Search rest from the rest of the times data
     * */
    if (
        _.isEmpty(postList) ||
        (!_.isEmpty(postList) && postList.length!==countToQuery)
    ) {
        const listFromAllTime = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(category, board, countToQuery-postList.length, minUpvote, searchBaseTime, undefined);

        if (!_.isEmpty(listFromAllTime)) postList = postList.concat(listFromAllTime);
    }


    postNumberList = _.isEmpty(postList)? []: postList.map(model => model._doc.number);
    // Shuffle the list
    postNumberList = _.shuffle(postNumberList);

    const res = await this.create({
        "index.category": category,
        "index.board": board,
        "index.rangeType": rangeType,
        "postNumberList": postNumberList,
        "time.created": time.now()
    })

    postList = _.isEmpty(postNumberList)? []: await PostCollection.findAllPostMatchPostNumberList(postNumberList);

    res._doc.postList = postList;

    return res;
}





module.exports = mongoose.model('board_hot_post_list', BoardHotPostListSchema);