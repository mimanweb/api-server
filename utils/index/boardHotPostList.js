const {parseDocFromModel} = require("../mongoose");
const time = require("../time");
const cheerio = require('cheerio')
const _ = require("underscore");


const BOARD_HOT_POST_LIST_RANGE_TYPE = {
    "24h": "24h",
    "7d": "7d"
}


const BOARD_HOT_POST_LIST_UPDATE_INTERVAL = {
    "24h": 1,
    "7d": "start of the day"
}


exports.checkBoardHotPostListDataIsExpired = (data, rangeType) => {
    const doc = parseDocFromModel(data);

    let baseTime;
    let includeSameInitHour;


    switch (rangeType) {
        case BOARD_HOT_POST_LIST_RANGE_TYPE["24h"]:
            const initHour = time.getInitHourTime(time.now());

            baseTime = time.getHourSubtractedTime(initHour, BOARD_HOT_POST_LIST_UPDATE_INTERVAL["24h"]);
            includeSameInitHour = true;
            break;
        case BOARD_HOT_POST_LIST_RANGE_TYPE["7d"]:
            baseTime = time.todayStart();
            break;
    }


    const dataTime = time.getInitHourTime(doc.time.created);

    return time.compare(baseTime, dataTime, includeSameInitHour);
}


const formatBoardHotPost = (post) => {
    const $ = cheerio.load(post.content);

    const textContent = $.text();

    post.textContent = !/[\S]/g.test(textContent)? "": textContent;

    if (!_.isEmpty(post.images)) {
        post.thumbnail = post.images.shift();
        post.isImagePost = true;
    } else {
        post.thumbnail = null;
        post.isImagePost = false;
    }

    delete post.content;
    delete post.phrase;
    delete post.owner;


    return post;
}



const formatBoardHotPostList = (list) => {
    list.forEach(post => formatBoardHotPost(post));
}




exports.BOARD_HOT_POST_LIST_RANGE_TYPE = BOARD_HOT_POST_LIST_RANGE_TYPE;
exports.BOARD_HOT_POST_LIST_UPDATE_INTERVAL = BOARD_HOT_POST_LIST_UPDATE_INTERVAL;
exports.formatBoardHotPostList = formatBoardHotPostList;





