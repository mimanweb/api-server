const express = require('express');
const router = express.Router();
const page = require('./pagedata.controller');
const communityBoard = require("./boardPage/commonBoardPage.controller");
const comicsPage = require("./comicsPage.controller");
const comicBookPage = require("./comicBookPage.controller");
const comicBookEditPage = require("./comicBook/editPage.controller");
const comicsTranslatePage = require("./comicsTranslatePage.controller");


const {verifyLogin} = require('../../../utils/auth/login');


router.use(verifyLogin);

router.get('/data/homepage', page.homePage);
router.get("/data/post", page.postPage);
router.get("/data/post/edit", page.postEditPage);
router.get("/data/comics", comicsPage.comicsPage);
router.get("/data/comics/translate", comicsTranslatePage.comicsTranslatePageData);

router.get("/data/board/:category/:board", communityBoard.boardPage);

router.get("/data/comic-book/write", comicBookPage.comicBookEditorPage);
router.get("/data/comic-book/edit", comicBookEditPage.comicBookEditPage);
router.get("/data/comic-book", comicBookPage.comicBookPage);


module.exports = router;
