const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const BoardNoticePostIndexSchema = new Schema({
  category: String,
  board: String,
  list: [
    {
      postNumber: Number,
      order: Number,
      showAlways: Boolean
    }
  ]
})



BoardNoticePostIndexSchema.index({category: 1, board: 1});




BoardNoticePostIndexSchema.statics.findIndexData = async function (category, board) {
  return await this.findOne({category: category, board: board});
}


BoardNoticePostIndexSchema.statics.findIndexDataWithPostNumber = async function (category, board, postNumber) {
  return await this.findOne({category: category, board: board, "list.postNumber": postNumber});
}


BoardNoticePostIndexSchema.statics.findBoardNoticePostList = async function (category, board) {
  return await this.aggregate([
    {
      '$match': {
        'category': category,
        'board': board
      }
    }, {
      '$unwind': {
        'path': '$list'
      }
    }, {
      '$project': {
        'postNumber': '$list.postNumber',
        'showAlways': '$list.showAlways'
      }
    }, {
      '$lookup': {
        from: 'posts',
        let: {post_number: "$postNumber"},
        pipeline: [
          {$match: {$expr: {
                $and: [
                  {$eq: ["$number", "$$post_number"]},
                  {$eq: ["$index.category", category]},
                  {$eq: ["$index.board", board]}
                ]
              }}}
        ],
        as: 'post'
      }
    }, {
      '$unwind': {
        'path': '$post'
      }
    }, {
      '$project': {
        'number': "$post.number",
        'showAlways': 1,
        "index": "$post.index",
        "owner.userId": "$post.owner.userId",
        "owner.nickname": "$post.owner.nickname",
        'title': '$post.title',
        'time': '$post.time',
        "record": "$post.record"
      }
    }
  ])
}


BoardNoticePostIndexSchema.statics.checkAndCreateIndex = async function (category, board) {
  const index = await this.findOne({category: category, board: board});

  if (!index) {
    await this.create({
      category: category,
      board: board,
      list: []
    })
  }
}



BoardNoticePostIndexSchema.methods.replacePostList = async function (postList) {
  await this.updateOne({
    list: postList
  })
}



module.exports = mongoose.model('board_notice_post_index', BoardNoticePostIndexSchema);