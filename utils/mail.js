const nodemailer = require("nodemailer");



exports.send = async (from='"MIMANWEB" <master@mimanweb.com>', to, subject, text, html) => {
    const transporter = nodemailer.createTransport({
        host: "mail.privateemail.com",
        port: 465,
        secure: true,
        auth: {
            user: process.env["MAIL_ACCOUNT"],
            pass: process.env["MAIL_PASSWORD"],
        },
    });

    try {
        return await transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            text: text,
            html: html,
        });
    } catch (e) {
        return false;
    }

}

