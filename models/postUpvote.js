const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");


const PostUpvoteSchema = new Schema({
  postNumber: Number,
  owner: {
    userId: {type: String, index: true, sparse: true},
    ipAddress: {type: String, index: true, sparse: true},
  },
  isUpvoted: {type: Boolean, default: true},
  time: {
    created: Date,
    updated: Date
  }
});


PostUpvoteSchema.index({"postNumber": -1, "time.created": -1});




PostUpvoteSchema.statics.createRegUserRecord = async function (postNumber, userId) {
  return await this.create({
    "postNumber": postNumber,
    "owner.userId": userId,
    "time.created": time.now()
  })
}


PostUpvoteSchema.statics.createUnregUserRecord = async function (postNumber, ipAddress) {
  return await this.create({
    "postNumber": postNumber,
    "owner.ipAddress": ipAddress,
    "time.created": time.now()
  })
}


PostUpvoteSchema.statics.findRegUserRecord = async function (postNumber, userId) {
  return await this.findOne({
    "postNumber": postNumber,
    "owner.userId": userId,
    "owner.ipAddress": {$exists: false}
  }).sort({
    "postNumber": -1,
    "time.created": -1
  });
}


PostUpvoteSchema.statics.findUnregUserRecord = async function (postNumber, ipAddress) {
  return await this.findOne({
    "postNumber" :postNumber,
    "owner.userid": {$exists: false},
    "owner.ipAddress": ipAddress
  }).sort({
    "postNumber": -1,
    "time.created": -1
  });
}


PostUpvoteSchema.methods.setIncreased = async function () {
  await this.updateOne({
    "isUpvoted": true,
    "time.updated": time.now()
  })
}


PostUpvoteSchema.methods.setDecreased = async function () {
  await this.updateOne({
    "isUpvoted": false,
    "time.updated": time.now()
  })
}



module.exports = mongoose.model('post_upvote', PostUpvoteSchema);
