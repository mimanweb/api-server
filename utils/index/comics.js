const path = require("path");
const {mkdirIfNone} = require("../file");
const {readDir} = require("../file");
const _ = require("underscore");
const {getLastNumberOfNumItemsInDir} = require("../file");
const {writeFile} = require("../file");
const {resizeImgBuffer} = require("../image");


const COMICS_SEARCH_ITEMS_PER_PAGE = 10;


const COMICS_SEARCH_KOR_CHAR_INDEXES = [
    {key: "ㄱ", pattern: "가-깋"},
    {key: "ㄲ", pattern: "까-낗"},
    {key: "ㄴ", pattern: "나-닣"},
    {key: "ㄷ", pattern: "다-딯"},
    {key: "ㄸ", pattern: "따-띻"},
    {key: "ㄹ", pattern: "라-맇"},
    {key: "ㅁ", pattern: "마-밓"},
    {key: "ㅂ", pattern: "바-빟"},
    {key: "ㅃ", pattern: "빠-삫"},
    {key: "ㅅ", pattern: "사-싷"},
    {key: "ㅆ", pattern: "싸-앃"},
    {key: "ㅇ", pattern: "아-잏"},
    {key: "ㅈ", pattern: "자-짛"},
    {key: "ㅉ", pattern: "짜-찧"},
    {key: "ㅊ", pattern: "차-칳"},
    {key: "ㅋ", pattern: "카-킿"},
    {key: "ㅌ", pattern: "타-팋"},
    {key: "ㅍ", pattern: "파-핗"},
    {key: "ㅎ", pattern: "하-힣"},
]


const COMICS_STATUS = {
    DELETED: "DELETED",
    APPROVED: "APPROVED",
    AWAITING_APPROVAL: "AWAITING_APPROVAL",
}

const COMICS_PUBLISH_STATUS = {
    ON_PROGRESS: "ON_PROGRESS",
    COMPLETE: "COMPLETE"
}


const MAX_COMICS_COVER_FOLDER_ITEMS_COUNT = 1000;




const convertComicsTitleIntoDataId = (title) => {
    return [...title.matchAll(/[\w]+/g)].join("-").toLowerCase();
}


const generateComicsLinkIndexNumber = (dataSeq) => {
    return dataSeq;
    // if (dataSeq < 10) return "00" + dataSeq;
    // else if (dataSeq > 10 && dataSeq < 100) return "0" + dataSeq;
}


const formatComicsDataList = (list) => {
    list.forEach(comics => {
        comics._doc.linkIndex = generateComicsLinkIndexNumber(comics.number);
    })
}



/**
 * DIRECTORIES
 * */
const getComicsFolderDir = () => {
    return path.join(process.env["IMAGES_DIR"], process.env["COMICS_FOLDER"]);
}

const getComicsCoverFolderDir = () => {
    return path.join(getComicsFolderDir(), process.env["COMICS_COVER_IMAGE_FOLDER"]);
}

const getLastComicsCoverFolderNumber = async () => {
    const comicsCoverFolderDir = getComicsCoverFolderDir();

    await mkdirIfNone(comicsCoverFolderDir);

    return await getLastNumberOfNumItemsInDir(comicsCoverFolderDir);
}


const getAvailableComicsCoverFolder = async () => {
    const comicsCoverFolderDir = getComicsCoverFolderDir();
    const lastComicsCoverFolder = await getLastComicsCoverFolderNumber();


    /**
     * No folder yet
     * Create one
     * */
    if (lastComicsCoverFolder===0) {
        const newFolder = (lastComicsCoverFolder+1).toString();
        await mkdirIfNone(path.join(comicsCoverFolderDir, newFolder));

        return [newFolder, 0];
    } else {
        const lastNumberInFolder = await getLastNumberOfNumItemsInDir(path.join(comicsCoverFolderDir, lastComicsCoverFolder.toString()), true);

        if (lastNumberInFolder<MAX_COMICS_COVER_FOLDER_ITEMS_COUNT) {
            return [lastComicsCoverFolder.toString(), lastNumberInFolder];
        }
        /**
         * Max item count exceeded
         * Create new folder
         * */
        else {
            const newFolder = (lastComicsCoverFolder+1).toString();
            await mkdirIfNone(path.join(comicsCoverFolderDir, newFolder));

            return [newFolder, 0];
        }
    }
}


const saveComicsCoverFile = async (file, size) => {
    const coverFolderDir = getComicsCoverFolderDir();
    const [folder, lastNumberInFolder] = await getAvailableComicsCoverFolder();
    const fileExt = file.mimetype.split("/").pop();
    const resizedBuffer = await resizeImgBuffer(file.buffer, size);

    const fileName = `${lastNumberInFolder+1}.${fileExt}`;

    await writeFile(path.join(coverFolderDir, folder, fileName), resizedBuffer);

    return [folder, fileName];
}



exports.COMICS_STATUS = COMICS_STATUS;
exports.COMICS_PUBLISH_STATUS = COMICS_PUBLISH_STATUS;
exports.COMICS_SEARCH_ITEMS_PER_PAGE = COMICS_SEARCH_ITEMS_PER_PAGE;
exports.COMICS_SEARCH_KOR_CHAR_INDEXES = COMICS_SEARCH_KOR_CHAR_INDEXES;

exports.getComicsFolderDir = getComicsFolderDir;
exports.formatComicsDataList = formatComicsDataList;
exports.convertComicsTitleIntoDataId = convertComicsTitleIntoDataId;
exports.getComicsCoverFolderDir = getComicsCoverFolderDir;
exports.getAvailableComicsCoverFolder = getAvailableComicsCoverFolder;
exports.saveComicsCoverFile = saveComicsCoverFile;

