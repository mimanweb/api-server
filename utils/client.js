const { v4: uuid_v4 } = require('uuid');



/**
 * Check if request has DeviceUUID(uuid v4) in session.cookie which previously issues and saved in the client browser
 * Issue a new one if none
 * */
exports.setClientDeviceUUID = async (req, res, next) => {
    if (!req.session.UUID) req.session.UUID = uuid_v4();
    return next();
}


/**
 * Read client ip address from X-Forwarded-For header
 * And set it in req.clientIp
 * */
exports.setClientIp = async (req, res, next) => {
    const xForwardedFor = req.headers["x-forwarded-for"];

    if (xForwardedFor) req.clientIp = xForwardedFor.split(", ")[0];

    // Local development setting
    if (!xForwardedFor && process.env.SERVER_ENV==="local") req.clientIp = "127.0.0.1";

    return next();
}


/**
 * Set client init data
 * */
exports.setClientInitData = async (req, res, next) => {
    req.isRegUser = undefined;
    req.userData = {};

    return next();
}


/**
 * Trim ip address, first two fields
 * */
exports.trimIp = (ipAddress) => {
    const match = /([\d]+.[\d]+)./.exec(ipAddress);

    return match[1];
}