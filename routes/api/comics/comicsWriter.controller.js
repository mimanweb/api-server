const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const ComicsWriterCollection = require("../../../models/comicsWriter");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const mongoose = require("mongoose");



/**
 * Find all comics writers data
 * Regex match word
 * */
exports.findComicsWriters = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {textToSearch} = req.query;

    if (!paramCheck(textToSearch)) return httpError(res, 400);


    const writerList = await ComicsWriterCollection.findAllComicsWritersMatchWord(textToSearch);

    if (_.isEmpty(writerList)) return httpError(res, 404);


    return httpRespond(res, 200, {writerList: writerList});
}


/**
 * Add comics writer info
 * */
exports.addComicsWriter = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, comicsWriterDataId} = req.body;

    if (
      !paramCheck(comicsDataId) ||
      !paramCheck(comicsWriterDataId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    const comicsWriterList = comicsData.writers;
    const comicsWriterToAdd = mongoose.Types.ObjectId(comicsWriterDataId);

    if (comicsWriterList.includes(comicsWriterToAdd)) return httpError(res, 400);

    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    comicsWriterList.push(comicsWriterToAdd);

    await ComicsCollection.updateComicsWriters(comicsData._id, comicsWriterList);


    return httpRespond(res, 200);
}


/**
 * Add new comics writer info
 * */
exports.addNewComicsWriter = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, writerName} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(writerName) ||
        (writerName && !/[\w]/g.test(writerName))
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    const newComicsWriterData = await ComicsWriterCollection.createComicsWriterData(writerName);
    const comicsWriterList = comicsData.writers;

    comicsWriterList.push(newComicsWriterData._id);

    await ComicsCollection.updateComicsWriters(comicsData._id, comicsWriterList);


    return httpRespond(res, 200);
}