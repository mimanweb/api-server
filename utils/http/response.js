/**
 * http 응답 정리
 * */

exports.httpRespond = (res, status=200, data) => {
    if (!res) throw Error("no response object given")
    res.statusCode = status;

    return data? res.send(data): res.end();
};

exports.httpError = (res, status=500, code=undefined, message=undefined, data=undefined) => {
    if (!res) return Error("no response object given")
    if (!status) return Error("no status code given")
    res.statusCode = status;

    if (!code && !message && !data) {
        return res.end();
    } else {
        return res.send({
            code: code,
            message: message,
            data: data
        })
    }
};