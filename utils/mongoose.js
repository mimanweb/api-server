const mongoose = require("mongoose");


exports.parseDataId = (dataId) => {
  return typeof dataId==="string"? mongoose.Types.ObjectId(dataId): dataId;
}


exports.parseDocFromModel = (model) => {
  return !model? null: model && model._doc? model._doc: model;
}