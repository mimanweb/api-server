/**
 * Set default server wide routers
 */
const testRouter = require('./api/test/index');
const authRouter = require('./api/auth/index');
const userRouter = require('./api/user/index');
const serviceDataRouter = require('./api/service/index');
const mailRouter = require("./api/mail/index");
const imageRouter = require("./api/image/index");
const fileRouter = require("./api/file/index");
const PostRouter = require("./api/post/index");
const CommentRouter = require("./api/comment/index");
const ComicsRouter = require("./api/comics/index");
const ComicBookRouter = require("./api/comicBook/index");


const {setClientDeviceUUID, setClientIp, setClientInitData} = require("../utils/client");
const {saveUnregUserVisitLog} = require("../utils/log");


exports.setRoutes = (app) => {
  app.use(setClientDeviceUUID);
  app.use(setClientIp);
  app.use(setClientInitData);
  app.use(saveUnregUserVisitLog);

  app.use('/test', testRouter);
  app.use('/auth', authRouter);
  app.use('/user', userRouter);
  app.use('/service', serviceDataRouter);
  app.use('/mail', mailRouter);
  app.use('/image', imageRouter);
  app.use('/file', fileRouter);
  app.use('/post', PostRouter);
  app.use('/comment', CommentRouter);
  app.use('/comics', ComicsRouter);
  app.use('/comic-book', ComicBookRouter);

};