const express = require('express');
const router = express.Router();
const controller = require('./auth.controller');


const {verifyLogin} = require('../../../utils/auth/login');


router.get('/login/verify', verifyLogin, controller.verify);
router.post('/login', controller.login);
router.post('/logout', controller.logout);


module.exports = router;
