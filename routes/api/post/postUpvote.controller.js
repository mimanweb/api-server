const PostCollection = require('../../../models/post');
const PostUpvoteCollection = require('../../../models/postUpvote');
const {httpRespond, httpError} = require('../../../utils/http/response');
const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {POST_STATUS} = require("../../../utils/index/post");



exports.updatePostUpvoteCount = async (req, res) => {
    const {postNumber, category, board} = req.body;
    const ipAddress = req.clientIp;
    const {id: userId} = req.userData;


    if (
        !paramCheck(postNumber, paramType.Number) ||
        !paramCheck(category, paramType.String) ||
        !paramCheck(board, paramType.String)
    ) return httpError(res, 400);


    /**
     * Find post data
     * */
    const postData = await PostCollection.findPostWithPostNumber(postNumber, category, board, POST_STATUS.PUBLISH);


    if (!postData || (postData && postData.status!==POST_STATUS.PUBLISH)) return httpError(res, 404);

    /**
     * Find post upvote date
     * */
    let postUpvoteData;
    let isIncreased;
    let updatedUpvoteCount;

    if (req.isRegUser) {
        postUpvoteData = await PostUpvoteCollection.findRegUserRecord(postNumber, userId);
    } else {
        postUpvoteData = await PostUpvoteCollection.findUnregUserRecord(postNumber, ipAddress);
    }

    /**
     * No data, create one and add 1 record in Post data
     * */
    if (!postUpvoteData) {
        if (req.isRegUser) {
            await PostUpvoteCollection.createRegUserRecord(postNumber, userId);
        } else {
            await PostUpvoteCollection.createUnregUserRecord(postNumber, ipAddress);
        }

        updatedUpvoteCount = await postData.increaseUpvoteCount();
        isIncreased = true;
    }
    /**
     * Data exists and previously increased one,
     * Set to decreased and sub 1 record in Post data
     * */
    else if (postUpvoteData && postUpvoteData.isUpvoted) {
        await postUpvoteData.setDecreased();
        updatedUpvoteCount = await postData.decreaseUpvoteCount();
        isIncreased = false;
    }
    /**
     * Data exists and previously decreased one,
     * Set to increased and add 1 record in Post data
     * */
    else if (postUpvoteData && !postUpvoteData.isUpvoted) {
        await postUpvoteData.setIncreased();
        updatedUpvoteCount = await postData.increaseUpvoteCount();
        isIncreased = true;
    }



    return httpRespond(res, 200, {isIncreased: isIncreased, upvoteCount: updatedUpvoteCount});
}