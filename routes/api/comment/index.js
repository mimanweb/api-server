const express = require('express');
const router = express.Router();
const comment = require("./comment.controller");
const commentUpvote = require("./commnetUpvote.controller");
const multer  = require('multer')
const upload = multer()


const {verifyLogin} = require('../../../utils/auth/login');


router.use(verifyLogin);


router.get("/page", comment.findCommentsInSpecificPage);
router.post("/", upload.single("image"), comment.createComment);
router.post("/reply", upload.single("image"), comment.createComment);
router.post("/upvote", commentUpvote.updateCommentUpvote);
router.delete("/", comment.deleteComment);




module.exports = router;