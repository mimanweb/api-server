const express = require('express');
const router = express.Router();
const comics = require("./comics.controller");
const comicsWriter = require("./comicsWriter.controller");
const comicsArtist = require("./comicsArtist.controller");
const comicsPublisher = require("./comicsPublisher.controller");
const comicsGenre = require("./comicsGenre.controller");
const comicsCoverImage = require("./comicsCoverImage.controller");
const comicsContentDescriptor = require("./comicsContentDescriptor.controller");
const comicsSummary = require("./comicsSummary.controller");
const multer  = require('multer');
const upload = multer();


const {verifyLogin} = require('../../../utils/auth/login');


router.use(verifyLogin);


router.post("/", comics.createComics);
router.get("/match/title", comics.findComicsDataWithTitle);
router.get("/match/title/index/char", comics.findComicsDataMatchTitleIndexChar);

router.get("/recent/read/list", comics.findAllComicsAndLatestUpdateInfo)


router.post("/writer/new", comicsWriter.addNewComicsWriter);
router.post("/writer", comicsWriter.addComicsWriter);
router.get("/writers/match/text", comicsWriter.findComicsWriters);


router.post("/artist/new", comicsArtist.addNewComicsArtist);
router.post("/artist", comicsArtist.addComicsArtist);
router.get("/artists/match/text", comicsArtist.findComicsArtists);


router.put("/publisher/new", comicsPublisher.addNewComicsPublisher);
router.put("/publisher", comicsPublisher.updateComicsPublisher);
router.get("/publishers/match/text", comicsPublisher.findComicsPublishers);


router.get("/genres", comicsGenre.findAllComicsGenres);
router.post("/genre", comicsGenre.addComicsGenre);


router.get("/content-descriptors", comicsContentDescriptor.findAllComicsContentDescriptors);
router.post("/content-descriptor", comicsContentDescriptor.addComicsContentDescriptor);


router.post("/cover-image", upload.single("image"), comicsCoverImage.updateComicsCoverImage);


router.put("/summary", comicsSummary.updateComicsSummary);


module.exports = router;