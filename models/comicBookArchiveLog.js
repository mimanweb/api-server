const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");
const {COMIC_BOOK_IMAGE_STATUS} = require("../utils/index/comicBook");
const {COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_STATUS} = require("../utils/index/comicBook");
const {parseDataId} = require("../utils/mongoose");
const _ = require("underscore");






const ComicBookArchiveLogSchema = new Schema({
    status: String,
    owner: {
        userId: String,
        nickname: String,
    },
    index: {
        comicBookDataId: Schema.Types.ObjectID,
        postDataId: Schema.Types.ObjectID,
        comicsDataId: Schema.Types.ObjectID,
        contentType: String,
        isTitle: Boolean,
        isShortStory: Boolean,
        isVolume: Boolean,
        volume: Number,
        issue: Number,
        shortStoryNumber: Number
    },
    record: {
        viewCount: Number
    },
    title: String,
    pages: [
        {
            status: String,
            pageNumber: Number,
            pageType: String,
            pairNumber: Number,
            imgId: {
                small: String,
                medium: String,
                large: String,
                xLarge: String,
                xxLarge: String
            }
        }
    ],
    link: String,
    linkThumbnails: [
        {
            status: String,
            imgId: {
                small: String,
                medium: String,
                large: String,
                xLarge: String,
                xxLarge: String
            }
        }
    ],
    summary: String,
    time: {
        created: Date,
        updated: Date,
        publish: Date,
        deleted: Date,
        archived: Date
    }
})




ComicBookArchiveLogSchema.statics.archiveComicBookData = async function (comicBookData) {
    const doc = comicBookData._doc? _.clone(comicBookData._doc): _.clone(comicBookData);

    doc.time.archived = time.now();
    doc.index.comicBookDataId = doc._id;

    delete doc._id;

    await this.create(doc);
}



module.exports = mongoose.model('comic_book_archive_log', ComicBookArchiveLogSchema);