const {paramCheck} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const ComicsCollection = require("../../../models/comics");



/**
 * Update comics summary info
 * */
exports.updateComicsSummary = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const userId = req.userData.id;

    const {comicsDataId, comicsSummary} = req.body;


    if (
        !paramCheck(comicsDataId) ||
        typeof comicsSummary!=="string"
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    await ComicsArchiveLogCollection.archiveComicsData(userId, comicsData);


    await ComicsCollection.updateComicsSummary(comicsDataId, comicsSummary);


    return httpRespond(res, 200);
}