const express = require('express');
const router = express.Router();
const imageController = require("./image.controller")
const multer  = require('multer')
const upload = multer()


router.post("/post", upload.single("image"), imageController.uploadPostImage);


module.exports = router;