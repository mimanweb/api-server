const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsCollection = require("../../../models/comics");
const ComicBookCollection = require("../../../models/comicBook");
const PostCollection = require("../../../models/post");
const ImageCollection = require("../../../models/image");
const path = require("path");
const fs = require("fs");
const {mkdirIfNone, readDir, mv, checkPath} = require("../../../utils/file");
const {resizeImgBuffer, deleteImgFile} = require("../../../utils/image");
const _ = require("underscore");
const {getPostImageFolderDir} = require("../../../utils/index/post");
const {getComicBookPageFolderDir} = require("../../../utils/index/comicBook");
const {saveComicBookPageFile} = require("../../../utils/index/comicBook");
const {COMIC_BOOK_IMAGE_STATUS, COMIC_BOOK_PAGE_TYPE, COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_STATUS} = require("../../../utils/index/comicBook");
const {SITE_CATEGORY_INDEX, SITE_BOARD_INDEX} = require("../../../utils/index/siteIndex");
const {POST_STATUS} = require("../../../utils/index/post");
const {IMAGE_TYPE} = require("../../../utils/index/image");
const {validateEditorContent, findImgIdListFromEditorContent} = require("../../../utils/editor");




const SIZE = {
  SMALL: {
    value: 128,
    name: "small",
  },
  MEDIUM: {
    value: 256,
    name: "medium",
  },
  LARGE: {
    value: 512,
    name: "large",
  },
  XLARGE: {
    value: 1024,
    name: "xlarge",
  },
  XXLARGE: {
    value: 2048,
    name: "xxlarge",
  },
}




/**
 * Upload comic book page image
 * */
exports.addComicBookPageImage = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;
  const clientIp = req.session.clientIp;

  if (!isRegUser) return httpError(res, 403);

  const imgFile = req.file;
  const {comicBookDataId} = req.body;
  let {isLinkThumbnail} = req.body;

  isLinkThumbnail = isLinkThumbnail==="true";


  if (
    !imgFile ||
    !paramCheck(comicBookDataId)
  ) return httpError(res, 400);


  const mimeType = imgFile.mimetype;
  const fileExt = mimeType.split("/")[1];


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);

  const {index} = comicBookData;
  const {comicsDataId} = index;

  /**
   * Find comics data
   * */
  const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

  if (!comicsData) return httpError(res, 404);

  const {number: comicsNumber, id: comicsId} = comicsData;



  /**
   * Save image file in all sizes
   * */
  const [smallSizePageFolder, smallSizePageName] = await saveComicBookPageFile(imgFile, SIZE.SMALL.value);
  const [mediumSizePageFolder, mediumSizePageName] = await saveComicBookPageFile(imgFile, SIZE.MEDIUM.value);
  const [largeSizePageFolder, largeSizePageName] = await saveComicBookPageFile(imgFile, SIZE.LARGE.value);
  const [xLargeSizePageFolder, xLargeSizePageName] = await saveComicBookPageFile(imgFile, SIZE.XLARGE.value);
  const [xxLargeSizePageFolder, xxLargeSizePageName] = await saveComicBookPageFile(imgFile, SIZE.XXLARGE.value);


  const createImageData = async (size, folder, fileName, type) => {
    const latestImageData = await ImageCollection.findLatestImage();
    const newImgDataNumber = !latestImageData? 0: latestImageData.number+1;
    const imgId = `${type}/${newImgDataNumber}/${comicsId}_page_${size}.${fileExt}`;

    await ImageCollection.createImage(imgId, newImgDataNumber, [type, size].join("/"), userId, clientIp, folder, fileName, mimeType, undefined, undefined);

    return imgId;
  }


  const smallSizePageImgId = await createImageData(SIZE.SMALL.name, smallSizePageFolder, smallSizePageName, isLinkThumbnail? IMAGE_TYPE.COMIC_BOOK.PAGE.LINK_THUMBNAIL: IMAGE_TYPE.COMIC_BOOK.PAGE.IMAGE);
  const mediumSizePageImgId = await createImageData(SIZE.MEDIUM.name, mediumSizePageFolder, mediumSizePageName, isLinkThumbnail? IMAGE_TYPE.COMIC_BOOK.PAGE.LINK_THUMBNAIL: IMAGE_TYPE.COMIC_BOOK.PAGE.IMAGE);
  const largeSizePageImgId = await createImageData(SIZE.LARGE.name, largeSizePageFolder, largeSizePageName, isLinkThumbnail? IMAGE_TYPE.COMIC_BOOK.PAGE.LINK_THUMBNAIL: IMAGE_TYPE.COMIC_BOOK.PAGE.IMAGE);
  const xLargeSizePageImgId = await createImageData(SIZE.XLARGE.name, xLargeSizePageFolder, xLargeSizePageName, isLinkThumbnail? IMAGE_TYPE.COMIC_BOOK.PAGE.LINK_THUMBNAIL: IMAGE_TYPE.COMIC_BOOK.PAGE.IMAGE);
  const xxLargeSizePageImgId = await createImageData(SIZE.XXLARGE.name, xxLargeSizePageFolder, xxLargeSizePageName, isLinkThumbnail? IMAGE_TYPE.COMIC_BOOK.PAGE.LINK_THUMBNAIL: IMAGE_TYPE.COMIC_BOOK.PAGE.IMAGE);




  /**
   * Save as link-thumbnail
   * */
  if (isLinkThumbnail) {
    let thumbnailList = [...comicBookData.linkThumbnails] || [];

    /**
     * Delete previously saved image
     * */
    const prevSavedThumbnail = thumbnailList.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.WRITING);

    if (prevSavedThumbnail) {
      for (const imgId of Object.values(prevSavedThumbnail._doc.imgId)) {
        const imgData = await ImageCollection.findImageById(imgId);
        const {file} = imgData;

        await deleteImgFile(path.join(getComicBookPageFolderDir(), file.directory, file.name));
        await imgData.deleteImageData();
      }
    }


    thumbnailList = thumbnailList.filter(thumbnail => thumbnail.status!==COMIC_BOOK_IMAGE_STATUS.WRITING);

    thumbnailList.push({
      status: COMIC_BOOK_IMAGE_STATUS.WRITING,
      imgId: {
        small: smallSizePageImgId,
        medium: mediumSizePageImgId,
        large: largeSizePageImgId,
        xLarge: xLargeSizePageImgId,
        xxLarge: xxLargeSizePageImgId
      }
    })


    await comicBookData.updateComicBookLinkThumbnailList(thumbnailList);

    comicBookData.linkThumbnails = thumbnailList;
  }
  /**
   * Save as image page
   * */
  else {
    const pageList = [...comicBookData.pages] || [];
    const latestPageNumber = _.isEmpty(pageList)? 0: Math.max(...pageList.map(page => page.pageNumber));

    pageList.push({
      status: COMIC_BOOK_IMAGE_STATUS.WRITING,
      pageNumber: latestPageNumber+1,
      pageType: COMIC_BOOK_PAGE_TYPE.SINGLE,
      imgId: {
        small: smallSizePageImgId,
        medium: mediumSizePageImgId,
        large: largeSizePageImgId,
        xLarge: xLargeSizePageImgId,
        xxLarge: xxLargeSizePageImgId
      }
    })

    await comicBookData.updateComicBookPageList(pageList);

    comicBookData.pages = pageList;
  }


  return httpRespond(res, 200, {
    comicBookData: comicBookData
  })
}


/**
 * Update comic book single page type
 * */
exports.updateComicBookSinglePageType = async (req, res) => {
  const isRegUser = req.isRegUser;

  const {comicBookDataId, pageNumber, pageType} = req.body;


  if (
    !paramCheck(comicBookDataId) ||
    !paramCheck(pageNumber, paramType.Number) ||
    !paramCheck(pageType) ||
    ![COMIC_BOOK_PAGE_TYPE.SINGLE, COMIC_BOOK_PAGE_TYPE.WIDE].includes(pageType)
  ) return httpError(res, 400);


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);


  if (
    !isRegUser ||
    comicBookData.owner.userId!==req.userData.id
  ) return httpError(res, 403);


  const pageList = comicBookData.pages;

  pageList.forEach(page => {
    if (page.pageNumber===pageNumber) page.pageType = pageType;
  })

  await comicBookData.updateComicBookPageList(pageList);

  comicBookData.pages = pageList;

  return httpRespond(res, 200, {
    comicBookData: comicBookData
  });
}


/**
 * Update comic book double-page type
 * */
exports.updateComicBookDoublePageType = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;

  const {comicBookDataId, firstPageNumber, secondPageNumber, pageType} = req.body;


  if (
    !paramCheck(comicBookDataId) ||
    !paramCheck(firstPageNumber, paramType.Number) ||
    !paramCheck(secondPageNumber, paramType.Number) ||
    (secondPageNumber-firstPageNumber)!==1 ||
    !paramCheck(pageType) ||
    ![COMIC_BOOK_PAGE_TYPE.SINGLE, COMIC_BOOK_PAGE_TYPE.PAIRED].includes(pageType)
  ) return httpError(res, 400);


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);

  const {owner: comicBookOwner, pages} = comicBookData;

  const pageList = [...pages];
  const pairedPageList = pageList.filter(page => page.pageType===COMIC_BOOK_PAGE_TYPE.PAIRED);


  if (
    !isRegUser ||
    comicBookOwner.userId!==userId
  ) return httpError(res, 403);


  const pairNumber = _.isEmpty(pairedPageList)? 1: Math.max(...pairedPageList.map(page => page.pairNumber))+1;


  pageList.forEach(page => {
    if ([firstPageNumber, secondPageNumber].includes(page.pageNumber)) {

      page.pageType = pageType;

      switch (pageType) {
        case COMIC_BOOK_PAGE_TYPE.PAIRED:
          page.pairNumber = pairNumber;
          break;
        case COMIC_BOOK_PAGE_TYPE.SINGLE:
          delete page.pairNumber;
          break;
      }
    }
  })


  await comicBookData.updateComicBookPageList(pageList);

  comicBookData.pages = pageList;


  return httpRespond(res, 200, {
    comicBookData: comicBookData
  });
}


/**
 * Delete comic book page
 * */
exports.deleteComicBookPage = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;

  const {comicBookDataId, pageNumber, secondPageNumber} = req.body;


  if (
    !paramCheck(comicBookDataId) ||
    !paramCheck(pageNumber, paramType.Number) ||
    (secondPageNumber && !paramCheck(secondPageNumber, paramType.Number))
  ) return httpError(res, 400);


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);

  const {owner: comicBookOwner, pages, index} = comicBookData;

  const comicsData = await ComicsCollection.findComicsDataWithDataId(index.comicsDataId);

  if (!comicsData) return httpError(res, 404);


  if (
    !isRegUser ||
    comicBookOwner.userId!==userId
  ) return httpError(res, 403);


  const pageList = [];
  const deleteFuncList = [];

  [...pages].forEach(page => {
    /**
     * Pages to remain
     * */
    if (
      (secondPageNumber && ![pageNumber, secondPageNumber].includes(page.pageNumber)) ||
      (!secondPageNumber && page.pageNumber!==pageNumber)
    ) {
      pageList.push(page)
    }
    /**
     * Pages to delete
     * */
    else {
      const tempFolderDir = path.join(process.env["IMAGES_DIR"], process.env["COMICS_FOLDER"], comicsData.folder, process.env["COMICS_CONTENT_TEMP_FOLDER"]);

      Object.values(page._doc.imgId).forEach(imgId => {
        const splits = imgId.split("/");
        const fileName = splits[splits.length-1];
        const filePath = path.join(tempFolderDir, fileName);

        deleteFuncList.push(new Promise(resolve => {
          deleteImgFile(filePath);
          resolve();
        }));
      })

    }
  })


  await Promise.all(deleteFuncList);


  /**
   * Reset page numbers
   * */
  pageList.forEach((page, idx) => {
    page.pageNumber = idx+1;
  })


  await comicBookData.updateComicBookPageList(pageList);

  comicBookData.pages = pageList;


  return httpRespond(res, 200, {
    comicBookData: comicBookData
  });
}


/**
 * Update comic book page order
 * */
exports.updateComicBookPageOrder = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;

  const {comicBookDataId, pageToMove, isPairedPage} = req.body;

  let {pageToGo} = req.body;

  pageToGo = !isPairedPage && pageToMove<pageToGo? pageToGo-1: pageToGo;


  if (
    !paramCheck(comicBookDataId) ||
    !paramCheck(pageToGo, paramType.Number) ||
    !paramCheck(pageToMove, paramType.Number)
  ) return httpError(res, 400);


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);

  const {owner: comicBookOwner, pages} = comicBookData;


  if (
    !isRegUser ||
    comicBookOwner.userId!==userId
  ) return httpError(res, 403);


  const pageList = [...pages].map(page => page._doc);



  pageList.forEach(page => {
    const pageNumber = page.pageNumber;

    /**
     * Single page to move
     * */
    if (!isPairedPage) {
      if (pageNumber===pageToMove) page.pageNumber = pageToGo;

      else if (pageToMove<pageToGo) {
        if (pageNumber>pageToMove && pageNumber<=pageToGo) page.pageNumber -= 1;
      }
      else {
        if (pageNumber<pageToMove && pageNumber>=pageToGo) page.pageNumber += 1;
      }

    }
    /**
     * Double page to move
     * */
    else {
      const secondPageToMove = pageToMove+1;

      if (secondPageToMove<=pageToGo) {
        if (pageNumber===pageToMove) page.pageNumber = pageToGo-2;
        else if (pageNumber===secondPageToMove) page.pageNumber = pageToGo-1;
        else if (secondPageToMove<pageNumber && pageNumber<pageToGo) page.pageNumber-=2;
      }
      else if (pageToGo<=pageToMove) {
        if (pageNumber===pageToMove) page.pageNumber = pageToGo;
        else if (pageNumber===secondPageToMove) page.pageNumber = pageToGo+1;
        else if (pageToGo<=pageNumber && pageNumber<pageToMove) page.pageNumber+=2;
      }
    }
  })

  await comicBookData.updateComicBookPageList(pageList);

  comicBookData.pages = pageList;


  return httpRespond(res, 200, {
    comicBookData: comicBookData
  });
}


/**
 * Update comic book info
 * */
exports.updateComicBookInfo = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;


  const {comicBookDataId, title, volume, issue, contentType, isTitle, isShortStory, isVolume, link} = req.body;

  if (
    !paramCheck(comicBookDataId)
  ) return httpError(res, 400);


  const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);


  const {owner: comicBookOwner} = comicBookData;

  if (
    !isRegUser ||
    comicBookOwner.userId!==userId
  ) return httpError(res, 403);


  await comicBookData.updateComicBookInfo(
      title===undefined? comicBookData.title: title,
      volume===undefined? comicBookData.index.volume: volume,
      issue===undefined? comicBookData.index.issue: issue,
      contentType===undefined? comicBookData.index.contentType: contentType,
      isTitle===undefined? comicBookData.index.isTitle: isTitle,
      isShortStory===undefined? comicBookData.index.isShortStory: isShortStory,
      isVolume===undefined? comicBookData.index.isVolume: isVolume,
      link===undefined? comicBookData.link: link,
  );


  return httpRespond(res, 200);
}


/**
 * Publish comic book
 * */
exports.publishComicBook = async (req, res) => {
  const isRegUser = req.isRegUser;
  const userId = req.userData.id;
  const userNickname = req.userData.nickname;


  if (!isRegUser) return httpError(res, 403);


  const {comicBookDataId, postContent, postPhrase} = req.body;

  if (
      !paramCheck(comicBookDataId) ||
      (postContent && !validateEditorContent(postContent))
  ) return httpError(res, 400);


  /**
   * Comic book data
   * */
  let comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookData) return httpError(res, 404);


  const _comicBookData = comicBookData._doc;

  const {owner: comicBookOwner, index: comicBookIndex, pages: comicBookPages, linkThumbnails, title, status: comicBookStatus} = _comicBookData;
  const {isTitle, isVolume, isShortStory, volume, issue} = comicBookIndex;

  if (
      comicBookStatus!==COMIC_BOOK_STATUS.WRITING ||
      userId!==comicBookOwner.userId
  ) return httpError(res, 403);


  /**
   * Comics data
   * */
  const comicsData = await ComicsCollection.findComicsDataWithDataId(comicBookIndex.comicsDataId);

  if (!comicsData) return httpError(res, 404);

  const {folder: comicsFolder, _id: comicsDataId, title: comicsTitle, id: comicsId, number: comicsNumber} = comicsData;


  /**
   * Post data
   * */
  const postData = await PostCollection.findPostWithPostOid(comicBookIndex.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code, POST_STATUS.WRITING);






  /**
   * Set new short story number
   * */
  let newShortStoryNumber = _comicBookData.index.shortStoryNumber;

  if (isShortStory) {
    const allComicBookData = await ComicBookCollection.findAllComicBookDataWithLinkedComicsDataId(userId, comicsDataId);
    const allShortStoryComicBookData = allComicBookData.filter(book => book._doc.status===COMIC_BOOK_STATUS.PUBLISH && book._doc.index.isShortStory);
    const latestShortStoryComicBookNumber = _.isEmpty(allShortStoryComicBookData)? 0: Math.max(...allShortStoryComicBookData.map(book => book._doc.index.shortStoryNumber));

    _comicBookData.index.shortStoryNumber = latestShortStoryComicBookNumber+1;
    newShortStoryNumber = latestShortStoryComicBookNumber+1;
  }



  // const pageList = [...comicBookPages].map(page => page._doc);
  const linkThumbnail = [...linkThumbnails].map(thumbnail => thumbnail._doc).find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.WRITING);


  switch (comicBookIndex.contentType) {
    case COMIC_BOOK_CONTENT_TYPE.IMAGE:
      /**
       * Mv comic-book temp page images to content folder
       * Change temp img id names in comic-book data
       * */


      /**
       * Delete link-thumbnail files
       * */
      // delete _comicBookData.linkThumbnails;
      // delete _comicBookData.link;
      break;

    case COMIC_BOOK_CONTENT_TYPE.LINK:
      /**
       * Set link-thumbnail published
       * */
      if (linkThumbnail) {
        linkThumbnail.status = COMIC_BOOK_IMAGE_STATUS.PUBLISH;
      }


      _comicBookData.linkThumbnails = linkThumbnail? [linkThumbnail]: [];


      /**
       * Delete comic-book page image files
       * */
      // for (const page of _comicBookData.pages) {
      //   for (const imgId of Object.values(page._doc.imgId)) {
      //
      //   }
      // }
      // delete _comicBookData.pages;
      break;
  }






  /**
   * Delete unused image files in the post content
   * */
  const imgIdListInContent = findImgIdListFromEditorContent(postContent);
  const imgIdListInData = postData.images;

  const unusedImgIdList = imgIdListInData.filter(imgId => !imgIdListInContent.includes(imgId));

  await (()=>{
    unusedImgIdList.forEach(async imgId => {
      const imageData = await ImageCollection.findImageById(imgId, IMAGE_TYPE.POST);
      const {file} = imageData;

      const filePath = path.join(getPostImageFolderDir(), file.directory, file.name);

      await deleteImgFile(filePath);
    })
  })();






  /**
   * Publish comic-book data
   * */
  await comicBookData.publishComicBookData(userNickname, _comicBookData);



  /**
   * Publish post data
   * */
  const latestPostData = await PostCollection.findLatestPost();

  let postTitle = comicsTitle.korean;

  if (isTitle) postTitle += `:${title}`;

  if (isShortStory) {
    postTitle += " (단편)";
  }
  else {
    if (isVolume) postTitle += ` vol${volume}`;

    postTitle += ` issue${issue}`;
  }

  await postData.publishPost(
      latestPostData? latestPostData.number+1: 1,
      userId,
      req.session.UUID,
      req.clientIp,
      userNickname,
      undefined,
      postTitle,
      postContent,
      postPhrase,
      imgIdListInData,
      undefined,
      undefined
  )


  return httpRespond(res, 200, {
    comicsId: comicsId,
    comicsNumber: comicsNumber,
    isShortStory: isShortStory,
    isVolume: isVolume,
    volume: volume,
    issue: issue,
    shortStoryNumber: newShortStoryNumber,
    translator: userId
  });
}