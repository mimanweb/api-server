const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const time = require("../utils/time");
const {USER_STATE} = require("../utils/index/user");


const Schema = mongoose.Schema;



const UserSchema = new Schema({
  id: {type: String, unique: true},
  password: {
    value: String,
    salt: String,
    resetToken: String,
    resetAttemptCount: Number,
  },
  nickname: String,
  email: {type: String, unique: true},
  login: {
    tokenKey: String,
    failureCount: Number,
    ipAddress: String,
  },
  type: String,
  status: String,
  phrase: {type: String, default: '"재밌게 읽었으면 추천."'},
  time: {
    registered: Date,
    updated: Date,
    dailyVisit: Date,
    nicknameUpdated: Date,
    unregistered: Date,
  },
  record: {
    comicsCount: {type: Number, default: 0},
    hasUnreadNotification: Boolean
  }
});


/**
 * Find a user with given user id
 * */
UserSchema.statics.findUserById = async function (id) {
  return this.findOne({id: id});
};


/**
 * Update daily visit time record with today visit time
 * */
UserSchema.methods.updateDailyVisitTime = async function (todayVisitTime) {
  await this.updateOne(
      {"time.dailyVisit": todayVisitTime},
  );
};


/**
 * Register a login token salt(key) in hash
 * */
UserSchema.methods.registerLoginTokenKey = async function (keyHash) {
  await this.updateOne(
    {$set: {login: {tokenKey: keyHash}}},
    {upsert: true}
  );
};


/**
 * Create a user
 * */
UserSchema.statics.createUser = async function (id, password, passwordSalt, nickname, email) {
  const now = time.now();

  const query = await this.create({
    id: id,
    password: {
      value: password,
      salt: passwordSalt
    },
    nickname: nickname,
    email: email,
    time: {
      registered: now,
      updated: now,
      nicknameUpdated: now,
    }
  });
  return query.id
};


/**
 * Check duplicate id
 * */
UserSchema.statics.findOneBy = async function (param) {
  return this.findOne(param);
};


UserSchema.methods.savePasswordResetToken = async function (token) {
  const password = this.password;
  password.resetToken = token;
  password.resetAttemptCount = 3;

  await this.updateOne(
    {
      $set: {
        password: password
      }
    },
    {upsert: true}
  );
};


UserSchema.methods.updatePasswordResetAttemptCount = async function () {
  const password = this.password;

  password.resetAttemptCount = this._doc.password.resetAttemptCount - 1;

  await this.updateOne(
      {
        $set: {
          password: password
        }
      },
    {upsert: true}
  )
};


UserSchema.methods.changePassword = async function (newPassword) {
  const passwordHash = await bcrypt.hash(newPassword, this._doc.password.salt);

  const password = this.password;

  password.value = passwordHash

  await this.updateOne(
    {
      $set: {
        password: password
      }
    },
    {upsert: true},
  );
  await this.updateOne({
    $unset: {
      "login.tokenKey": 1,
      "password.resetToken": 1,
      "password.resetAttemptCount": 1,
    }
  });
};


UserSchema.methods.changeNickname = async function (nickname) {
  const time = this.time;

  time.nicknameUpdated = new Date();

  await this.updateOne(
    {
      $set: {
        nickname: nickname,
        time: time
      }
    },
    {upsert: true});
};


/**
 * Set unread notification status
 * */
UserSchema.statics.setUserUnreadNotificationStatus = async function (userId, status) {
  await this.updateOne({id: userId}, {"record.hasUnreadNotification": status})
}


/**
 * Increase comics registration count
 * */
UserSchema.statics.increaseUserComicsRegisterCount = async function (userId) {
  await this.updateOne({id: userId}, {$inc: {"record.comicsCount": 1}});
}


/**
 * Unregister user data
 * */
UserSchema.methods.unregisterUserData = async function () {
  await this.updateOne({
    status: USER_STATE.DELETED,
    "time.unregistered": time.now()
  })
}




module.exports = mongoose.model('user', UserSchema);

