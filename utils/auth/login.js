const { issueLoginAccessToken, verifyToken } = require("./token");
const User = require('../../models/user');
const UserDailyVisitCollection = require("../../models/userDailyVisitLog");
const time = require("../../utils/time");
const {USER_STATE} = require("../index/user");
const {USER_TYPE} = require("../../utils/index/user");


/**
 * Verify user login status and pass user data along
 *
 * @param req.session session cookie includes [ACCESS, REFRESH] tokens
 * */
exports.verifyLogin = async (req, res, next) => {
    const expireSession = () => {
        req.session.ACCESS_TOKEN = null
        req.session.REFRESH_TOKEN = null
        return next();
    };


    const accessToken = req.session.ACCESS_TOKEN;
    const refreshToken = req.session.REFRESH_TOKEN;

    // No access token
    if (!accessToken) return expireSession();


    let tokenData = await verifyToken(accessToken, "ACCESS");

    // Invalid access token, or expired
    if (!tokenData && !refreshToken) return expireSession();

    if (!tokenData && refreshToken) {
        tokenData = await verifyToken(refreshToken, "REFRESH");

        // Invalid refresh token, or expired
        if (!tokenData) return expireSession();
    }

    let userData = {};
    try {
        userData = await User.findUserById(tokenData.userId);
    } catch (e) {
        // DB error
        return expireSession();
    }


    // Invalid user id in the given token
    // Or user id or login token was changed before the request
    if (
        !userData ||
        tokenData.userId !== userData.id ||
        tokenData.key !== userData.login.tokenKey ||
        userData.status===USER_STATE.DELETED
    ) {
        return expireSession();
    }


    req.session.ACCESS_TOKEN = await issueLoginAccessToken(tokenData.userId, tokenData.key);

    req.isRegUser = true;
    req.userData = userData;

    /**
     * Check is admin user
     * */
    if (userData.type===USER_TYPE.ADMIN) {
        req.isAdminUser = true;
    }

    /**
     * Check if user has uploaded comics
     * */
    if (userData.record.comicsCount>0) {
        req.isComicsUploadedUser = true;
    }



    /**
     * Update user daily visit log
     * */
    // User has not visited today
    if (userData && userData.time.dailyVisit<time.todayStart()) {

        // Create a new daily visit record
        const userDailyVisitData = await UserDailyVisitCollection.createUserTodayVisitLog(userData.id, req.clientIp);

        // Update user data's daily visit time
        await userData.updateDailyVisitTime(userDailyVisitData.createdTime);
    }

    return next();
};