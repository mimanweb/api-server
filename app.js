const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')
const helmet = require('helmet');
const cookieSession = require('cookie-session');

// module imports
const db = require('./utils/db');
const {setRoutes} = require('./routes/index');

console.log(`Express server getting started at ${Date()}`)

// setting up environment variables
const SERVER_ENV = process.env.SERVER_ENV;
const IS_LOCAL = SERVER_ENV==="local";

console.log(`SERVER_ENV: ${SERVER_ENV}, NODE_ENV: ${process.env.NODE_ENV}, IS_LOCAL: ${IS_LOCAL}`);

const envFile = IS_LOCAL? ".env.local": SERVER_ENV==="production"? ".env.production": ".env.development";

require('dotenv').config({path: path.join(__dirname, envFile)});


console.log("ENV START ============")
console.log(process.env)
console.log("ENV END ============")


const app = express();

// express behind a proxy
// app.enable('trust proxy')
app.set('trust proxy', 1);

// apply helmet
app.use(helmet());

// apply cookie-session
app.use(cookieSession({
  name: process.env["COOKIE_SESSION_NAME"],
  keys: [process.env["COOKIE_SESSION_KEY1"], process.env["COOKIE_SESSION_KEY2"]],
  httpOnly: true,
  domain: IS_LOCAL? "localhost": process.env["COOKIE_DOMAIN"],
}))


// allowed origins
const allowedOrigins = [
    'http://localhost:3000',
    'https://dev.mimanweb.com',
    'https://dev.m.mimanweb.com',
    'https://mimanweb.com',
    'https://m.mimanweb.com',
]


// allowing cors
app.use(cors({
  credentials: true,
  // origin: function (origin, callback) {
  //     // allow origins of mobile apps or curl requests etc
  //     if (!origin) return callback(null, true);
  //     // check if allowed origins
  //     if (!allowedOrigins.includes(origin)) {
  //         const msg = "The CORS policy for this site " +
  //             "does not allow access from the specified Origin."
  //         return callback(new Error(msg), false);
  //     }
  //     return callback(null, true);
  // }
  origin: true
}));

// establish db connection
db.connect();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// set server wide routes
setRoutes(app);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
