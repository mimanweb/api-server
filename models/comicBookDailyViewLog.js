const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");
const {parseDataId} = require("../utils/mongoose");



const ComicBookDailyViewSchema = new Schema({
  comicBookDataId: Schema.Types.ObjectID,
  viewer: {
    userId: String,
    ipAddress: String
  },
  time: Date
});


ComicBookDailyViewSchema.index({"comicBookDataId": -1, "viewer.userId": 1, "viewer.ipAddress": 1, "time": -1}, {partialFilterExpression: {"viewer.userId": {$exists: true}}});



ComicBookDailyViewSchema.statics.createComicBookDailyViewLog = async function (comicBookDataId, userId, ipAddress) {
  const param = {
    "comicBookDataId": comicBookDataId,
    "viewer.ipAddress": ipAddress,
    "time": time.now()
  }

  if (userId) param["viewer.userId"] = userId;

  await this.create(param);
}



ComicBookDailyViewSchema.statics.findComicBookDailyViewLog = async function (comicBookDataId, userId, ipAddress) {
  const param = {
    "comicBookDataId": parseDataId(comicBookDataId),
    "viewer.ipAddress": ipAddress
  }

  if (userId) param["viewer.userId"] = userId;

  return await this.findOne(param).sort({
    "time": -1
  })
}


// ComicBookDailyViewSchema.methods.updateComicBookDailyViewLogUserId = async function (userId) {
//   await this.updateOne({
//     "viewer.userId": userId
//   })
// }




module.exports = mongoose.model('comic_book_daily_view_log', ComicBookDailyViewSchema);