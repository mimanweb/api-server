const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");



const CommentUpvote = new Schema({
    postNumber: Number,
    commentNumber: Number,
    owner: {
        userId: String,
        ipAddress: String,
    },
    isUpvoted: {type: Boolean, default: true},
    time: {
        created: Date,
        updated: Date
    }
});



CommentUpvote.index({"postNumber": -1, "owner.userId": 1, "owner.ipAddress": 1, "time.created": -1}, {partialFilterExpression: {"owner.userId": {$exists: true}}});
CommentUpvote.index({"postNumber": -1, "commentNumber": -1, "owner.userId": 1, "owner.ipAddress": 1, "time.created": -1}, {partialFilterExpression: {"owner.userId": {$exists: true}}});




/**
 * Find all comment upvote data belong to post and user
 * */
CommentUpvote.statics.findUpvotedListByPostAndUser = async function(postNumber, userId, ipAddress) {
    const query = {
        "postNumber": postNumber,
        "owner.ipAddress": ipAddress,
        "isUpvoted": true
    }

    if (userId) {
        query["owner.userId"] = userId;
    } else {
        query["owner.userId"] = {$exists: false};
    }

    return await this.find(query).sort({"postNumber": -1, "time.created": -1});
}


/**
 * Find comment upvote data
 * */
CommentUpvote.statics.findCommentUpvoteData = async function(postNumber, commentNumber, userId, ipAddress) {
    const query = {
        "postNumber": postNumber,
        "commentNumber": commentNumber,
        "owner.ipAddress": ipAddress
    }

    if (userId) {
        query["owner.userId"] = userId;
    } else {
        query["owner.userId"] = {$exists: false};
    }

    return await this.findOne(query).sort({"postNumber": -1, "commentNumber": -1, "time.created": -1});
}


/**
 * Create comment upvote data
 * */
CommentUpvote.statics.createCommentUpvoteData = async function(postNumber, commentNumber, userId, ipAddress) {
    const query = {
        "postNumber": postNumber,
        "commentNumber": commentNumber,
        "owner.ipAddress": ipAddress,
        "time.created": time.now()
    }

    if (userId) {
        query["owner.userId"] = userId;
    }

    await this.create(query);
}


/**
 * Set upvoted
 * */
CommentUpvote.methods.setUpvoted = async function() {
    await this.updateOne({"isUpvoted": true, "time.updated": time.now()});
}


/**
 * Unset upvoted
 * */
CommentUpvote.methods.unsetUpvoted = async function() {
    await this.updateOne({"isUpvoted": false, "time.updated": time.now()});
}




module.exports = mongoose.model('comment_upvote', CommentUpvote);