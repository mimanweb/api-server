exports.IMAGE_TYPE = {
    POST: "post",
    COMMENT: "comment",
    COMICS: {
        COVER: "comics/cover",
    },
    COMIC_BOOK: {
        PAGE: {
            IMAGE: "comic-book/page/image",
            LINK_THUMBNAIL: "comic-book/page/link_thumbnail"
        }
    }
}