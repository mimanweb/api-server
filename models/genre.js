const mongoose = require('mongoose');
const Schema = mongoose.Schema;




const GenreSchema = new Schema({
    name: {
        korean: String,
        english: String
    },
    code: String
})



GenreSchema.statics.findAllGenres = async function () {
    return await this.find({});
}




module.exports = mongoose.model('genre', GenreSchema);