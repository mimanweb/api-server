const express = require('express');
const router = express.Router();
const controller = require('./test.controller');

router.get('/uuid', controller.uuid);
router.post('/query', controller.query);
router.get('/message', controller.message);

module.exports = router;