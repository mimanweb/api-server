const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const ContentDescriptorCollection = require("../../../models/contentDescriptor");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const mongoose = require("mongoose");



/**
 * Find all Comics Content Descriptors
 * */
exports.findAllComicsContentDescriptors = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const contentDescriptorList = await ContentDescriptorCollection.findAllContentDescriptors();

    return httpRespond(res, 200, {contentDescriptorList: contentDescriptorList});
}


/**
 * Add Comics Descriptor to Comics data
 * */
exports.addComicsContentDescriptor = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, contentDescriptorDataId} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(contentDescriptorDataId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);

    const comicsContentDescriptorToAdd = mongoose.Types.ObjectId(contentDescriptorDataId);

    if (comicsData.contentDescriptors.includes(comicsContentDescriptorToAdd)) return httpError(res, 400);


    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    comicsData.contentDescriptors.push(comicsContentDescriptorToAdd);

    await ComicsCollection.updateComicsContentDescriptors(comicsDataId, comicsData.contentDescriptors);

    return httpRespond(res, 200);
}