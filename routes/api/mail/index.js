const express = require('express');
const router = express.Router();
const controller = require('./mail.controller');


router.post('/user/id', controller.userId);
router.post('/user/password/reset/link', controller.userPasswordResetLink);


module.exports = router;