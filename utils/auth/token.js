const jwt = require('jsonwebtoken');


async function issueJwtToken(params, expiresIn) {
    try {
        return await jwt.sign(
            params,
            params.type === "ACCESS"? process.env['JWT_ACCESS_TOKEN_SECRET_KEY']: process.env['JWT_REFRESH_TOKEN_SECRET_KEY'],
            {expiresIn: expiresIn})
    } catch (e) {
        console.error(e);
        return null;
    }
}


const ACCESS_TOKEN_EXPIRATION_TIME = (()=>{
    if (process.env.SERVER_ENV==="local") return 3;
    else return 60 * 60 * 3;
})()


const REFRESH_TOKEN_EXPIRATION_TIME = (()=>{
    if (process.env.SERVER_ENV==="local") return 60 * 60 * 24 * 90;
    else return 60 * 60 * 24 * 90;
})()



exports.issueLoginAccessToken = async (userId, key) => {
    return await issueJwtToken(
        { userId: userId, type: 'ACCESS', key: key },
      ACCESS_TOKEN_EXPIRATION_TIME
    )
};


exports.issueLoginRefreshToken = async (userId, key) => {
    return await issueJwtToken(
        { userId: userId, type: 'REFRESH', key: key },
      REFRESH_TOKEN_EXPIRATION_TIME
    )
};


exports.verifyToken = async (token, type) => {
    try {
        return await jwt.verify(token, type === "ACCESS"? process.env['JWT_ACCESS_TOKEN_SECRET_KEY']: process.env['JWT_REFRESH_TOKEN_SECRET_KEY']);
    } catch (e) {
        return false;
    }
};