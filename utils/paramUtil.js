
const isNumeric = (str) => {
    return !isNaN(parseFloat(str))
}

const paramType = {
    Boolean: "boolean",
    Number: "number",
    Object: "object",
    String: "string"
}

exports.paramType = paramType;


exports.paramCheck = (param, type) => {
    if (!param) return false;

    if (param && type) {
        switch (type) {
            case paramType.Number:
                if (!isNumeric(param)) return false;
                break;
            default:
                if (typeof param!==type) return false;
                break;
        }
    }

    return true;
}
