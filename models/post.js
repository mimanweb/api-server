const mongoose = require('mongoose');
const time = require("../utils/time");
const {POST_STATUS, POST_TYPE} = require("../utils/index/post");
const {BOARD_POST_LIST_TYPE, BOARD_HOT_POST_LIST_CRITERIA} = require("../utils/index/board");
const Schema = mongoose.Schema;
const _ = require("underscore");



const PostSchema = new Schema({
  number: Number,
  type: {type: String, index: true, sparse: true},
  index: {
    category: String,
    board: String,
  },
  owner: {
    userId: {type: String, index: true, sparse: true},
    deviceUUID: {type: String, index: true},
    ipAddress: String,
    nickname: String,
    password: String,
  },
  title: String,
  content: String,
  phrase: {type: String, default: '"재밌게 봤으면 추천."'},
  images: [String],
  hasYoutube: Boolean,
  status: {type: String, default: POST_STATUS.WRITING, index: true},
  record: {
    viewCount: {type:Number, default: 0},
    upvoteCount: {type:Number, default: 0},
    commentCount: {type:Number, default: 0}
  },
  time: {
    created: Date,
    updated: Date,
    published: Date,
    deleted: Date
  }
});


// INDEX for querying post number
PostSchema.index({"number": -1}, {index: true, sparse: true});

// INDEX for ordering by desc created time
PostSchema.index({"time.created": -1});

PostSchema.index({"index.category": 1, "index.board": 1});




/**
 * Find post with post id
 * */
PostSchema.statics.findPostWithPostOid = async function (postOid, category, board, status) {
  const query = {
    _id: typeof postOid==="string"? mongoose.Types.ObjectId(postOid): postOid,
    "index.category": category,
    "index.board": board
  }

  if (status) query["status"] = status;

  return await this.findOne(query).sort({"time.created": -1});
}


/**
 * FInd post with post number
 * */
PostSchema.statics.findPostWithPostNumber = async function (postNumber, category, board, status) {
  const query = {
    "number": Number(postNumber)
  }

  if (category && board) {
    query["index.category"] = category;
    query["index.board"] = board;
  }

  if (status) {
    query["status"] = status;
  }

  return await this.findOne(query).sort({"time.created": -1});
}


PostSchema.statics.findAllPostMatchPostNumberList = async function (postNumberList) {
  return this.find({number: {$in: postNumberList}}).sort({number: -1});
}


/**
 * Find temp-saved post
 * */
PostSchema.statics.findTempPost = async function (userId, deviceUUID, category, board) {
  const query = {
    "index.category": category,
    "index.board": board,
    "status": POST_STATUS.WRITING
  }

  if (userId) {
    query["owner.userId"] = userId;
  } else {
    query["owner.userId"] = {$exists: false};
    query["owner.deviceUUID"] = deviceUUID;
  }

  return await this.findOne(query).sort({"time.created": -1});
}


/**
 * Find latest post
 * */
PostSchema.statics.findLatestPost = async function () {
  return await this.findOne({
    number: {$exists: true},
  }).sort({number: -1});
}


/**
 * Find latest post
 * */
PostSchema.statics.findLatestPostWithSiteIndex = async function (category, board, status) {
  const param = {
    "index.category": category,
    "index.board": board
  }

  if (status) param.status = status;

  return await this.findOne(param).sort({number: -1});
}


/**
 * Find most recent post list
 * */
PostSchema.statics.findLatestPostList = async function (category, board, count, status) {
  const param = {
    "index.category": category,
    "index.board": board
  }

  if (status) param.status = status;

  return await this.find(param).sort({number: -1}).limit(count);
}


/**
 * Find recent posts
 * Sorted by upvote count
 * */
PostSchema.statics.findPostListWithUpvoteCountCritInTimeRange = async function (category, board, countToQuery=0, minUpvoteCount=0, startTime, endTime) {
  const param = {
    "index.category": category,
    "index.board": board,
    "record.upvoteCount": {$gte: minUpvoteCount},
    "status": POST_STATUS.PUBLISH
  }

  const $andCond = [];

  if (startTime) {
    $andCond.push({"time.published": {$lt: startTime}});
  }
  if (endTime) {
    $andCond.push({"time.published": {$gte: endTime}});
  }

  if (!_.isEmpty($andCond)) param["$and"] = $andCond;

  return await this.find(param).sort({"record.upvoteCount": -1}).limit(countToQuery) || [];
}


/**
 * Find post-page items and page info
 * With post-number
 * Find only PUBLISH, NONE-NOTICE or NOTICE posts
 * Includes post content
 * */
PostSchema.statics.findPublishedPostListAndPageInfoWithPostNumber = async function (postNumber, category, board, postPerPage, listType) {
  postNumber = Number(postNumber);

  let aggregationRes


  switch (listType) {
    case BOARD_POST_LIST_TYPE.NOTICE:
      aggregationRes = await this.aggregate([
        {
          '$facet': {
            'post': [
              {
                '$match': {
                  'number': postNumber,
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board
                }
              }
            ],
            'ltCount': [
              {
                '$match': {
                  'number': {
                    '$lt': postNumber
                  },
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': POST_TYPE.NOTICE
                }
              }, {
                '$count': 'count'
              }
            ],
            'totalCount': [
              {
                '$match': {
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': POST_TYPE.NOTICE
                }
              }, {
                '$count': 'count'
              }
            ]
          }
        }, {
          '$project': {
            'post': {
              '$first': '$post'
            },
            'ltCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$ltCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$ltCount.count'
                },
                'else': 0
              }
            },
            'totalCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$totalCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$totalCount.count'
                },
                'else': 0
              }
            }
          }
        }, {
          '$project': {
            'post._id': 0,
            'post.owner.deviceUUID': 0,
            'post.owner.password': 0
          }
        }, {
          '$addFields': {
            'postPerPage': postPerPage
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            postPage: {
              $ceil: {
                $divide: [
                  {
                    $subtract: [
                      "$totalCount",
                      '$ltCount'
                    ]
                  },
                  "$postPerPage"
                ]
              }
            },
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', '$postPerPage'
                ]
              }
            }
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postPage': 1,
            'postToSkip': {
              '$multiply': [
                {
                  '$subtract': [
                    '$postPage', 1
                  ]
                }, '$postPerPage'
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'postskip': '$postToSkip'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$eq': [
                          '$status', POST_STATUS.PUBLISH
                        ]
                      }, {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$eq': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }
                    ]
                  }
                }
              }, {
                '$project': {
                  'number': 1,
                  '_id': 0
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }
            ],
            'as': 'postNumList'
          }
        }, {
          '$project': {
            'post': 1,
            'postPage': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postToSkip': 1,
            'pagePostNumberList': {
              '$ifNull': [
                {
                  '$slice': [
                    '$postNumList', '$postToSkip', '$postPerPage'
                  ]
                }, []
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'list': '$pagePostNumberList'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$in': [
                          '$number', '$$list.number'
                        ]
                      }, {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$eq': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }
                    ]
                  }
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }, {
                '$project': {
                  '_id': 0,
                  'owner.deviceUUID': 0,
                  'owner.password': 0,
                  'content': 0
                }
              }
            ],
            'as': 'pagePostList'
          }
        }, {
          '$project': {
            'pagePostNumberList': 0
          }
        }
      ]);
      break;
    case BOARD_POST_LIST_TYPE.HOT:
      aggregationRes = await this.aggregate([
        {
          '$facet': {
            'post': [
              {
                '$match': {
                  'number': postNumber,
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board
                }
              }
            ],
            'ltCount': [
              {
                '$match': {
                  'number': {
                    '$lt': postNumber
                  },
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': {
                    '$ne': POST_TYPE.NOTICE
                  },
                  "record.upvoteCount": {$gte: BOARD_HOT_POST_LIST_CRITERIA.UPVOTE}
                }
              }, {
                '$count': 'count'
              }
            ],
            'totalCount': [
              {
                '$match': {
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': {
                    '$ne': POST_TYPE.NOTICE
                  },
                  "record.upvoteCount": {$gte: BOARD_HOT_POST_LIST_CRITERIA.UPVOTE}
                }
              }, {
                '$count': 'count'
              }
            ]
          }
        }, {
          '$project': {
            'post': {
              '$first': '$post'
            },
            'ltCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$ltCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$ltCount.count'
                },
                'else': 0
              }
            },
            'totalCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$totalCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$totalCount.count'
                },
                'else': 0
              }
            }
          }
        }, {
          '$project': {
            'post._id': 0,
            'post.owner.deviceUUID': 0,
            'post.owner.password': 0
          }
        }, {
          '$addFields': {
            'postPerPage': postPerPage
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            postPage: {
              $cond: {
                if: {
                  $eq: ["$post.type", POST_TYPE.NOTICE]
                },
                then: 1,
                else: {
                  $ceil: {
                    $divide: [
                      {
                        $subtract: [
                          "$totalCount",
                          '$ltCount'
                        ]
                      },
                      "$postPerPage"
                    ]
                  }
                }
              }
            },
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', '$postPerPage'
                ]
              }
            }
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postPage': 1,
            'postToSkip': {
              '$multiply': [
                {
                  '$subtract': [
                    '$postPage', 1
                  ]
                }, '$postPerPage'
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'postskip': '$postToSkip'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$ne': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }, {
                        '$eq': [
                          '$status', POST_STATUS.PUBLISH
                        ]
                      }, {
                        $gte: ["$record.upvoteCount", BOARD_HOT_POST_LIST_CRITERIA.UPVOTE]
                      }
                    ]
                  }
                }
              }, {
                '$project': {
                  'number': 1,
                  '_id': 0
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }
            ],
            'as': 'postNumList'
          }
        }, {
          '$project': {
            'post': 1,
            'postPage': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postToSkip': 1,
            'pagePostNumberList': {
              '$ifNull': [
                {
                  '$slice': [
                    '$postNumList', '$postToSkip', '$postPerPage'
                  ]
                }, []
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'list': '$pagePostNumberList'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$ne': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }, {
                        $gte: ["$record.upvoteCount", BOARD_HOT_POST_LIST_CRITERIA.UPVOTE]
                      }, {
                        '$in': [
                          '$number', '$$list.number'
                        ]
                      }
                    ]
                  }
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }, {
                '$project': {
                  '_id': 0,
                  'owner.deviceUUID': 0,
                  'owner.password': 0,
                  'content': 0
                }
              }
            ],
            'as': 'pagePostList'
          }
        }, {
          '$project': {
            'pagePostNumberList': 0
          }
        }
      ]);
      break;
    default:
      aggregationRes = await this.aggregate([
        {
          '$facet': {
            'post': [
              {
                '$match': {
                  'number': postNumber,
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board
                }
              }
            ],
            'ltCount': [
              {
                '$match': {
                  'number': {
                    '$lt': postNumber
                  },
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': {
                    '$ne': POST_TYPE.NOTICE
                  }
                }
              }, {
                '$count': 'count'
              }
            ],
            'totalCount': [
              {
                '$match': {
                  'status': POST_STATUS.PUBLISH,
                  'index.category': category,
                  'index.board': board,
                  'type': {
                    '$ne': POST_TYPE.NOTICE
                  }
                }
              }, {
                '$count': 'count'
              }
            ]
          }
        }, {
          '$project': {
            'post': {
              '$first': '$post'
            },
            'ltCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$ltCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$ltCount.count'
                },
                'else': 0
              }
            },
            'totalCount': {
              '$cond': {
                'if': {
                  '$gt': [
                    {
                      '$size': '$totalCount'
                    }, 0
                  ]
                },
                'then': {
                  '$first': '$totalCount.count'
                },
                'else': 0
              }
            }
          }
        }, {
          '$project': {
            'post._id': 0,
            'post.owner.deviceUUID': 0,
            'post.owner.password': 0
          }
        }, {
          '$addFields': {
            'postPerPage': postPerPage
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            postPage: {
              $cond: {
                if: {
                  $eq: ["$post.type", POST_TYPE.NOTICE]
                },
                then: 1,
                else: {
                  $ceil: {
                    $divide: [
                      {
                        $subtract: [
                          "$totalCount",
                          '$ltCount'
                        ]
                      },
                      "$postPerPage"
                    ]
                  }
                }
              }
            },
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', '$postPerPage'
                ]
              }
            }
          }
        }, {
          '$project': {
            'post': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postPage': 1,
            'postToSkip': {
              '$multiply': [
                {
                  '$subtract': [
                    '$postPage', 1
                  ]
                }, '$postPerPage'
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'postskip': '$postToSkip'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$eq': [
                          '$status', POST_STATUS.PUBLISH
                        ]
                      }, {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$ne': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }
                    ]
                  }
                }
              }, {
                '$project': {
                  'number': 1,
                  '_id': 0
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }
            ],
            'as': 'postNumList'
          }
        }, {
          '$project': {
            'post': 1,
            'postPage': 1,
            'postPerPage': 1,
            'totalPageCount': 1,
            'postToSkip': 1,
            'pagePostNumberList': {
              '$ifNull': [
                {
                  '$slice': [
                    '$postNumList', '$postToSkip', '$postPerPage'
                  ]
                }, []
              ]
            }
          }
        }, {
          '$lookup': {
            'from': 'posts',
            'let': {
              'list': '$pagePostNumberList'
            },
            'pipeline': [
              {
                '$match': {
                  '$expr': {
                    '$and': [
                      {
                        '$in': [
                          '$number', '$$list.number'
                        ]
                      }, {
                        '$eq': [
                          '$index.category', category
                        ]
                      }, {
                        '$eq': [
                          '$index.board', board
                        ]
                      }, {
                        '$ne': [
                          '$type', POST_TYPE.NOTICE
                        ]
                      }
                    ]
                  }
                }
              }, {
                '$sort': {
                  'number': -1
                }
              }, {
                '$project': {
                  '_id': 0,
                  'owner.deviceUUID': 0,
                  'owner.password': 0,
                  'content': 0
                }
              }
            ],
            'as': 'pagePostList'
          }
        }, {
          '$project': {
            'pagePostNumberList': 0
          }
        }
      ]);
      break;
  }

  const {post, postPage, totalPageCount, pagePostList} = aggregationRes[0];


  return {
    searchedPost: post || null,
    searchedPostPage: postPage,
    totalPageCount: totalPageCount,
    pagePostList: pagePostList
  }
}



/**
 * Find post-page items and page info
 * With post-page number
 * Exclude post content
 * */
PostSchema.statics.findPublishedPostListAndPageInfoWithPageNumber = async function (pageNumber, category, board, postPerPage, listType) {
  pageNumber = Number(pageNumber);

  const postCountToSkip = (pageNumber-1)*postPerPage;

  let aggregationRes;

  switch (listType) {
    case BOARD_POST_LIST_TYPE.NOTICE:
      aggregationRes = await this.aggregate([
        {
          '$match': {
            'status': POST_STATUS.PUBLISH,
            'index.category': category,
            'index.board': board,
            "type": {
              $eq: POST_TYPE.NOTICE
            }
          }
        }, {
          '$sort': {
            'number': -1
          }
        }, {
          '$group': {
            '_id': null,
            'totalCount': {
              '$sum': 1
            },
            'totalList': {
              '$push': '$$ROOT'
            }
          }
        }, {
          '$project': {
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', postPerPage
                ]
              }
            },
            'postList': {
              '$slice': [
                '$totalList', postCountToSkip, postPerPage
              ]
            }
          }
        }
      ]);
      break;
    case BOARD_POST_LIST_TYPE.HOT:
      aggregationRes = await this.aggregate([
        {
          '$match': {
            'status': POST_STATUS.PUBLISH,
            'index.category': category,
            'index.board': board,
            "type": {
              $ne: POST_TYPE.NOTICE
            },
            "record.upvoteCount": {
              $gte: BOARD_HOT_POST_LIST_CRITERIA.UPVOTE
            }
          }
        }, {
          '$sort': {
            'number': -1
          }
        }, {
          '$group': {
            '_id': null,
            'totalCount': {
              '$sum': 1
            },
            'totalList': {
              '$push': '$$ROOT'
            }
          }
        }, {
          '$project': {
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', postPerPage
                ]
              }
            },
            'postList': {
              '$slice': [
                '$totalList', postCountToSkip, postPerPage
              ]
            }
          }
        }
      ]);
      break;
    default:
      aggregationRes = await this.aggregate([
        {
          '$match': {
            'status': POST_STATUS.PUBLISH,
            'index.category': category,
            'index.board': board,
            "type": {
              $ne: POST_TYPE.NOTICE
            }
          }
        }, {
          '$sort': {
            'number': -1
          }
        }, {
          '$group': {
            '_id': null,
            'totalCount': {
              '$sum': 1
            },
            'totalList': {
              '$push': '$$ROOT'
            }
          }
        }, {
          '$project': {
            'totalPageCount': {
              '$ceil': {
                '$divide': [
                  '$totalCount', postPerPage
                ]
              }
            },
            'postList': {
              '$slice': [
                '$totalList', postCountToSkip, postPerPage
              ]
            }
          }
        }
      ]);
      break;
  }

  let postList = []
  let totalPageCount = 0

  if (!_.isEmpty(aggregationRes)) {

    postList = aggregationRes[0].postList;
    totalPageCount = aggregationRes[0].totalPageCount;
  }


  return {
    postList: postList,
    totalPageCount: totalPageCount
  }
}


/**
 * Find last page's items and page info
 * Exclude post content
 * */
PostSchema.statics.findPublishedPostListAndPageInfoOfLastPage = async function (category, board, postPerPage) {
  const aggregationRes = await this.aggregate([
    {
      '$match': {
        'status': POST_STATUS.PUBLISH,
        'index.category': category,
        'index.board': board,
        'type': {
          $ne: POST_TYPE.NOTICE
        }
      }
    }, {
      '$sort': {
        'number': -1
      }
    }, {
      '$group': {
        '_id': null,
        'totalCount': {
          '$sum': 1
        },
        'postList': {
          '$push': '$$ROOT'
        }
      }
    }, {
      '$addFields': {
        'totalPageCount': {
          '$ceil': {
            '$divide': [
              '$totalCount', postPerPage
            ]
          }
        }
      }
    }, {
      '$project': {
        '_id': 0,
        'totalCount': 1,
        'totalPageCount': 1,
        'postList': {
          '$slice': [
            '$postList', {
              '$multiply': [
                {
                  '$subtract': [
                    '$totalPageCount', 1
                  ]
                }, postPerPage
              ]
            }, postPerPage
          ]
        }
      }
    }
  ])


  const {postList, totalPageCount} = aggregationRes[0];

  return {
    postList: postList || [],
    lastPageNumber: totalPageCount || null,
    totalPageCount: totalPageCount || 0
  }
}


/**
 * Create temp(writing) post
 * */
PostSchema.statics.createTempPost = async function (userId, deviceUUID, ipAddress, category, board, phrase) {
  return await this.create({
    index: {
      category: category,
      board: board
    },
    owner: {
      userId: userId,
      deviceUUID: deviceUUID,
      ipAddress: ipAddress,
    },
    title: "",
    content: "",
    phrase: phrase,
    images: [],
    status: POST_STATUS.WRITING,
    time: {
      created: time.now()
    }
  });
};


/**
 * Update post data
 * */
PostSchema.methods.updatePost = async function (title, content, phrase, images, hasYoutube) {
  const param = {
    "content": content,
    "phrase": phrase,
    "images": images,
    "hasYoutube": hasYoutube,
    "time.updated": time.now()
  }

  if (title) param.title = title;

  await this.updateOne(param);
}


/**
 * Update temp-saved post
 * Post title, content, phrase, imageList
 * */
PostSchema.methods.updateTempPost = async function (title, content, phrase, imageOid) {
  const param = {
    "time.updated": time.now(),
    title: "",
    content: "",
    phrase: ""
  };


  if (title===undefined) {
    delete param.title;
  } else {
    param["title"] = title;
  }

  if (content===undefined) {
    delete param.content;
  } else {
    param["content"] = content;
  }

  if (phrase===undefined) {
    delete param.phrase;
  } else {
    param["phrase"] = phrase;
  }

  if (imageOid) {
    const images = this.images;

    images.push(mongoose.Types.ObjectId(imageOid));
    param["images"] = images;
  }

  await this.updateOne(param);
}


/**
 * Update image list in the data
 * */
PostSchema.methods.updateImageList = async function (imgList) {
  await this.updateOne({
    "images": imgList
  })
}

/**
 * Increase post view count
 * */
PostSchema.statics.increaseViewCount = async function (postNumber, category, board) {
  await this.updateOne({
    number: postNumber,
    "index.category": category,
    "index.board": board
  }, {
    $inc: {"record.viewCount": 1}
  })
}


/**
 * Increase post upvote count
 * */
PostSchema.methods.increaseUpvoteCount = async function () {
  const currUpvoteCount = this.record.upvoteCount;
  const newCount = currUpvoteCount+1;

  await this.updateOne({
    "record.upvoteCount": newCount
  })

  return newCount;
}


/**
 * Decrease post upvote count
 * */
PostSchema.methods.decreaseUpvoteCount = async function () {
  const currUpvoteCount = this.record.upvoteCount;
  const newCount = currUpvoteCount<=0? 0: currUpvoteCount-1;

  await this.updateOne({
    "record.upvoteCount": newCount
  })

  return newCount;
}


/**
 * Increase post comment count
 * */
PostSchema.methods.increaseCommentCount = async function () {
  await this.updateOne({
    $inc: {
      "record.commentCount": 1
    }
  });
}


/**
 * Decrease post comment count
 * */
PostSchema.methods.decreaseCommentCount = async function () {
  await this.updateOne({
    $inc: {
      "record.commentCount": -1
    }
  });
}


/**
 * Add image id to post data
 * */
PostSchema.methods.addImage = async function (imgId) {
  const images = this.images;

  images.push(imgId);

  await this.updateOne({
    "images": images
  })
}


/**
 * Publish a temp-post
 * */
PostSchema.methods.publishPost = async function (newNumber, userId, deviceUUID, ipAddress, nickname, password, postTitle, postContent, postPhrase, images, hasYoutube, postType) {

  if (userId) {
    password = undefined;
  }

  await this.updateOne({
    "number": newNumber,
    "owner.userid": userId,
    "owner.deviceUUID": deviceUUID,
    "owner.ipAddress": ipAddress,
    "owner.nickname": nickname,
    "owner.password": password,
    "title": postTitle,
    "content": postContent,
    "phrase": postPhrase,
    "images": images,
    "hasYoutube": hasYoutube,
    "time.updated": time.now(),
    "time.published": time.now(),
    "status": POST_STATUS.PUBLISH,
    "type": postType
  });

  return newNumber;
}


/**
 * Delete post
 * */
PostSchema.methods.deletePost = async function () {
  await this.updateOne({
    "status": POST_STATUS.DELETED,
    "time.deleted": time.now()
  })
}



module.exports = mongoose.model('post', PostSchema);
