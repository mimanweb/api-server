const {httpRespond, httpError} = require('../../../utils/http/response');
const UserCollection = require('../../../models/user');
const NotificationCollection = require('../../../models/notification');
const {formatCommentData} = require("../../../utils/index/comment");
const {paramCheck, paramType} = require("../../../utils/paramUtil");



/**
 * Find all user notifications
 * In pages
 * */
exports.findAllUserNotifications = async (req, res) => {
  const isRegUser = req.isRegUser;
  const reqUserId = isRegUser? req.userData.id: null;


  if (!isRegUser) return httpError(res, 403);


  const {pageToSearch, itemsPerPage} = req.query;


  if (
    !paramCheck(pageToSearch, paramType.Number) ||
    !paramCheck(itemsPerPage, paramType.Number)
  ) return httpError(res, 400);


  const allNotificationCount = await NotificationCollection.findNotificationCount(reqUserId);
  const notificationDataList = await NotificationCollection.findAllNotificationInPages(reqUserId, itemsPerPage, pageToSearch);


  notificationDataList.forEach(noti => {
    formatCommentData(req, noti.comment);
  })

  await UserCollection.setUserUnreadNotificationStatus(reqUserId, false);


  return httpRespond(res, 200, {
    notificationList: notificationDataList,
    totalPageCount: Math.ceil(allNotificationCount/itemsPerPage),
  })
}


/**
 * Set notification data as checked state
 * */
exports.updateUserNotificationDataAsChecked = async (req, res) => {
  const isRegUser = req.isRegUser;


  if (!isRegUser) return httpError(res, 403);


  const {notificationDataId} = req.body;

  if (
      !paramCheck(notificationDataId)
  ) return httpError(res, 400);


  await NotificationCollection.updateNotificationDataAsChecked(notificationDataId);


  return httpError(res, 200);
}


/**
 * Set all notification dat aas checked
 * */
exports.updateUserNotificationDataAsChecked = async (req, res) => {
  const isRegUser = req.isRegUser;

  if (!isRegUser) return httpError(res, 403);


  await NotificationCollection.updateAllNotificationDataAsChecked(req.userData.id);


  return httpRespond(res, 200);
}