const PostUpvoteCollection = require("../../models/postUpvote");
const PostArchiveLogCollection = require("../../models/postArchiveLog");
const ImageCollection = require("../../models/image");
const {trimIp} = require("../client");
const path = require("path");
const {findTextContentFromHTML} = require("../editor");
const {parseDocFromModel} = require("../mongoose");
const {IMAGE_TYPE} = require("./image");
const {checkIframeInEditorContent} = require("../editor");
const {findImgIdListFromEditorContent} = require("../editor");



const POST_STATUS = {
    WRITING: "WRITING",
    PUBLISH: "PUBLISH",
    ARCHIVED: "ARCHIVED",
    DELETED: "DELETED"
}

exports.POST_TYPE = {
    NOTICE: "NOTICE"
}

exports.POST_PER_PAGE = 30;




const formatPostData = async (req, postData, applyUpvoteState, isViewCountIncreased) => {
    if (!postData) return;

    const post = parseDocFromModel(postData);

    /**
     * Check post ownership
     * */
    post.isMyPost = checkPostDataOwnership(req, post);

    /**
     * Apply post upvote status
     * */
    if (applyUpvoteState) post.isUpvotedPost = await checkIsUpvotedPost(req, post);

    /**
     * Trim post owner ip address
     * */
    post.owner.ipAddress = trimIp(post.owner.ipAddress);

    /**
     * Apply increased view count
     * */
    if (isViewCountIncreased) post.record.viewCount += 1;

    /**
     * Set text content
     * */
    post.textContent = findTextContentFromHTML(post.content);

    post.type = post.type || null;

    delete post._id;
    delete post.owner.deviceUUID;
    delete post.owner.password;
}


const formatPostList = async (req, postList) => {
    const formatListJobs = postList.map(post => {
        return new Promise((resolve, reject) => {
            formatPostData(req, post, false);
            resolve();
        })
    });

    await Promise.all(formatListJobs);
}


const checkPostDataOwnership = (req, postData) => {
    const post = postData._doc? postData._doc: postData;

    const {isRegUser, userData, clientIp} = req;
    const userId = isRegUser? userData.id: null;
    const isRegUserPost = post.owner.userId || false;


    return (!isRegUser && !isRegUserPost && clientIp===post.owner.ipAddress) ||
      (isRegUser && isRegUserPost && userId===post.owner.userId)
}


const checkIsUpvotedPost = async (req, postData) => {
    const post = postData._doc? postData._doc: postData;
    const {isRegUser, userData, clientIp} = req;
    const userId = isRegUser? userData.id: null;


    let isUpvotedPost = false;

    if (isRegUser) {
        const upvoteData = await PostUpvoteCollection.findRegUserRecord(post.number, userId);

        if (upvoteData && upvoteData.isUpvoted) isUpvotedPost = true;
    } else {
        const upvoteData = await PostUpvoteCollection.findUnregUserRecord(post.number, clientIp);

        if (upvoteData && upvoteData.isUpvoted) isUpvotedPost = true;
    }

    return isUpvotedPost;
}


/**
 * Delete unused image data in the post editor content
 * Make archive log data
 * Update post data
 * */
const applyNewContentInfoToPostDataAndMakeArchive = async (postData, newTitle, postContent, newPhrase) => {
    /**
     * Delete unused image files in the editor content
     * */
    const imgIdListInContent = findImgIdListFromEditorContent(postContent);
    const imgIdListInData = postData.images;

    const unusedImgIdList = imgIdListInData.filter(imgId => !imgIdListInContent.includes(imgId));

    await (()=>{
        unusedImgIdList.forEach(async imgId => {
            const imageData = await ImageCollection.findImageById(imgId, IMAGE_TYPE.POST);

            await imageData.deleteImageData();
        })
    })();


    /**
     * Create an archive post
     * */
    await PostArchiveLogCollection.createArchivePost(postData, imgIdListInContent, POST_STATUS.ARCHIVED);


    /**
     * Update post
     * */
    const hasYoutube = checkIframeInEditorContent(postContent);
    await postData.updatePost(newTitle, postContent, newPhrase, imgIdListInContent, hasYoutube);
}


/**
 * FILE DIRECTORIES
 * */
const getPostImageFolderDir = () => {
    return path.join(process.env["IMAGES_DIR"], process.env["POST_IMAGES_FOLDER"]);
}



exports.POST_STATUS = POST_STATUS;

exports.formatPostData = formatPostData;
exports.formatPostList = formatPostList;
exports.checkPostDataOwnership = checkPostDataOwnership;
exports.checkIsUpvotedPost = checkIsUpvotedPost;
exports.applyNewContentInfoToPostDataAndMakeArchive = applyNewContentInfoToPostDataAndMakeArchive;
exports.getPostImageFolderDir = getPostImageFolderDir;