const mongoose = require('mongoose');
const Schema = mongoose.Schema;




const ComicsArtistSchema = new Schema({
    name: {
        origin: String
    }
})



ComicsArtistSchema.statics.findAllComicsArtistsMatchWord = async function (word) {
    let pattern = word.trim().replace(/[\\{}()_\-\s,.\/?!^\[\]*=+;:'"]/g, " ")

    if (!/[\S]/.test(pattern)) return null;

    pattern = pattern.replace(/([\s]+)/g, ".*")

    return await this.find({
        "name.origin": {
            $regex: pattern,
            $options: "is"
        }
    })
}


ComicsArtistSchema.statics.createComicsArtistData = async function (name) {
    return await this.create({"name.origin": name})
}



module.exports = mongoose.model('comics_artist', ComicsArtistSchema);