const PostCollection = require('../../../models/post');
const PostArchiveLogCollection = require('../../../models/postArchiveLog');
const ImageCollection = require('../../../models/image');
const PostCountLogCollection = require("../../../models/postCountLog");
const BoardNoticePostIndexCollection = require("../../../models/boardNoticePostIndex");
const {httpRespond, httpError} = require('../../../utils/http/response');
const time = require("../../../utils/time");
const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {validateEditorContent, findImgIdListFromEditorContent, checkIframeInEditorContent} = require("../../../utils/editor");
const {POST_STATUS, POST_TYPE, POST_PER_PAGE, formatPostList} = require("../../../utils/index/post");
const {IMAGE_TYPE} = require("../../../utils/index/image");
const {BOARD_NOTICE_POST_COUNT} = require("../../../utils/index/board");
const {SITE_BOARD_INDEX, SITE_CATEGORY_INDEX} = require("../../../utils/index/siteIndex");
const {deleteImgFile} = require("../../../utils/image");
const path = require("path");
const _ = require("underscore");
const {applyNewContentInfoToPostDataAndMakeArchive} = require("../../../utils/index/post");
const {getPostImageFolderDir} = require("../../../utils/index/post");




/**
 * Check and create post count logs, board page notice post index list
 * */
Object.entries(SITE_BOARD_INDEX).forEach(async entry => {
  const [categoryCode, boardObj] = entry;
  const category = SITE_CATEGORY_INDEX[categoryCode].code;

  for (const board of Object.values(boardObj)) {
    await BoardNoticePostIndexCollection.checkAndCreateIndex(category, board.code);
  }
})





/**
 * Find previously saved temp post
 * */
exports.findPreviouslySavedTempPost = async (req, res) => {
  const data = req.query;
  const userId = req.userData.id;

  if (!data.category || !data.board) return httpError(res, 403);

  const prevTempPost = await PostCollection.findTempPost(userId, req.session.UUID, data.category, data.board);

  if (!prevTempPost) return httpError(res, 404);
  else {
    const latestUpdateTime = prevTempPost.time.updated || prevTempPost.time.created
    const updateTimeDiffInMinutes = time.now().diff(latestUpdateTime, "minutes");


    const postObj = {
      postId: prevTempPost._id.toString(),
      title: prevTempPost.title,
      content: prevTempPost.content,
      phrase: prevTempPost.phrase,
      createdTime: prevTempPost.time.created,
      updatedTime: prevTempPost.time.updated,
      updateTimeDiffInMinutes: updateTimeDiffInMinutes
    }

    return httpRespond(res, 200, postObj);
  }
}



/**
 * Find post list belong to the page number,
 * And the page group info
 * Of page-number
 * */
exports.findPostListAndPageInfoOfPageNumber = async (req, res) => {
  const {pageNumber, category, board} = req.body;



  if (
    !paramCheck(pageNumber, paramType.Number) ||
    pageNumber<0 ||
    !paramCheck(category) ||
    !paramCheck(board)
  ) return httpError(res, 400);



  const {postList, totalPageCount} = await PostCollection.findPublishedPostListAndPageInfoWithPageNumber(pageNumber, category, board, POST_PER_PAGE);


  const resFormat = {
    postList: Array,
    searchedPage: Number,
    totalPageCount: Number
  }


  if (!_.isEmpty(postList)) {
    await formatPostList(req, postList);

    resFormat.postList = postList;
    resFormat.searchedPage = Number(pageNumber);
    resFormat.totalPageCount = totalPageCount;

    return httpRespond(res, 200, resFormat);
  }
  /**
   * Find last page info if the given post page is empty due to prev page's post has been deleted
   * */
  else {
    const {postList, lastPageNumber, totalPageCount} = await PostCollection.findPublishedPostListAndPageInfoOfLastPage(category, board, POST_PER_PAGE);

    await formatPostList(req, postList);

    resFormat.postList = postList;
    resFormat.searchedPage = Number(lastPageNumber);
    resFormat.totalPageCount = totalPageCount;

    return httpError(res, 404, resFormat)
  }
}



/**
 * Check post data ownership
 * */
exports.checkPostOwnership = async (req, res) => {
  const {postNumber, category, board, password} = req.query;
  const userId = req.userData.id;

  if (
      !paramCheck(postNumber, paramType.Number) ||
      !paramCheck(category, paramType.String) ||
      !paramCheck(board, paramType.String)
  ) return httpError(res, 400);


  const postData = await PostCollection.findPostWithPostNumber(postNumber, category, board, POST_STATUS.PUBLISH);

  // Post has been deleted or archived
  if (!postData) return httpError(res, 404);

  const isRegUserPost = postData.owner.userId;


  /**
   * Check is my post
   * */
  let isMyPost;

  if (isRegUserPost) {
    isMyPost = postData.owner.userId===userId;
  } else {
    if (!paramCheck(password, paramType.String)) return httpError(res, 400);

    isMyPost = postData.owner.password===password;
  }


  // Post is there but Not My Post
  if (!isMyPost) return httpError(res, 403);
  // Post is mine
  else return httpRespond(res, 200);
}



/**
 * Create temp-post(on initial load of writing page)
 * */
exports.createTempPost = async (req, res) => {
  const data = req.body;

  if (!data.category || !data.board) return httpError(res, 403);

  // Fill userId, phrase when is a registered user
  const userId = req.userData.id;
  const phrase = req.userData.phrase;

  let tempPost = await PostCollection.createTempPost(userId, req.session.UUID, req.clientIp, data.category, data.board, phrase);

  const postObj = {
    postId: tempPost._id.toString(),
    title: tempPost.title,
    content: tempPost.content,
    phrase: tempPost.phrase,
    createdTime: tempPost.time.created
  }

  return httpRespond(res, 201, postObj);
}



/**
 * Update temp-post
 * */
exports.updateTempPost = async (req, res) => {
  const data = req.body;
  const {postId: postOid, category, board} = data;

  if (!postOid || !category || !board) return httpError(res, 400);


  const postData = await PostCollection.findPostWithPostOid(postOid, category, board);

  if (!postData) return httpError(res, 404);
  if (postData && postData.status!==POST_STATUS.WRITING) return httpError(res, 404, null, null, {postNumber: postData.number});

  await postData.updateTempPost(data.title, data.content, data.phrase);

  return httpRespond(res, 200);
}



/**
 * Publish temp-post
 * */
exports.publishPost = async (req, res) => {
  const userId = req.userData.id;
  const isAdminUser = req.isAdminUser;
  const ipAddress = req.clientIp;
  const deviceUUID = req.session.UUID;
  const {postId: postOid, category, board, nickname, password, title, content, phrase, type} = req.body;


  if (
      !paramCheck(postOid, paramType.String) ||
      !paramCheck(category, paramType.String) ||
      !paramCheck(board, paramType.String) ||
      !paramCheck(nickname, paramType.String) ||
      !paramCheck(title, paramType.String) ||
      !paramCheck(password) ||
      !paramCheck(content, paramType.String) ||
      !validateEditorContent(content) ||
      (type && ![POST_TYPE.NOTICE].includes(type))
  ) return httpError(res, 400);


  if ((type===POST_TYPE.NOTICE && !isAdminUser)) return httpError(res, 403);


  let tempPostData = await PostCollection.findPostWithPostOid(postOid, category, board, POST_STATUS.WRITING);

  if (!tempPostData) return httpError(res, 404);


  /**
   * Delete unused image files in the editor content
   * */
  const imgIdListInContent = findImgIdListFromEditorContent(content);
  const imgIdListInData = tempPostData.images;

  const unusedImgIdList = imgIdListInData.filter(imgId => !imgIdListInContent.includes(imgId));

  await (()=>{
    unusedImgIdList.forEach(async imgId => {
      const imageData = await ImageCollection.findImageById(imgId, IMAGE_TYPE.POST);
      const {file} = imageData;

      const filePath = path.join(process.env["IMAGES_DIR"], process.env["POST_IMAGES_FOLDER"], file.directory, file.name);

      await deleteImgFile(filePath);
    })
  })();


  /**
   * Find last post number
   * */
  const latestPostData = await PostCollection.findLatestPost();
  const newNumber =  !latestPostData? 1: latestPostData.number+1;

  const hasYoutube = checkIframeInEditorContent(content);

  const postNumber = await tempPostData.publishPost(newNumber, userId, deviceUUID, ipAddress, nickname, password, title, content, phrase, imgIdListInContent, hasYoutube, type);



  /**
   * Register board notice post to index list
   * */
  if (type) {
    const boardNoticePostIndexData = await BoardNoticePostIndexCollection.findIndexData(category, board);

    const {list: notiList} = boardNoticePostIndexData;
    const showAlwaysList = notiList.filter(item => item.showAlways);
    const normalList = notiList.filter(item => !item.showAlways);

    const isListFull = BOARD_NOTICE_POST_COUNT<=notiList.length;
    const isOnlyShowAlways = notiList.length===showAlwaysList.length;


    if (isListFull && !isOnlyShowAlways) {
      normalList.pop();
      normalList.unshift({postNumber: postNumber});
    }
    else if (!isListFull) {
      normalList.unshift({postNumber: postNumber});
    }

    await boardNoticePostIndexData.replacePostList(showAlwaysList.concat(normalList));
  }



  return httpRespond(res, 200, {postNumber: postNumber});
}



/**
 * Make archive post of current post and update current one
 * */
exports.makeArchivePostAndUpdate = async (req, res) => {
  const {postId, password: postPassword, category, board, title, content, phrase} = req.body;
  const {id: userId} = req.userData;
  const isRegUser = req.isRegUser;

  if (
      !paramCheck(postId, paramType.String) ||
      !paramCheck(category, paramType.String) ||
      !paramCheck(board, paramType.String) ||
      !paramCheck(title, paramType.String) ||
      !paramCheck(content, paramType.String) ||
      !validateEditorContent(content)
  ) return httpError(res, 400);


  const postData = await PostCollection.findPostWithPostOid(postId, category, board);

  // Post has been deleted or archived
  if (
      !postData ||
      (postData && postData.status!==POST_STATUS.PUBLISH)
  ) return httpError(res, 404);


  const {owner: postOwner} = postData;
  const isRegUserPost = postOwner.userId;

  /**
   * Check post ownership
   * */
  if (
      (isRegUserPost && !isRegUser) ||
      (isRegUserPost && isRegUser && userId!==postOwner.userId) ||
      (!isRegUserPost && !postPassword) ||
      (!isRegUserPost && postPassword && postPassword!==postOwner.password)
  ) return httpError(res, 403);


  await applyNewContentInfoToPostDataAndMakeArchive(postData, title, content, phrase)



  return httpRespond(res, 200);
}



/**
 * Delete post
 * */
exports.deletePost = async (req, res) => {
  const {postNumber, category, board, password: postPassword} = req.body;
  const {id: userId} = req.userData;
  const isRegUser = req.isRegUser;


  if (
      !paramCheck(postNumber, paramType.Number) ||
      !paramCheck(category) ||
      !paramCheck(board)
  ) return httpError(res, 400);


  const postData = await PostCollection.findPostWithPostNumber(postNumber, category, board, POST_STATUS.PUBLISH);


  // Post has been deleted or archived
  if (
      !postData ||
      (postData && postData.status!==POST_STATUS.PUBLISH)
  ) return httpError(res, 404);


  const {owner: postOwner} = postData;
  const isRegUserPost = postOwner.userId;

  /**
   * Check post ownership
   * */
  if (
      (isRegUserPost && !isRegUser) ||
      (isRegUserPost && isRegUser && userId!==postOwner.userId) ||
      (!isRegUserPost && !postPassword) ||
      (!isRegUserPost && postPassword && postPassword!==postOwner.password)
  ) return httpError(res, 403);


  await postData.deletePost();


  /**
   * Delete board notice post if match
   * */
  const boardNoticePostIndexData = await BoardNoticePostIndexCollection.findIndexDataWithPostNumber(category, board, postNumber);

  if (boardNoticePostIndexData) {
    const notiList = boardNoticePostIndexData.list.filter(item => item.postNumber!==postNumber);
    await boardNoticePostIndexData.replacePostList(notiList);
  }


  return httpRespond(res, 200);

}