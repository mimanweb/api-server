const fs = require("fs");
const {IMAGE_TYPE} = require("./index/image");
const path = require("path");
const time = require("./time");
const {SITE_CATEGORY_INDEX, SITE_BOARD_INDEX} = require("./index/siteIndex");
const sharp = require("sharp");



const MAX_FOLDER_ITEM_CNT = 1000;


const mkdir = async (dir) => {
    await fs.promises.mkdir(dir, { recursive: true });
}







/**
 * Generate common img-id for post, comment images
 * */
exports.generateCommonImgId = (number, type, category, board, fileExt) => {
    const categoryEntry = Object.entries(SITE_CATEGORY_INDEX).find(index => index[1].code===category);
    const categoryKey = categoryEntry[0]
    const categoryImgId = categoryEntry[1].imgId

    const targetBoard = Object.values(SITE_BOARD_INDEX[categoryKey]).find(index => index.code===board);

    let randStr = Math.random().toString(36).substr(2, 2);

    return `${type}/${categoryImgId}${targetBoard.imgId}${randStr}/miman_${number}.${fileExt}`;
}


/**
 * Delete image file at given file-path
 * */
exports.deleteImgFile = async (filePath) => {
    await fs.promises.access(filePath).then(async () => {
        await fs.promises.unlink(filePath);
    }).catch(() => {
    })
}


/**
 * Write common img file for post, comment images
 * */
exports.writeCommonImgFile = async (imgFile, imgType) => {
    const fileExt = imgFile.mimetype.split("/")[1];

    let imgFolderName;


    switch (imgType) {
        case IMAGE_TYPE.POST:
            imgFolderName = process.env["POST_IMAGES_FOLDER"];
            break;
        case IMAGE_TYPE.COMMENT:
            imgFolderName = process.env["COMMENT_IMAGES_FOLDER"];
            break;
    }


    /**
     * Find image folder name
     * */
    const imgFolderDir = path.join(process.env["IMAGES_DIR"], imgFolderName);


    /**
     * Check image folder exists
     * */
    await fs.promises.access(imgFolderDir).catch(async () => {
        await mkdir(imgFolderDir);
    })


    /**
     * Check [current year] folder is in the img folder
     * */
    const year = time.getYear().toString();
    const yearFolder = path.join(imgFolderDir, year);
    const imgFolderItems = await fs.promises.readdir(imgFolderDir);

    if (!imgFolderItems.includes(year)) await mkdir(yearFolder);


    /**
     * Check [current month-date] folder is in the year folder
     * */
    const monthDate = time.getMonthDate();
    const monthDateFolder = path.join(yearFolder, monthDate);
    const yearFolderItems = await fs.promises.readdir(yearFolder);

    if (!yearFolderItems.includes(monthDate)) await mkdir(monthDateFolder);


    /**
     * Find last item folder in [month-date] folder
     * */
    const monthDateFolderItems = await fs.promises.readdir(monthDateFolder);

    if (monthDateFolderItems.length>0) monthDateFolderItems.sort((a, b) => {return Number(b)-Number(a)});

    const lastFolderNumStr = monthDateFolderItems.length>0? monthDateFolderItems[0]: null;


    /**
     * Check last folder in the [month-date] folder
     * Mkdir if none,
     * Mkdir if the last one has more than 1000 items
     * */
    let folderNumStr;

    if (!lastFolderNumStr) {
        folderNumStr = "1";

        // Mkdir if none
        await mkdir(path.join(monthDateFolder, folderNumStr));
    } else {
        const folderItems = await fs.promises.readdir(path.join(monthDateFolder, lastFolderNumStr));

        if (folderItems.length>=MAX_FOLDER_ITEM_CNT) {
            folderNumStr = (Number(lastFolderNumStr)+1).toString();

            // Mkdir if the last folder is full
            await mkdir(path.join(monthDateFolder, folderNumStr));
        } else {
            folderNumStr = lastFolderNumStr;
        }
    }


    /**
     * Generate new file name
     * */
    const folderDir = path.join(monthDateFolder, folderNumStr);

    const folderItems = await fs.promises.readdir(folderDir);
    const fileName = `${folderItems.length+1}_${time.getTimestamp()}.${fileExt}`;
    const filePath = path.join(folderDir, fileName);


    /**
     * Write file
     * */
    await fs.promises.writeFile(filePath, imgFile.buffer);


    const fileDir = path.join(year, monthDate, folderNumStr);

    return {
        fileDir: fileDir,
        fileName: fileName,
        mimeType: imgFile.mimetype,
        fileExt: fileExt
    }
}



/**
 * Resize image buffer
 * */
exports.resizeImgBuffer = async (buffer, size) => {
    return await sharp(buffer).resize({width: size, withoutEnlargement: true}).toBuffer();
}
