const {paramCheck, paramType} = require("../../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../../utils/http/response");
const ComicsCollection = require("../../../../models/comics");
const ComicBookCollection = require("../../../../models/comicBook");
const PostCollection = require("../../../../models/post");
const ImageCollection = require("../../../../models/image");
const _ = require("underscore");
const path = require("path");
const {getComicBookPageFolderDir} = require("../../../../utils/index/comicBook");
const {deleteFile} = require("../../../../utils/file");
const {COMIC_BOOK_IMAGE_STATUS} = require("../../../../utils/index/comicBook");
const {COMIC_BOOK_CONTENT_TYPE} = require("../../../../utils/index/comicBook");
const {SITE_CATEGORY_INDEX} = require("../../../../utils/index/siteIndex");
const {SITE_BOARD_INDEX} = require("../../../../utils/index/siteIndex");
const {POST_STATUS} = require("../../../../utils/index/post");
const {COMIC_BOOK_STATUS} = require("../../../../utils/index/comicBook");



/**
 * Comic book edit page
 * */
exports.comicBookEditPage = async (req, res) => {
    const userId = req.userData.id;
    const isRegUser = req.isRegUser;


    if (!isRegUser) return httpError(res, 403);


    const {comicBookDataId} = req.query;


    if (!paramCheck(comicBookDataId)) return httpError(res, 400);


    /**
     * Find comic book data
     * */
    const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

    if (!comicBookData) return httpError(res, 404);

    const {index, owner, status} = comicBookData;


    if (
        owner.userId!==userId ||
        status!==COMIC_BOOK_STATUS.PUBLISH
    ) return httpError(res, 403);


    /**
     * Find comics data
     * Find post data
     * */
    const comicsData = await ComicsCollection.findComicsDataWithDataId(index.comicsDataId);
    const postData = await PostCollection.findPostWithPostOid(index.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code, POST_STATUS.PUBLISH);



    /**
     * Find previously editing data
     * */
    const previouslyEditingData = await ComicBookCollection.findComicBookDataWithIndexInfo(index, COMIC_BOOK_STATUS.EDITING);

    /**
     * Previously editing data exists,
     * Delete temp(writing) files and data
     * */
    let comicBookEditData;

    if (previouslyEditingData) {
        const {linkThumbnails} = previouslyEditingData;


        switch (index.contentType) {
            case COMIC_BOOK_CONTENT_TYPE.LINK:
                if (!_.isEmpty(linkThumbnails)) {
                    const linkThumbnail = linkThumbnails.find(info => info.status===COMIC_BOOK_IMAGE_STATUS.WRITING);

                    if (linkThumbnail) {
                        for (const imgId of Object.values(linkThumbnail._doc.imgId)) {
                            const imgData = await ImageCollection.findImageById(imgId);
                            const {file} = imgData;
                            const filePath = path.join(getComicBookPageFolderDir(), file.directory, file.name);

                            await deleteFile(filePath);
                        }
                    }
                }
                break;
        }

        await previouslyEditingData.replaceComicBookDataInfo(comicBookData, COMIC_BOOK_STATUS.EDITING);

        comicBookEditData = comicBookData._doc;
        comicBookEditData._id = previouslyEditingData._id;


        const publishedLinkThumbnail = linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);

        if (publishedLinkThumbnail) {
            publishedLinkThumbnail.status = COMIC_BOOK_IMAGE_STATUS.WRITING;
            comicBookEditData.linkThumbnails = [publishedLinkThumbnail];
        }

    }
    /**
     * No previously editing data
     * Create one
     * */
    else {
        comicBookEditData = await ComicBookCollection.createComicBookEditData(comicBookData);

        const publishedLinkThumbnail = comicBookEditData.linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);

        if (publishedLinkThumbnail) {
            publishedLinkThumbnail.status = COMIC_BOOK_IMAGE_STATUS.WRITING;
            comicBookEditData.linkThumbnails = [publishedLinkThumbnail];
        }
    }


    return httpRespond(res, 200, {
        user: req.userData,
        comics: comicsData,
        comicBook: comicBookEditData,
        post: postData
    })
}