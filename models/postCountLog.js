// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
//
//
// const PostCountLogSchema = new Schema({
//   category: String,
//   board: String,
//   count: {
//     all: {type: Number, default: 0},
//     published: {type: Number, default: 0},
//     deleted: {type: Number, default: 0}
//   }
// })
//
//
//
// PostCountLogSchema.index({category: 1, board: 1});
//
//
//
//
//
//
// /**
//  * Find and create each category/board record
//  * */
// PostCountLogSchema.statics.checkAndCreateRecord = async function (category, board) {
//   const record = await this.findOne({
//     category: category,
//     board: board
//   })
//
//   if (!record) {
//     await this.create({
//       category: category,
//       board: board
//     })
//   }
// }
//
//
// /**
//  * Add publish post count
//  * */
// PostCountLogSchema.statics.increasePublishedPostCount = async function (category, board) {
//   await this.updateOne({
//     category: category,
//     board: board
//   }, {
//     $inc: {
//       "count.all": 1,
//       "count.published": 1
//     }
//   })
// }
//
//
// /**
//  * Add deleted post count
//  * */
// PostCountLogSchema.statics.increaseDeletedPostCount = async function (category, board) {
//   await this.updateOne({
//     category: category,
//     board: board
//   }, {
//     $inc: {
//       "count.published": -1,
//       "count.deleted": 1,
//     }
//   })
// }
//
//
//
// module.exports = mongoose.model('post_count_log', PostCountLogSchema);