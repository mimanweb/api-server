const express = require('express');
const router = express.Router();
const fileController = require("./file.controller")
const comicsFile = require("./comicsFile.controller")
const comicBookFile = require("./comicBookFile.controller");


router.get("/post/*/*", fileController.servePostImageFile);
router.get("/comment/*/*", fileController.serveCommentImageFile);


router.get("/comics/cover/*/*", comicsFile.serveComicsCoverFile);
router.get("/comic-book/page/*/*/*", comicBookFile.serveComicBookPageImage);


// router.get("/comic-book/temp/*/*", comicBookFile.serveComicBookTempPageImage);

// router.get("/comic-book/link-thumbnail/*/*/*/*/*", comicBookFile.serveComicBookPageImage);
// router.get("/comic-book/link-thumbnail/*/*/*/*", comicBookFile.serveComicBookPageImage);
// router.get("/comic-book/link-thumbnail/*/*/*", comicBookFile.serveComicBookPageImage);


module.exports = router;