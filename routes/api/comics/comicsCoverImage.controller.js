const {paramCheck} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const {IMAGE_TYPE} = require("../../../utils/index/image");
const ImageCollection = require("../../../models/image");
const ComicsCollection = require("../../../models/comics");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const {parseDocFromModel} = require("../../../utils/mongoose");
const {saveComicsCoverFile} = require("../../../utils/index/comics");



const SIZE = {
    SMALL: {
        value: 128,
        name: "small"
    },
    MEDIUM: {
        value: 256,
        name: "medium"
    },
    LARGE: {
        value: 512,
        name: "large"
    },
    XLARGE: {
        value: 1024,
        name: "xlarge"
    },
}





/**
 * Update Comics Cover Image
 * */
exports.updateComicsCoverImage = async (req, res) => {
    const userId = req.userData.id;
    const isRegUser = req.isRegUser;
    const clientIp = req.clientIp;

    if (!isRegUser) return httpError(res, 403);


    const imgFile = req.file;
    const {comicsDataId} = req.body;


    if (
        !imgFile ||
        !paramCheck(comicsDataId)
    ) return httpError(res, 400);

    const mimeType = imgFile.mimetype;
    const fileExt = mimeType.split("/").pop();


    /**
     * Find comics data
     * */
    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);



    /**
     * Check comics cover image folder
     * */
    const {number: comicsNumber, id: comicsId} = comicsData;




    /**
     * Save comics all sizes of cover
     * */
    const [smallSizeCoverFolder, smallSizeCoverName] = await saveComicsCoverFile(imgFile, SIZE.SMALL.value);
    const [mediumSizeCoverFolder, mediumSizeCoverName] = await saveComicsCoverFile(imgFile, SIZE.MEDIUM.value);
    const [largeSizeCoverFolder, largeSizeCoverName] = await saveComicsCoverFile(imgFile, SIZE.LARGE.value);
    const [xLargeSizeCoverFolder, xLargeSizeCoverName] = await saveComicsCoverFile(imgFile, SIZE.XLARGE.value);



    const createImageData = async (size, folder, fileName) => {
        const latestImageData = parseDocFromModel(await ImageCollection.findLatestImage());
        const newImgDataNumber = !latestImageData? 0: latestImageData.number+1;
        const imgId = `${IMAGE_TYPE.COMICS.COVER}/${newImgDataNumber}/${comicsId}_cover_${size}.${fileExt}`;

        await ImageCollection.createImage(imgId, newImgDataNumber, [IMAGE_TYPE.COMICS.COVER, size].join("/"), userId, clientIp, folder, fileName, mimeType, undefined, undefined);

        return imgId;
    }

    const smallSizeCoverImgId = await createImageData(SIZE.SMALL.name, smallSizeCoverFolder, smallSizeCoverName);
    const mediumSizeCoverImgId = await createImageData(SIZE.MEDIUM.name, mediumSizeCoverFolder, mediumSizeCoverName);
    const largeSizeCoverImgId = await createImageData(SIZE.LARGE.name, largeSizeCoverFolder, largeSizeCoverName);
    const xLargeSizeCoverImgId = await createImageData(SIZE.XLARGE.name, xLargeSizeCoverFolder, xLargeSizeCoverName);


    /**
     * Make an archive of comics data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(userId, comicsData);

    /**
     * Update comics data
     * */
    await ComicsCollection.updateComicsCoverImageInfo(comicsDataId, smallSizeCoverImgId, mediumSizeCoverImgId, largeSizeCoverImgId, xLargeSizeCoverImgId);


    return httpRespond(res, 200);
}