const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");


const PostViewSchema = new Schema({
  postNumber: Number,
  viewer: {
    userId: {type: String, index: true, sparse: true},
    ipAddress: {type: String, index: true, sparse: true}
  },
  time: {
    lastView: Date
  }
});


PostViewSchema.index({"postNumber": -1, "time.lastView": -1});



PostViewSchema.statics.createPostViewLog = async function (postNumber, userId, ipAddress) {
  if (userId) {
    await this.create({
      "postNumber": postNumber,
      "viewer.userId": userId,
      "time.lastView": time.now()
    })
  } else {
    await this.create({
      "postNumber": postNumber,
      "viewer.ipAddress": ipAddress,
      "time.lastView": time.now()
    })
  }
}


PostViewSchema.statics.findPostViewLog = async function (postNumber, userId, ipAddress) {
  if (userId) {
    return await this.findOne({
      "postNumber": postNumber,
      "viewer.userId": userId,
      "viewer.ipAddress": {$exists: false}
    }).sort({
      "time.lastView": -1
    })
  } else {
    return await this.findOne({
      "postNumber": postNumber,
      "viewer.userId": {$exists: false},
      "viewer.ipAddress": ipAddress
    }).sort({
      "time.lastView": -1
    })
  }
}


PostViewSchema.methods.updateViewTime = async function () {
  await this.updateOne({
    "time.lastView": time.now()
  })
}



module.exports = mongoose.model('post_view_log', PostViewSchema);
