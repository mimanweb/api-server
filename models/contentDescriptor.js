const mongoose = require('mongoose');
const Schema = mongoose.Schema;




const ContentDescriptorSchema = new Schema({
    name: {
        korean: String,
        english: String
    },
    code: String
})



ContentDescriptorSchema.statics.findAllContentDescriptors = async function () {
    return await this.find({});
}



module.exports = mongoose.model('content_descriptor', ContentDescriptorSchema);