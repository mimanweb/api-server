const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsCollection = require("../../../models/comics");
const ComicBookCollection = require("../../../models/comicBook");
const ComicBookDailyViewCountLogCollection = require("../../../models/comicBookDailyViewLog");
const CommentCollection = require("../../../models/comment");
const CommentUpvoteCollection = require("../../../models/commentUpvote");
const PostCollection = require("../../../models/post");
const _ = require("underscore");
const {SITE_BOARD_INDEX, SITE_CATEGORY_INDEX} = require("../../../utils/index/siteIndex");
const {COMIC_BOOK_STATUS} = require("../../../utils/index/comicBook");
const {POST_STATUS} = require("../../../utils/index/post");
const time = require("../../../utils/time");
const {formatCommentList} = require("../../../utils/index/comment");
const {formatBestCommentList} = require("../../../utils/index/comment");
const {formatPostData} = require("../../../utils/index/post");
const {COMMENT_PER_PAGE_COUNT} = require("../../../utils/index/comment");




/**
 * Comic book writing page
 * */
exports.comicBookEditorPage = async (req, res) => {
  if (!req.isRegUser) return httpError(res, 403);

  const {comicsDataId} = req.query;

  const {id: userId} = req.userData;
  const clientIp = req.clientIp;
  const deviceUUID = req.session.UUID;


  /**
   * Find comics data
   * */
  const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

  if (!comicsData) return httpError(res, 404);


  /**
   * Check if a previously on-writing data(temp comic book data) of the target comics exists
   * */
  const comicBookDataList = await ComicBookCollection.findAllComicBookDataWithLinkedComicsDataId(userId, comicsData._id);


  const isDataExist = !_.isEmpty(comicBookDataList);
  const prevWritingData = isDataExist? comicBookDataList.find(data => data._doc.status===COMIC_BOOK_STATUS.WRITING): null;
  const publishedComicBooks = isDataExist? comicBookDataList.filter(data => data._doc.status===COMIC_BOOK_STATUS.PUBLISH): [];



  /**
   * Remove writers, artists, genres etc that only with data id data
   * To prevent conflict with those pages supposed to receive in obj form of data
   * */
  comicsData.writers = [];
  comicsData.artists = [];
  comicsData.genres = [];
  comicsData.contentDescriptors = [];
  comicsData.publisher = null;


  /**
   * No previously writing data,
   * Create a new one
   * */
  if (
      !isDataExist ||
      (isDataExist && !prevWritingData)
  ) {
    const tempPostData = await PostCollection.createTempPost(userId, deviceUUID, clientIp, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code);

    const newComicBookData = await ComicBookCollection.createTempComicBookData(userId, tempPostData._id, comicsData._id);

    return httpRespond(res, 203, {
      user: req.userData,
      comics: comicsData,
      comicBook: newComicBookData,
      publishedComicBooks: publishedComicBooks,
      post: tempPostData
    })
  }
  /**
   * Previously writing data exists,
   * Pass along
   * */
  else if (isDataExist && prevWritingData) {
    const postData = await PostCollection.findPostWithPostOid(prevWritingData.index.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code, POST_STATUS.WRITING);

    return httpRespond(res, 200, {
      user: req.userData,
      comics: comicsData,
      comicBook: prevWritingData,
      publishedComicBooks: publishedComicBooks,
      post: postData
    })
  }

}



/**
 * Comic book page
 * */
exports.comicBookPage = async (req, res) => {
  const {id: userId} = req.userData;
  const clientIp = req.clientIp;

  let {comicsNumber, comicsId, translator, issue, volume, shortStoryNumber} = req.query;


  if (
    !paramCheck(comicsNumber, paramType.Number) ||
    !paramCheck(comicsId) ||
    !paramCheck(translator) ||
    (issue && !paramCheck(issue, paramType.Number)) ||
    (volume && !paramCheck(volume, paramType.Number)) ||
    (shortStoryNumber && !paramCheck(shortStoryNumber, paramType.Number))
  ) return httpError(res, 400);

  comicsNumber = Number(comicsNumber);
  volume = Number(volume);
  issue = Number(issue);
  shortStoryNumber = Number(shortStoryNumber);


  const isNumeric = (val) => {
    return typeof val==="number" && !isNaN(val);
  }


  /**
   * Check comic book type
   * */
  const findShortStory = !isNumeric(issue) && !isNumeric(volume) && isNumeric(shortStoryNumber);
  const findLongStoryWithVolume = isNumeric(volume) && isNumeric(issue) && !isNumeric(shortStoryNumber);
  const findLongStoryWithIssue = !isNumeric(volume) && isNumeric(issue) && !isNumeric(shortStoryNumber);

  if (!findShortStory && !findLongStoryWithIssue && !findLongStoryWithVolume) return httpError(res, 400);


  /**
   * Find comics data
   * */
  const comicsData = await ComicsCollection.findComicsData(comicsNumber, comicsId);

  if (!comicsData) return httpError(res, 404);

  const {_id: comicsDataId} = comicsData;


  /**
   * Find comic book data
   * */
  let comicBookDataList = await ComicBookCollection.findAllComicBookDataWithLinkedComicsDataId(translator, comicsDataId);
  comicBookDataList = [...comicBookDataList].map(book => book._doc);


  let comicBookData;

  if (findShortStory) comicBookData = comicBookDataList.find(book => book.index.isShortStory && book.index.shortStoryNumber===shortStoryNumber && book.status===COMIC_BOOK_STATUS.PUBLISH);
  else if (findLongStoryWithVolume) comicBookData = comicBookDataList.find(book => !book.index.isShortStory && book.index.isVolume && book.index.volume===volume && book.index.issue===issue && book.status===COMIC_BOOK_STATUS.PUBLISH);
  else if (findLongStoryWithIssue) comicBookData = comicBookDataList.find(book => !book.index.isShortStory && !book.index.isVolume && book.index.issue===issue && book.status===COMIC_BOOK_STATUS.PUBLISH);


  if (!comicBookData) return httpError(res, 404);

  const {_id: comicBookDataId, index: comicBookIndex} = comicBookData;



  /**
   * Find comic book post data
   * */
  const postData = await PostCollection.findPostWithPostOid(comicBookIndex.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code, POST_STATUS.PUBLISH);


  /**
   * If post data is none,
   * Consider that is duplicated comic-book data and in conflict with DELETED comic-book data that has the same _id
   * Make the comic-book data DELETED
   * */
  if (!postData) {
      await ComicBookCollection.deleteComicBookDataWithDataId(comicBookDataId);

      return httpError(res, 410);
  }


  const {number: postNumber} = postData;


  /**
   * Find post comment data and comment paging info
   * Find comment data
   * Format post, comment data
   * */
  const commentData = await CommentCollection.findCommentListAndPagingInfo(postNumber, 1, undefined, COMMENT_PER_PAGE_COUNT);
  const upvotedUpvoteList = await CommentUpvoteCollection.findUpvotedListByPostAndUser(postNumber, userId, clientIp);

  const {bestComments, pageComments, totalCommentCount, totalPageCount: totalCommentPageCount, searchedPage: searchedCommentPage} = commentData;

  await formatPostData(req, postData, true);
  formatCommentList(req, pageComments, bestComments, upvotedUpvoteList);
  formatBestCommentList(bestComments, userId, req.clientIp, upvotedUpvoteList);



  /**
   * Record comic book view log
   * */
  const viewCountLogData = await ComicBookDailyViewCountLogCollection.findComicBookDailyViewLog(comicBookDataId, userId, clientIp);


  if (
      (viewCountLogData && time.checkIsBeforeTodayTime(viewCountLogData.time)) ||
      !viewCountLogData
  ) {
    await ComicBookDailyViewCountLogCollection.createComicBookDailyViewLog(comicBookDataId, userId, clientIp);
    await ComicBookCollection.increaseComicBookViewCount(comicBookDataId);
  }



  return httpRespond(res, 200, {
    user: req.userData,
    comics: comicsData,
    comicBook: comicBookData,
    post: postData,
    comment: {
      itemsPerPageCount: COMMENT_PER_PAGE_COUNT,
      totalPageCount: totalCommentPageCount,
      pageComments: pageComments,
      bestComments: bestComments,
      searchedPage: searchedCommentPage,
      totalCommentCount: totalCommentCount,
    },
  })
}