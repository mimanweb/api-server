const mongoose = require('mongoose');
const time = require("../utils/time");


const Schema = mongoose.Schema;


const ImageSchema = new Schema({
  number: Number,
  id: String,
  type: String,
  file: {
    width: Number,
    height: Number,
    mime: String,
    directory: String,
    name: String
  },
  status: {
    publish: {type: Boolean, default: true}
  },
  uploader: {
    userId: String,
    ipAddress: String
  },
  time: {
    created: Date,
  }
});


ImageSchema.index({"number": -1});

ImageSchema.index({"id": 1}, {unique: true});

ImageSchema.index({"id": 1, "type": 1, "status.publish": 1, "time.created": -1}, {partialFilterExpression: {"status.publish": {$exists: true}}});



/**
 * Find Latest Image
 * */
ImageSchema.statics.findLatestImage = async function () {
  return await this.findOne().sort({number: -1});
}


/**
 * Find image by id
 * */
ImageSchema.statics.findImageWithNumber = async function (number) {
  return await this.findOne({"number": Number(number), "status.publish": {$exists: true}}).sort({"time.created": -1});
}


/**
 * Find image by id
 * */
ImageSchema.statics.findImageById = async function (imgId, type) {
  const param = {"id": imgId, "status.publish": {$exists: true}};

  if (type) param.type = type;

  return await this.findOne(param).sort({"time.created": -1});
}



/**
 * Create image
 * */
ImageSchema.statics.createImage = async function (imgId, imgNumber, type, userId, ipAddress, fileDir, fileName, mime, width, height) {
  if (imgNumber===null || imgNumber===undefined) {
    const latestData = await this.findOne().sort({number: -1});

    imgNumber = latestData.number+1;
  }

  return await this.create({
    number: imgNumber,
    id: imgId,
    type: type,
    file: {
      width: width,
      height: height,
      mime: mime,
      directory: fileDir,
      name: fileName
    },
    uploader: {
      userId: userId,
      ipAddress: ipAddress
    },
    time: {
      created: time.now(),
    }
  });
}


/**
 * Delete image
 * */
ImageSchema.methods.deleteImageData = async function () {
  await this.updateOne({
    "status.publish": false
  })
}



module.exports = mongoose.model('image', ImageSchema);