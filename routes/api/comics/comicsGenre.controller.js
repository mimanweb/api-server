const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const GenreCollection = require("../../../models/genre");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const mongoose = require("mongoose");



/**
 * Find all Comics Genres
 * */
exports.findAllComicsGenres = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const genreList = await GenreCollection.findAllGenres();

    return httpRespond(res, 200, {genreList: genreList});
}


/**
 * Add Comics Genre to Comics data
 * */
exports.addComicsGenre = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, genreDataId} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(genreDataId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);

    const comicsGenreToAdd = mongoose.Types.ObjectId(genreDataId);

    if (comicsData.genres.includes(comicsGenreToAdd)) return httpError(res, 400);


    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    comicsData.genres.push(comicsGenreToAdd);

    await ComicsCollection.updateComicsGenres(comicsDataId, comicsData.genres);

    return httpRespond(res, 200);
}