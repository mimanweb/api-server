const CommentUpvoteCollection = require("../../models/commentUpvote");
const {trimIp} = require("../client");




const COMMENT_TYPE = {
    COMMENT: "COMMENT",
    REPLY_COMMENT: "REPLY_COMMENT"
}

const COMMENT_STATUS = {
    PUBLISH: "PUBLISH",
    OWNER_DELETED: "OWNER_DELETED"
}

exports.COMMENT_TYPE = COMMENT_TYPE;
exports.COMMENT_STATUS = COMMENT_STATUS;

exports.BEST_COMMENT_MIN_UPVOTE_COUNT_CRIT = 2;
exports.BEST_COMMENT_COUNT = 5;
exports.COMMENT_PER_PAGE_COUNT = 100;



const formatCommentData = (req, comment, bestComments, upvoteList) => {
    const {isRegUser, userData, clientIp: ipAddress} = req;
    const userId = userData.id;


    const isRegUserComment = comment.owner.userId;

    // Check is best comment qualified
    if (bestComments && bestComments.find(bestComment => bestComment.number===comment.number)) comment.isBestComment = true;

    // Check is upvoted comment by curr user
    if (
      !comment.isDeleted &&
      upvoteList &&
      upvoteList.find(upvote => upvote.commentNumber===comment.number)
    ) comment.isUpvoted = true;

    // Check is curr user's comment
    if (
      !comment.isDeleted &&
      (isRegUser && isRegUserComment && comment.owner.userId===userId) ||
      (!isRegUser && !isRegUserComment && comment.owner.ipAddress===ipAddress)
    ) comment.isMyComment = true;

    // Trim ip address
    comment.owner.ipAddress = trimIp(comment.owner.ipAddress);

    // Fill image url
    if (comment.content.imageId) comment.content.imageURL = `${process.env["IMG_SERVER_URL"]}/${comment.content.imageId}`;

    delete comment._id;
    delete comment.owner.password;
    delete comment.owner.deviceUUID;

    if (comment.isDeleted) {
        comment.owner = {};
        comment.content = {};
    }

    if (comment.type===COMMENT_TYPE.REPLY_COMMENT && comment.repliedComment) {
        if (comment.repliedComment.status===COMMENT_STATUS.OWNER_DELETED) {
            comment.repliedComment.owner = {};
            comment.repliedComment.isDeleted = true;
        } else {
            comment.repliedComment.owner.ipAddress = trimIp(comment.repliedComment.owner.ipAddress);
        }

        delete comment.repliedComment._id;
        delete comment.repliedComment.owner.password;
        delete comment.repliedComment.owner.deviceUUID;
    }
}



const formatCommentList = (req, list, bestComments, upvoteList) => {
    list.forEach(comment => {
        formatCommentData(req, comment, bestComments, upvoteList);
    })
}



const formatBestCommentList = (list, userId, ipAddress, upvoteList) => {
    const isRegUser = userId;

    list.forEach(comment => {
        const isRegUserComment = comment.owner.userId;

        // Check is upvoted comment by curr user
        if (upvoteList.find(upvote => upvote.commentNumber===comment.number)) comment.isUpvoted = true;

        // Check is curr user's comment
        if (
            (isRegUser && isRegUserComment && comment.owner.userId===userId) ||
            (!isRegUser && !isRegUserComment && comment.owner.ipAddress===ipAddress)
        ) comment.isMyComment = true;

        // Trim ip address
        comment.owner.ipAddress = trimIp(comment.owner.ipAddress);

        // Fill image url
        if (comment.content.imageId) comment.content.imageURL = `${process.env["IMG_SERVER_URL"]}/${comment.content.imageId}`;

        delete comment._id;
        delete comment.owner.password;
        delete comment.owner.deviceUUID;
        delete comment.repliedComment;
    })
}




exports.formatCommentData = formatCommentData;
exports.formatCommentList = formatCommentList;
exports.formatBestCommentList = formatBestCommentList;

