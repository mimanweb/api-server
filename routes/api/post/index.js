const express = require('express');
const router = express.Router();
const post = require("./post.controller");
const postUpvote = require("./postUpvote.controller");
const editor = require("./editor.controller");

const {verifyLogin} = require('../../../utils/auth/login');


router.use(verifyLogin);

router.post("/", post.publishPost);
router.put("/", post.makeArchivePostAndUpdate);
router.delete("/", post.deletePost);
router.get("/temp", post.findPreviouslySavedTempPost);
router.post("/temp", post.createTempPost);
router.put("/temp", post.updateTempPost);
router.get("/ownership", post.checkPostOwnership);
router.post("/upvote", postUpvote.updatePostUpvoteCount);
router.post("/page/number", post.findPostListAndPageInfoOfPageNumber);
router.post("/link/title", editor.linkTitle);


module.exports = router;