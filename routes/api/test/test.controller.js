const { v4: uuidv4 } = require('uuid');
const uuid = uuidv4();
const User = require("../../../models/user")
const time = require("../../../utils/time")
const moment = require("moment")
const axios = require("axios");


exports.uuid = async (req, res) => {
    res.status(200).send(`1 ${uuid}`)
};

exports.query = async (req, res) => {
    const re = await axios.post("https://bbs.ruliweb.com/best");
    console.log(re)

    return res.end()
}

exports.message = async (req, res) => {
    const d = new Date();
    d.setHours(0, 0, 0, 0)

    res.send({
        message: "some random message",
        "xForwardHeader": req.headers["x-forwarded-for"],
        ip: req.ip,
        clientIp: req.clientIp,
        "remoteAddress": req.connection.remoteAddress,
        "now": Date.now(),
        "Date": new Date(),
        "DateTimezoneOffset": new Date().getTimezoneOffset(),
        "DateSetHour0": d,
        "momentUtcOffset": moment().utcOffset()/60,
        "moment": moment(),
        "momentSub9hour": moment().subtract(9, "hour"),
        "momentTodayStart": time.todayStart(),
        "momentStartOf": moment().startOf("day"),
    })
}