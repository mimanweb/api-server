const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");
const {COMIC_BOOK_IMAGE_STATUS} = require("../utils/index/comicBook");
const {COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_STATUS} = require("../utils/index/comicBook");
const {parseDataId} = require("../utils/mongoose");
const _ = require("underscore");






const ComicBookSchema = new Schema({
  status: String,
  owner: {
    userId: String,
    nickname: String,
  },
  index: {
    postDataId: Schema.Types.ObjectID,
    comicsDataId: Schema.Types.ObjectID,
    contentType: {type: String, default: COMIC_BOOK_CONTENT_TYPE.LINK},
    isTitle: {type: Boolean, default: false},
    isShortStory: {type: Boolean, default: false},
    isVolume: {type: Boolean, default: false},
    volume: {type: Number, default: 1},
    issue: {type: Number, default: 1},
    shortStoryNumber: Number
  },
  record: {
    viewCount: {type: Number, default: 0}
  },
  title: {type: String, default: ""},
  pages: [
    {
      status: String,
      pageNumber: Number,
      pageType: String,
      pairNumber: Number,
      imgId: {
        //128
        small: String,
        //256
        medium: String,
        //512
        large: String,
        //1024
        xLarge: String,
        //2048
        xxLarge: String
      }
    }
  ],
  link: String,
  linkThumbnails: [
    {
      status: String,
      imgId: {
        //128
        small: String,
        //256
        medium: String,
        //512
        large: String,
        //1024
        xLarge: String,
        //2048
        xxLarge: String
      }
    }
  ],
  summary: String,
  time: {
    created: Date,
    updated: Date,
    publish: Date,
    deleted: Date
  }
})





/**
 * Find comic book data with data id
 * */
ComicBookSchema.statics.findComicBookDataWithDataId = async function (comicBookDataId) {
  return await this.findOne({
    _id: parseDataId(comicBookDataId)
  });
}


/**
 * Find comic book data with index info
 * */
ComicBookSchema.statics.findComicBookDataWithIndexInfo = async function (comicBookIndex, status) {
  const {isShortStory, isVolume, volume, issue, shortStoryNumber} = comicBookIndex;

  const param = {
    "index.isShortStory": isShortStory,
      "index.isVolume": isVolume,
      "index.volume": volume,
      "index.issue": issue,
      "index.shortStoryNumber": shortStoryNumber,
  }

  if (status) param.status = status;

  return await this.findOne(param);
}


/**
 * Find comic book data linked to a specific comics data
 * */
ComicBookSchema.statics.findComicBookDataWithLinkedComicsDataId = async function (userId, comicsDataId) {
  return await this.findOne({
    "owner.userId": userId,
    "index.comicsDataId": parseDataId(comicsDataId)
  });
}


/**
 * Find all comic book data linked to a specific comics data
 * */
ComicBookSchema.statics.findAllComicBookDataWithLinkedComicsDataId = async function (userId, comicsDataId, status) {
  const param = {
    "index.comicsDataId": parseDataId(comicsDataId)
  }

  if (userId) param["owner.userId"] = userId;

  if (status) param.status = status;

  return await this.find(param);
}


/**
 * Find most recently published comic book data
 * Group by comics
 * */
ComicBookSchema.statics.findLatestComicBookGroupByComics = async function (count) {
  return await this.aggregate([
    {
      '$match': {
        'status': COMIC_BOOK_STATUS.PUBLISH,
        "linkThumbnails.status": COMIC_BOOK_IMAGE_STATUS.PUBLISH
      }
    }, {
      '$sort': {
        'time.publish': -1
      }
    }, {
      '$group': {
        '_id': '$index.comicsDataId',
        'data': {
          '$first': '$$ROOT'
        }
      }
    }, {
      '$limit': count
    }, {
      '$replaceRoot': {
        'newRoot': '$data'
      }
    }, {
      $lookup: {
        from: 'comics',
        localField: 'index.comicsDataId',
        foreignField: '_id',
        as: 'comics'
      }
    }, {
      $addFields: {
        comics: {$first: "$comics"}
      }
    }
  ]) || [];
}


/**
 * Create temp(writing) comic book data
 * */
ComicBookSchema.statics.createTempComicBookData = async function (userId, postDataId, comicsDataId) {
  return await this.create({
    status: COMIC_BOOK_STATUS.WRITING,
    owner: {
      userId: userId
    },
    index: {
      postDataId: postDataId,
      comicsDataId: comicsDataId,
      contentType: COMIC_BOOK_CONTENT_TYPE.LINK
    },
    "time.created": time.now()
  })
}


/**
 * Create comic book edit data
 * */
ComicBookSchema.statics.createComicBookEditData = async function (publishedData) {
  const doc = publishedData._doc? publishedData._doc: publishedData;

  delete doc._id;

  doc.status = COMIC_BOOK_STATUS.EDITING;

  return await this.create(doc);
}


/**
 * Add temp(writing) page image
 * */
ComicBookSchema.methods.updateComicBookPageList = async function (pageList) {
  await this.updateOne({
    pages: pageList,
    "time.updated": time.now()
  })
}


/**
 * Add temp(writing) link-thumbnail image
 * */
ComicBookSchema.methods.updateComicBookLinkThumbnailList = async function (thumbnailList) {
  await this.updateOne({
    linkThumbnails: thumbnailList,
    "time.updated": time.now()
  })
}


/**
 * Update comic book info
 * */
ComicBookSchema.methods.updateComicBookInfo = async function (title, volume, issue, contentType, isTitle, isShortStory, isVolume, link) {
  await this.updateOne({
    title: title || "",
    link: link || null,
    "index.isShortStory": isShortStory || false,
    "index.contentType": contentType || COMIC_BOOK_CONTENT_TYPE.LINK,
    "index.isTitle": isTitle || false,
    "index.isVolume": isVolume || false,
    "index.volume": volume || 0,
    "index.issue": issue || 0,
  })
}



/**
 * Replace all info
 * */
ComicBookSchema.methods.replaceComicBookDataInfo = async function (dataToReplaceWith, status) {
  const doc = dataToReplaceWith._doc? _.clone(dataToReplaceWith._doc): _.clone(dataToReplaceWith);

  delete doc._id;

  doc.status = status;
  doc.time.updated = time.now();
  await this.updateOne(doc);
}


/**
 * Publish comic book data
 * */
ComicBookSchema.methods.publishComicBookData = async function (nickname, newData) {
  const {index, pages, linkThumbnails} = newData;
  const {contentType, shortStoryNumber, isVolume, isShortStory, isTitle} = index;

  const isImageContent = contentType===COMIC_BOOK_CONTENT_TYPE.IMAGE;


  const params = {
    "owner.nickname": nickname,
  }

  if (!isTitle) params["index.title"] = "";

  if (isShortStory) {
    params["index.volume"] = 0;
    params["index.issue"] = 0;
    params["index.shortStoryNumber"] = shortStoryNumber;
  }

  if (!isShortStory && !isVolume) params["index.volume"] = 0;

  if (isImageContent) {
    params["pages"] = pages;
    params["link"] = "";
    params["linkThumbnails"] = [];
  } else {
    params["pages"] = [];
    params["linkThumbnails"] = linkThumbnails;
  }


  params["status"] = COMIC_BOOK_STATUS.PUBLISH;
  params["time.publish"] = time.now();

  await this.updateOne(params);
}



/**
 * Increase comic book view count
 * */
ComicBookSchema.statics.increaseComicBookViewCount = async function (comicBookDataId) {
  await this.updateOne({_id: parseDataId(comicBookDataId)},{$inc: {"record.viewCount": 1}})
}


/**
 * Delete link-content comic book
 * */
ComicBookSchema.methods.deleteLinkContentComicBookData = async function (linkThumbnailInfo) {
  const param = {
    "status": COMIC_BOOK_STATUS.DELETED
  }

  if (linkThumbnailInfo) {
    linkThumbnailInfo.status = COMIC_BOOK_IMAGE_STATUS.DELETED;
    param["linkThumbnails"] = [linkThumbnailInfo];
  }

  await this.updateOne(param);
}


/**
 * Delete comic book data
 * */
ComicBookSchema.statics.deleteComicBookDataWithDataId = async function (dataId) {
  await this.updateOne({_id: parseDataId(dataId)}, {status: COMIC_BOOK_STATUS.DELETED});
}



module.exports = mongoose.model('comic_book', ComicBookSchema);