const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const ComicsArtistCollection = require("../../../models/comicsArtist");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const mongoose = require("mongoose");



/**
 * Find all comics artists data
 * Regex match word
 * */
exports.findComicsArtists = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {textToSearch} = req.query;

    if (!paramCheck(textToSearch)) return httpError(res, 400);


    const artistList = await ComicsArtistCollection.findAllComicsArtistsMatchWord(textToSearch);

    if (_.isEmpty(artistList)) return httpError(res, 404);


    return httpRespond(res, 200, {artistList: artistList});
}


/**
 * Add comics artist info
 * */
exports.addComicsArtist = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, comicsArtistDataId} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(comicsArtistDataId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    const comicsArtistList = comicsData.artists;
    const comicsArtistToAdd = mongoose.Types.ObjectId(comicsArtistDataId);

    if (comicsArtistList.includes(comicsArtistToAdd)) return httpError(res, 400);

    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    comicsArtistList.push(comicsArtistToAdd);

    await ComicsCollection.updateComicsArtists(comicsDataId, comicsArtistList);


    return httpRespond(res, 200);
}


/**
 * Add new comics artist info
 * */
exports.addNewComicsArtist = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, artistName} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(artistName) ||
        (artistName && !/[\w]/g.test(artistName))
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    const newComicsArtistData = await ComicsArtistCollection.createComicsArtistData(artistName);
    const comicsArtistList = comicsData.artists;

    comicsArtistList.push(newComicsArtistData._id);

    await ComicsCollection.updateComicsArtists(comicsDataId, comicsArtistList);


    return httpRespond(res, 200);
}