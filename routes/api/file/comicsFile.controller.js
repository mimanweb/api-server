const fs = require('fs');
const path = require("path");
const {httpError, httpRespond} = require("../../../utils/http/response");
const ImageCollection = require("../../../models/image");
const {getComicsCoverFolderDir} = require("../../../utils/index/comics");
const {paramCheck, paramType} = require("../../../utils/paramUtil");




/**
 * Serve comics cover file
 * */
exports.serveComicsCoverFile = async (req, res) => {
    const imgDataNumber = req.params[0];


    if (
        !paramCheck(imgDataNumber, paramType.Number)
    ) return httpError(res, 403);



    const imageData = await ImageCollection.findImageWithNumber(imgDataNumber);

    const {file} = imageData;

    const coverFilePath = path.join(getComicsCoverFolderDir(), file.directory, file.name);


    /**
     * Start read stream
     * */
    const stream = fs.createReadStream(coverFilePath);

    stream.on("open", () => {
        res.set("Content-Type", file.mime);
        stream.pipe(res);
    })

    stream.on("error", (e) => {
        return httpError(res, 404);
    })
}