const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicBookCollection = require("../../../models/comicBook");
const ComicBookArchiveLogCollection = require("../../../models/comicBookArchiveLog");
const PostCollection = require("../../../models/post");
const _ = require("underscore");
const {applyNewContentInfoToPostDataAndMakeArchive} = require("../../../utils/index/post");
const {COMIC_BOOK_IMAGE_STATUS, COMIC_BOOK_PAGE_TYPE, COMIC_BOOK_CONTENT_TYPE, COMIC_BOOK_STATUS} = require("../../../utils/index/comicBook");
const {SITE_CATEGORY_INDEX, SITE_BOARD_INDEX} = require("../../../utils/index/siteIndex");



/**
 * Publish comic book edit data
 * Archive previously published data
 * */
exports.makeArchiveAndUpdateComicBookData = async (req, res) => {
  if (!req.isRegUser) return httpError(res, 403);


  const userId = req.userData.id;


  const {comicBookDataId, postContent="", postPhrase=""} = req.body;


  if (!paramCheck(comicBookDataId)) return httpError(res, 400);


  /**
   * Find comic book edit data
   * */
  const comicBookEditingData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

  if (!comicBookEditingData) return httpError(res, 404);
  if (
      comicBookEditingData.status!==COMIC_BOOK_STATUS.EDITING ||
      comicBookEditingData.owner.userId!==userId
  ) return httpError(res, 403);


  const {index, linkThumbnails} = comicBookEditingData;

  const comicBookEditingDataDoc = comicBookEditingData._doc;


  /**
   * Find post data
   * */
  const postData = await PostCollection.findPostWithPostOid(index.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code);


  /**
   * Find published comic book data
   * */
  const comicBookData = await ComicBookCollection.findComicBookDataWithIndexInfo(index, COMIC_BOOK_STATUS.PUBLISH);

  if (!comicBookData) return httpError(res, 403);



  /**
   * Archive current published comic book, post data
   * */
  await ComicBookArchiveLogCollection.archiveComicBookData(comicBookData);
  await applyNewContentInfoToPostDataAndMakeArchive(postData, undefined, postContent, postPhrase);

  /**
   * Replace current published comic book data with editing data
   * */
  switch (index.contentType) {
    case COMIC_BOOK_CONTENT_TYPE.LINK:
      const writingThumbnail = linkThumbnails.find(thumbnail => thumbnail._doc.status===COMIC_BOOK_IMAGE_STATUS.WRITING);

      if (writingThumbnail) {
        const writingThumbnailDoc = writingThumbnail._doc;

        writingThumbnailDoc.status = COMIC_BOOK_IMAGE_STATUS.PUBLISH;

        comicBookEditingDataDoc.linkThumbnails = [writingThumbnailDoc];
      }
      break;
  }

  await comicBookData.replaceComicBookDataInfo(comicBookEditingDataDoc, COMIC_BOOK_STATUS.PUBLISH);

  return httpRespond(res, 200);
}