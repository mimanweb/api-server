const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const UserCollection = require("../../../models/user");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const {parseDocFromModel} = require("../../../utils/mongoose");
const {COMICS_SEARCH_ITEMS_PER_PAGE} = require("../../../utils/index/comics");
const {createComicsFolder} = require("../../../utils/image");
const {convertComicsTitleIntoDataId, formatComicsDataList} = require("../../../utils/index/comics");






/**
 * Create Comics Data
 * */
exports.createComics = async (req, res) => {
    const isRegUser = req.isRegUser;
    const userId = req.userData.id;
    const {title} = req.body;


    if (!isRegUser) return httpError(res, 403);


    if (
        !paramCheck(title, paramType.Object) ||
        !paramCheck(title.korean) ||
        !paramCheck(title.origin) ||
        !convertComicsTitleIntoDataId(title.origin)
    ) return httpError(res, 400);



    const latestComicsData = await ComicsCollection.findLatestComicsData();

    let newNumber = latestComicsData? latestComicsData.number+1: 1;

    const comicsData = await ComicsCollection.createComicsData(newNumber, convertComicsTitleIntoDataId(title.origin), title, userId);


    await UserCollection.increaseUserComicsRegisterCount(userId);


    return httpRespond(res, 203, {comicsDataOid: comicsData._id});
}


/**
 * Find comics data with title
 * */
exports.findComicsDataWithTitle = async (req, res) => {
    const {title, pageToSearch} = req.query;


    if (
        !paramCheck(title) ||
        (pageToSearch && !paramCheck(pageToSearch, paramType.Number))
    ) return httpError(res, 400);




    const [totalCount, itemsPerPage, searchedPage, comicsList] = await ComicsCollection.findAllComicsDataWithTitle(title, true, COMICS_SEARCH_ITEMS_PER_PAGE, pageToSearch);


    if (_.isEmpty(comicsList)) return httpError(res, 404);

    formatComicsDataList(comicsList);

    return httpRespond(res, 200, {
        comicsList: comicsList,
        totalCount: totalCount,
        itemsPerPage: itemsPerPage,
        searchedPage: searchedPage
    });
}


/**
 * Find all comics match title index character
 * */
exports.findComicsDataMatchTitleIndexChar = async (req ,res) => {
    const {text, pageToSearch} = req.query;


    if (
        (text && !/[\S0-9a-zA-Z가-힣]/g.test(text)) ||
        !paramCheck(pageToSearch, paramType.Number)
    ) return httpError(res, 400);


    const [totalCount, itemsPerPage, searchedPage, comicsList] = await ComicsCollection.findAllComicsDataMatchTitleIndexChar(text, true, undefined, pageToSearch);

    if (!_.isEmpty(comicsList)) formatComicsDataList(comicsList);


    return httpRespond(res, 200, {
        comicsList: comicsList,
        totalCount: totalCount,
        itemsPerPage: itemsPerPage,
        searchedPage: searchedPage
    })
}


/**
 * Find all comics match comics number list
 * Find all latest comic book match comics
 * */
exports.findAllComicsAndLatestUpdateInfo = async (req, res) => {
    let {recentReadList} = req.query;

    if (
        !recentReadList
    ) return httpError(res, 400);

    recentReadList = recentReadList.map(info => JSON.parse(info));


    const comicsNumberList = recentReadList.map(info => info.comicsNumber);

    const comicsList = await ComicsCollection.findAllComicsAndLatestBookWithComicsNumbers(comicsNumberList);


    const matchingComicsDataList = [];

    if (comicsList) {
        recentReadList.forEach(info => {
            const match = comicsList.find(comics => comics.number===info.comicsNumber);

            if (match && match.latestComicBook) {
                match.isRecentReadBook = match.latestComicBook._id.toString() === info.comicBookDataId;

                match.latestComicBook.linkThumbnail = match.latestComicBook.linkThumbnails.shift();

                matchingComicsDataList.push(match);
            }
        })
    }


    return httpRespond(res, 200, {recentReadComicsList: matchingComicsDataList});
}