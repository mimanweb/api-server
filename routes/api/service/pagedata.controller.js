const { httpRespond, httpError } = require('../../../utils/http/response');
const PostCollection = require("../../../models/post");
const PostUpvoteCollection = require("../../../models/postUpvote");
const PostViewLogCollection = require("../../../models/postViewLog");
const CommentCollection = require("../../../models/comment");
const CommentUpvoteCollection = require("../../../models/commentUpvote");
const BoardNoticePostIndexCollection = require("../../../models/boardNoticePostIndex");
const ComicBookCollection = require("../../../models/comicBook");
const {paramCheck, paramType} = require("../../../utils/paramUtil");
const time = require("../../../utils/time");
const {SITE_BOARD_INDEX} = require("../../../utils/index/siteIndex");
const {SITE_CATEGORY_INDEX} = require("../../../utils/index/siteIndex");
const {checkIsUpvotedPost} = require("../../../utils/index/post");
const {checkPostDataOwnership} = require("../../../utils/index/post");
const {BOARD_POST_LIST_TYPE} = require("../../../utils/index/board");
const {POST_STATUS, POST_PER_PAGE, formatPostData, formatPostList} = require("../../../utils/index/post");
const {trimIp} = require("../../../utils/client");
const {COMMENT_PER_PAGE_COUNT, formatCommentList, formatBestCommentList, findFormattedCommentListAndPagingInfo} = require("../../../utils/index/comment");
const _ = require("underscore");



const HOMEPAGE_BEST_HUMOR_POST_COUNT = 8;
const HOMEPAGE_BEST_VARIOUS_POST_COUNT = 8;



/**
 * Homepage
 * */
exports.homePage = async (req, res) => {

    const now = time.now();
    const prev24hourTime = time.getHourSubtractedTime(time.getInitHourTime(now), 24);


    /**
     * Find best humor post list
     * In prev 24 hours
     * Fill the rest with most upvoted with no time boundary if the number not qualified
     * */
    let bestHumorPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.HUMOR.code, HOMEPAGE_BEST_HUMOR_POST_COUNT, 1, undefined, prev24hourTime);

    if (
        _.isEmpty(bestHumorPostList) ||
        bestHumorPostList.length<HOMEPAGE_BEST_HUMOR_POST_COUNT
    ) {
        const humorPostListFromBeforeBaseTime = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.HUMOR.code, HOMEPAGE_BEST_HUMOR_POST_COUNT-bestHumorPostList.length, 2, prev24hourTime, undefined);

        bestHumorPostList = bestHumorPostList.concat(humorPostListFromBeforeBaseTime)
    }



    /**
     * Find best post list from various boards
     * In prev 24 hours
     * Fill the rest with most upvoted with no time boundary if the number not qualified
     * */
    const bestCommunityFoodPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.FOOD.code, 1, 1, undefined, prev24hourTime);
    const bestCommunityHorrorPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.HORROR.code, 1, 1, undefined, prev24hourTime);
    const bestCommunityFreeboardPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.FREEBOARD.code, HOMEPAGE_BEST_VARIOUS_POST_COUNT, 1, undefined, prev24hourTime);
    let bestComicsFreeboardPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.FREEBOARD.code, HOMEPAGE_BEST_VARIOUS_POST_COUNT, 1, undefined, prev24hourTime);
    const bestAnimationFreeboardPostList = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.ANIMATION.code, SITE_BOARD_INDEX.ANIMATION.FREEBOARD.code, HOMEPAGE_BEST_VARIOUS_POST_COUNT, 1, undefined, prev24hourTime);


    const isCommunityFood = !_.isEmpty(bestCommunityFoodPostList);
    const isCommunityHorror = !_.isEmpty(bestCommunityHorrorPostList);
    const isCommunityFreeboard = !_.isEmpty(bestCommunityFreeboardPostList);
    const isAnimationFreeboard = !_.isEmpty(bestAnimationFreeboardPostList);

    let comicsFreeboardCount = 2;

    if (!isCommunityFood) {
        comicsFreeboardCount += 1;
    }
    if (!isCommunityHorror) {
        comicsFreeboardCount += 1;
    }
    if (!isCommunityFreeboard) {
        comicsFreeboardCount += 2;
    }
    else if (bestCommunityFreeboardPostList.length<2) {
        comicsFreeboardCount += 1;
    }
    if (!isAnimationFreeboard) {
        comicsFreeboardCount += 2;
    }
    else if (bestAnimationFreeboardPostList.length<2) {
        comicsFreeboardCount += 1;
    }
    if (comicsFreeboardCount>bestComicsFreeboardPostList.length) {
        const comicsFreeboardListFromAllTime = await PostCollection.findPostListWithUpvoteCountCritInTimeRange(SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.FREEBOARD.code, comicsFreeboardCount-bestComicsFreeboardPostList.length, 1, prev24hourTime, undefined);

        bestComicsFreeboardPostList = bestComicsFreeboardPostList.concat(comicsFreeboardListFromAllTime);
    }


    const bestCombinedBoardPostList = [...bestComicsFreeboardPostList, ...bestAnimationFreeboardPostList, ...bestAnimationFreeboardPostList, ...bestCommunityHorrorPostList, ...bestCommunityFoodPostList];



    /**
     * Find recent post list
     * */
    const communityFreeboardRecentPostList = await PostCollection.findLatestPostList(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.FREEBOARD.code, 8, POST_STATUS.PUBLISH);
    const comicsFreeboardRecentPostList = await PostCollection.findLatestPostList(SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.FREEBOARD.code, 8, POST_STATUS.PUBLISH);



    /**
     * Find most recent notice post
     * */
    const noticePost = await PostCollection.findLatestPostWithSiteIndex(SITE_CATEGORY_INDEX.COMMUNITY.code, SITE_BOARD_INDEX.COMMUNITY.NOTICE.code, POST_STATUS.PUBLISH);


    /**
     * Find most recent comic book data list
     * One each grouped by comics
     * */
    const comicBookList = await ComicBookCollection.findLatestComicBookGroupByComics(6);



    await formatPostData(req, noticePost, false, undefined);
    await formatPostList(req, bestHumorPostList);
    await formatPostList(req, bestCombinedBoardPostList);
    await formatPostList(req, communityFreeboardRecentPostList);
    await formatPostList(req, comicsFreeboardRecentPostList);


    return httpRespond(res, 200, {
        user: req.userData,
        homepage: {
            noticePost: noticePost,
            bestHumorPostList: bestHumorPostList,
            bestCombinedBoardPostList: bestCombinedBoardPostList,
            communityFreeboardRecentPostList: communityFreeboardRecentPostList,
            comicsFreeboardRecentPostList: comicsFreeboardRecentPostList,
            recentComicBookList: comicBookList
        }
    })
};




/**
 * Post page
 * */
exports.postPage = async (req, res) => {
    const userData = req.userData;
    const ipAddress = req.clientIp;
    const userId = userData.id;
    const isRegUser = req.isRegUser;


    const {postNumber, category, board, commentToSearch} = req.query;
    let {listType} = req.query;


    if (
        !paramCheck(postNumber, paramType.Number) ||
        !paramCheck(category, paramType.String) ||
        !paramCheck(board, paramType.String) ||
        (listType && !Object.values(BOARD_POST_LIST_TYPE).some(type => type===listType)) ||
        (commentToSearch && !paramCheck(commentToSearch, paramType.Number))
    ) return httpError(res, 400);




    /**
     * Find post data,
     * Board page info,
     * Board page post list
     * */
    const {searchedPost: postData, pagePostList: boardPostList, totalPageCount: totalPostPageCount, searchedPostPage} = await PostCollection.findPublishedPostListAndPageInfoWithPostNumber(postNumber, category, board, POST_PER_PAGE, listType);

    if (!postData) return httpError(res, 404);



    const {owner: postOwner} = postData;
    const isRegUserPost = postOwner.userId;



    /**
     * Increase post view count,
     * Pass if the last view log is created within 5 seconds,
     * Pass if post owner requested
     * */
    let isPostViewCountIncreased;

    if (!(
      (isRegUser && isRegUserPost && userId===postOwner.userId) ||
      (!isRegUser && !isRegUserPost && ipAddress===postOwner.ipAddress)
    )) {
        const postViewLogData = await PostViewLogCollection.findPostViewLog(postNumber, userId, ipAddress);

        if (!postViewLogData) {
            await PostViewLogCollection.createPostViewLog(postNumber, userId, ipAddress);
            await PostCollection.increaseViewCount(postNumber, category, board);

            isPostViewCountIncreased = true;
        } else {
            const now = time.now();
            const diffInMs = now.diff(postViewLogData.time.lastView);

            if (diffInMs > 5000) {
                await postViewLogData.updateViewTime();
                await PostCollection.increaseViewCount(postNumber, category, board);

                isPostViewCountIncreased = true;
            }
        }
    }



    /**
     * Find post comment data and comment paging info
     * */
    const commentData = await CommentCollection.findCommentListAndPagingInfo(postNumber, !commentToSearch? 1: undefined, commentToSearch? commentToSearch: undefined, COMMENT_PER_PAGE_COUNT);

    const {bestComments, pageComments, totalCommentCount, totalPageCount: totalCommentPageCount, searchedPage: searchedCommentPage} = commentData;


    /**
     * Trim ip address and remove password, deviceUUID in comments
     * Fill best comment id info to comment data
     * Fill image url
     * */
    const upvotedUpvoteList = await CommentUpvoteCollection.findUpvotedListByPostAndUser(postNumber, userId, ipAddress);

    await formatPostData(req, postData, true, isPostViewCountIncreased);
    await formatPostList(req, boardPostList);

    formatCommentList(req, pageComments, bestComments, upvotedUpvoteList);
    formatBestCommentList(bestComments, userId, ipAddress, upvotedUpvoteList);



    /**
     * Find board notice posts
     * */
    const boardNoticePosts = await BoardNoticePostIndexCollection.findBoardNoticePostList(category, board);



    return httpRespond(res, 200, {
        user: userData,
        post: postData,
        comment: {
            itemsPerPageCount: COMMENT_PER_PAGE_COUNT,
            totalPageCount: totalCommentPageCount,
            pageComments: pageComments,
            bestComments: bestComments,
            searchedPage: searchedCommentPage,
            totalCommentCount: totalCommentCount,
        },
        board: {
            postList: boardPostList,
            noticePostList: boardNoticePosts,
            searchedPage: searchedPostPage,
            totalPageCount: totalPostPageCount,
            postPerPage: POST_PER_PAGE
        }
    });
}


/**
 * Post edit page
 * */
exports.postEditPage = async (req, res) => {
    const {postNumber, category, board, postPassword} = req.query;
    const userData = req.userData;


    if (
      !paramCheck(postNumber, paramType.Number) ||
      !paramCheck(category) ||
      !paramCheck(board) ||
      (!req.isRegUser && !paramCheck(postPassword))
    ) return httpError(res, 400);


    const postData = await PostCollection.findPostWithPostNumber(postNumber, category, board, POST_STATUS.PUBLISH);

    // Post has been deleted or archived
    if (!postData) return httpError(res, 404);

    const isRegUserPost = postData.owner.userId;


    if (
      (isRegUserPost && postData.owner.userId!==userData.id) ||
      (!isRegUserPost && postData.owner.password!==postPassword)
    ) return httpError(res, 403);


    return httpRespond(res, 200, {
        user: userData,
        post: {
            postId: postData._id.toString(),
            number: postData.number,
            category: postData.index.category,
            board: postData.index.board,
            title: postData.title,
            content: postData.content,
            phrase: postData.phrase,
            owner: {
                userId: postData.owner.userId,
                nickname: postData.owner.nickname,
                ipAddress: trimIp(postData.owner.ipAddress)
            },
            time: {
                updated: postData.time.updated
            },
            record: {
                upvoteCount: postData.record.upvoteCount
            }
        }
    })

}
