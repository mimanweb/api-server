const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");



const ComicsArchiveLogSchema = new Schema({
    index: {
        userId: String,
        dataId: Schema.Types.ObjectID
    },
    id: String,
    title: {
        korean: String,
        origin: String
    },
    status: String,
    writers: [Schema.Types.ObjectID],
    artists: [Schema.Types.ObjectID],
    publisher: String,
    publishStatus: String,
    genres: [Schema.Types.ObjectID],
    contentDescriptors: [Schema.Types.ObjectID],
    summary: String,
    coverImage: {
        small: String,
        medium: String,
        large: String,
        xLarge: String
    },
    time: {
        created: Date,
        updated: Date,
        approved: Date,
        deleted: Date,
        archived: Date
    }
})




/**
 * Archive Comics data
 * */
ComicsArchiveLogSchema.statics.archiveComicsData = async function (archivedUserId, params) {
    await this.create({
        index: {
            userId: archivedUserId,
            dataId: params._id
        },
        id: params.id,
        title: {
            korean: params.title.korean,
            origin: params.title.origin
        },
        status: params.status,
        writers: params.writers,
        artists: params.artists,
        publisher: params.publisher,
        publishStatus: params.publishStatus,
        genres: params.genres,
        contentDescriptors: params.contentDescriptors,
        summary: params.summary,
        images: params.images,
        time: {
            created: params.time.created,
            updated: params.time.updated,
            approved: params.time.approved,
            deleted: params.time.deleted,
            archived: time.now()
        }
    })
}




module.exports = mongoose.model('comics_archive_log', ComicsArchiveLogSchema);
