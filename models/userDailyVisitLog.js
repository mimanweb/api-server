const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");



const UserDailyVisitLogSchema = new Schema({
    userId: {type: String},
    ipAddress: {type: String},
    createdTime: Date
});


// Index for query unregistered user ip address
UserDailyVisitLogSchema.index({ipAddress: 1, createdTime: -1});
// Index for query registered user by user id
UserDailyVisitLogSchema.index({userId: 1, createdTime: -1}, {partialFilterExpression: {userId: {$exists: true}}});




/**
 * Find unregistered user's today visit log by access IP address
 * */
UserDailyVisitLogSchema.statics.findUnregUserTodayVisitLog = async function (ipAddress) {
    return await this.findOne({
        userId: {$exists: false},
        ipAddress: ipAddress,
        createdTime: {$gte: time.todayStart()}
    })
      .sort({createdTime: -1});
};


/**
 * Find registered user's today visit log
 * */
UserDailyVisitLogSchema.statics.findUserTodayVisitLog = async function (userId) {
    return await this.findOne({
        userId: userId,
        createdTime: {$gte: time.todayStart()}
    })
      .sort({createdTime: -1})
};


/**
 * Create registered user's today visit log
 * */
UserDailyVisitLogSchema.statics.createUserTodayVisitLog = async function (userId, ipAddress) {
    return await this.create({
        userId: userId,
        ipAddress: ipAddress,
        createdTime: time.now()
    });
};


/**
 * Create unregistered user's today visit log
 * */
UserDailyVisitLogSchema.statics.createUnregUserTodayVisitLog = async function (ipAddress) {
    await this.create({
        ipAddress: ipAddress,
        createdTime: time.now()
    });
};


module.exports = mongoose.model("user_daily_visit_log", UserDailyVisitLogSchema);