const CommentCollection = require('../../../models/comment');
const CommentUpvoteCollection = require('../../../models/commentUpvote');
const {httpRespond, httpError} = require('../../../utils/http/response');
const {paramCheck, paramType} = require("../../../utils/paramUtil");



/**
 * Increase/Decrease comment upvote count
 * */
exports.updateCommentUpvote = async (req, res) => {
    const {postNumber, commentNumber} = req.body;

    const userId = req.userData.id;
    const ipAddress = req.clientIp;


    if (
        !paramCheck(postNumber, paramType.Number) ||
        !paramCheck(commentNumber, paramType.Number)
    ) return httpError(res, 400);



    const commentData = await CommentCollection.findComment(postNumber, commentNumber);

    // Comment has been deleted
    if (!commentData) return httpError(res, 404);


    let isUpvoted;
    let upvoteCount = commentData.record.upvoteCount;


    const upvoteData = await CommentUpvoteCollection.findCommentUpvoteData(postNumber, commentNumber, userId, ipAddress);

    if (!upvoteData) {
        await CommentUpvoteCollection.createCommentUpvoteData(postNumber, commentNumber, userId, ipAddress);

        isUpvoted = true;
        upvoteCount += 1;
    }
    else if (upvoteData && upvoteData.isUpvoted) {
        await upvoteData.unsetUpvoted();

        isUpvoted = false;
        if (upvoteCount!==0) upvoteCount -= 1;
    }
    else if (upvoteData && !upvoteData.isUpvoted) {
        await upvoteData.setUpvoted();

        isUpvoted = true;
        upvoteCount += 1;
    }


    await commentData.updateCommentUpvoteRecord(upvoteCount);


    return httpRespond(res, 200, {isUpvoted: isUpvoted, updatedUpvoteCount: upvoteCount});
}