const pattern = {
  id: "(?=^[a-zA-Z0-9]*$)(?=^\\S*$).*",
  password: "(?=^[a-zA-Z0-9!@#$%^&*_-]*$)(?=^\\S*$).*",
  nickname: "(?=^[a-zA-Z0-9가-힣]*$)(?=^\\S*$).*",
  email: "(?=^.+@.+\\..+$)(?=^\\S*$).*"
}


exports.test = {
  id: text => RegExp(pattern.id).test(text) && (4 <= text.length <= 16),
  password: text => RegExp(pattern.password).test(text) && (8 <= text.length <= 24),
  nickname: text =>RegExp(pattern.nickname).test(text) && (1 <= text.length <= 16),
  email: text => RegExp(pattern.email).test(text)
}
