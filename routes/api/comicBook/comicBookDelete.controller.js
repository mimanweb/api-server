const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsCollection = require("../../../models/comics");
const ComicBookCollection = require("../../../models/comicBook");
const ImageCollection = require("../../../models/image");
const PostCollection = require("../../../models/post");
const _ = require("underscore");
const {COMIC_BOOK_STATUS} = require("../../../utils/index/comicBook");
const {POST_STATUS} = require("../../../utils/index/post");
const {SITE_BOARD_INDEX} = require("../../../utils/index/siteIndex");
const {SITE_CATEGORY_INDEX} = require("../../../utils/index/siteIndex");
const {COMIC_BOOK_IMAGE_STATUS, COMIC_BOOK_CONTENT_TYPE} = require("../../../utils/index/comicBook");



/**
 * Delete comic book
 * */
exports.deleteComicBook = async (req, res) => {
    const isRegUser = req.isRegUser;
    const userId = req.userData.id;

    if (!isRegUser) return httpError(res, 403);


    const {comicBookDataId} = req.body;

    if (
        !paramCheck(comicBookDataId)
    ) return httpError(res, 400);


    /**
     * Find comic book data
     * */
    const comicBookData = await ComicBookCollection.findComicBookDataWithDataId(comicBookDataId);

    if (!comicBookData) return httpError(res, 404);

    const {index: comicBookIndex, linkThumbnails, owner: comicBookOwner, status: comicBookStatus} = comicBookData;
    const {contentType, isShortStory, isVolume, volume, issue, shortStoryNumber} = comicBookIndex;


    if (![
        COMIC_BOOK_STATUS.PUBLISH,
        COMIC_BOOK_STATUS.WRITING,
        COMIC_BOOK_STATUS.EDITING,
    ].includes(comicBookStatus)) return httpError(res, 400);

    if (userId!==comicBookOwner.userId) return httpError(res, 403);


    /**
     * Find comics data
     * */
    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicBookIndex.comicsDataId);

    if (!comicsData) return httpError(res, 404);





    switch (contentType) {
        case COMIC_BOOK_CONTENT_TYPE.LINK:
            const linkThumbnail = linkThumbnails.find(thumbnail => thumbnail.status===COMIC_BOOK_IMAGE_STATUS.PUBLISH);
            const linkThumbnailDoc = linkThumbnail? linkThumbnail._doc: null;

            if (linkThumbnailDoc) {
                for (const imgId of Object.values(linkThumbnailDoc.imgId)) {
                    const imageData = await ImageCollection.findImageById(imgId);

                    await imageData.deleteImageData();
                }
            }

            await comicBookData.deleteLinkContentComicBookData(linkThumbnailDoc);
            break;
    }


    /**
     * Find and delete post data
     * */
    const postData = await PostCollection.findPostWithPostOid(comicBookIndex.postDataId, SITE_CATEGORY_INDEX.COMICS.code, SITE_BOARD_INDEX.COMICS.BOOK.code, POST_STATUS.PUBLISH);

    await postData.deletePost();


    return httpError(res, 200);
}