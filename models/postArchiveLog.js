const mongoose = require('mongoose');
const time = require("../utils/time");
const Schema = mongoose.Schema;


const PostArchiveLogSchema = new Schema({
    number: Number,
    type: String,
    index: {
        category: String,
        board: String
    },
    owner: {
        userId: String,
        deviceUUID: String,
        ipAddress: String,
        nickname: String,
        password: String,
    },
    title: String,
    content: String,
    phrase: String,
    images: [String],
    status: String,
    record: {
        viewCount: Number,
        upvoteCount: Number,
    },
    time: {
        created: Date,
        updated: Date,
        published: Date,
        archived: Date
    }
});



/**
 * Create an archive post
 * */
PostArchiveLogSchema.statics.createArchivePost = async function (postData, images, status) {
    const {number, type, index, owner, title, content, phrase, record, time: postTime} = postData._doc? postData._doc: postData;

    await this.create({
        number: number,
        type: type,
        index: {
            category: index.category,
            board: index.board
        },
        owner: {
            userId: owner.userId,
            deviceUUID: owner.deviceUUID,
            ipAddress: owner.ipAddress,
            nickname: owner.nickname,
            password: owner.password,
        },
        title: title,
        content: content,
        phrase: phrase,
        images: images,
        status: status,
        record: {
            viewCount: record.viewCount,
            upvoteCount: record.upvoteCount,
        },
        time: {
            created: postTime.created,
            updated: postTime.updated,
            published: postTime.published,
            archived: time.now()
        }
    })
}



module.exports = mongoose.model('post_archive_log', PostArchiveLogSchema);
