const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require("underscore");
const time = require("../utils/time");
const {parseDataId} = require("../utils/mongoose");
const {parseDocFromModel} = require("../utils/mongoose");





const NotificationSchema = new Schema({
    type: String,
    userId: String,
    dataId: Schema.Types.ObjectID,
    isChecked: Boolean,
    time: {
        created: Date,
        checked: Date
    }
})





/**
 * Find documents count
 * */
NotificationSchema.statics.findNotificationCount = async function (userId) {
    return await this.find({userId: userId}).countDocuments();
}


/**
 * Find all notification
 * */
NotificationSchema.statics.findAllNotificationInPages = async function (userId, itemsPerPage, pageToSearch=1) {
    itemsPerPage = Number(itemsPerPage);
    pageToSearch = Number(pageToSearch);


    return await this.aggregate([
        {
            '$match': {
                'userId': userId
            }
        }, {
            '$sort': {
                'time.created': -1
            }
        }, {
            '$skip': (pageToSearch-1)*itemsPerPage
        }, {
            '$limit': itemsPerPage
        }, {
            '$lookup': {
                'from': 'comments',
                'localField': 'dataId',
                'foreignField': '_id',
                'as': 'comment'
            }
        }, {
            '$project': {
                'type': 1,
                'time': 1,
                "isChecked": 1,
                'comment': {
                    '$first': '$comment'
                }
            }
        }, {
            $lookup: {
                from: "posts",
                localField: "comment.belongPostNumber",
                foreignField: "number",
                as: "post"
            }
        }, {
            $project: {
                'type': 1,
                'time': 1,
                "isChecked": 1,
                "comment": 1,
                "post": {$first: "$post"}
            }
        }
    ] || [])
}



/**
 * Update notification data as checked
 * */
NotificationSchema.statics.updateNotificationDataAsChecked = async function (dataId) {
    await this.updateOne({_id: parseDataId(dataId)}, {isChecked: true});
}


/**
 * Update all notification data as checked
 * */
NotificationSchema.statics.updateAllNotificationDataAsChecked = async function (userId) {
    await this.updateMany({userId: userId}, {isChecked: true});
}


/**
 * Create notification data
 * */
NotificationSchema.statics.createNotificationData = async function (type, userId, dataId) {
    await this.create({
        type: type,
        userId: userId,
        dataId: parseDataId(dataId),
        time: {
            created: time.now()
        }
    })
}






module.exports = mongoose.model('notification', NotificationSchema);