const {httpRespond, httpError} = require('../../../utils/http/response');
const crypto = require("crypto");
const mail = require("../../../utils/mail");

const User = require('../../../models/user');



/**
 * Send user id of registered email through email
 * */
exports.userId = async (req, res) => {
  const {email} = req.body;

  // No email
  if (!email) return httpError(res, 400);

  let user;
  try {
    user = await User.findOneBy({email: email});
  } catch (e) {
    return httpError(res, 500);
  }

  // Not found
  if (!user) return httpError(res, 404);


  //해당 이메일로 가입된 아이디는 [ ${userData.id} ] 입니다.
  const sent = await mail.send(
    undefined,
    email,
    "아이디 찾기",
    ` 해당 이메일로 가입된 ID는 [ ${user.id} ] 입니다.`);

  if (!sent) return httpError(res, 500)

  return httpRespond(res);
};


/**
 * Send password reset email
 * */
exports.userPasswordResetLink = async (req, res, next) => {
  const {id, email} = req.body;

  // No parameter
  if (!id || !email) return httpError(res, 400);

  let userData;
  try {
    userData = await User.findOneBy({id: id, email: email});
  } catch (e) {
    return httpError(res, 500);
  }

  // Not found
  if (!userData) return httpError(res, 404);

  const token = await crypto.randomBytes(5).toString("hex");

  try {
    await userData.savePasswordResetToken(token);
  } catch (e) {
    // DB Error
    return httpError(res, 500, "000");
  }



  const link = `${process.env["WEBSITE_URL"]}/account/password/reset/${userData.id}/${token}`


  const sent = await mail.send(
    undefined,
    email,
    "비번 재설정",
    undefined,
    `<a href="${link}">비번 재설정하기</a>`);


  if (!sent) return httpError(res, 500, "001")

  return httpRespond(res);
};