const express = require('express');
const router = express.Router();
const comicBookEdit = require("./comicBook.controller");
const comicBookDelete = require("./comicBookDelete.controller");
const comicBookArchiveAndUpdate = require("./archiveAndUpdate.controller");
const multer  = require('multer');
const upload = multer();


const {verifyLogin} = require('../../../utils/auth/login');


router.use(verifyLogin);


router.post("/edit/page", upload.single("image"), comicBookEdit.addComicBookPageImage);
router.put("/edit/single/page/type", comicBookEdit.updateComicBookSinglePageType);
router.put("/edit/double/page/type", comicBookEdit.updateComicBookDoublePageType);
router.delete("/edit/page", comicBookEdit.deleteComicBookPage);
router.put("/edit/page/order", comicBookEdit.updateComicBookPageOrder);
router.put("/edit/info", comicBookEdit.updateComicBookInfo);

router.post("/", comicBookEdit.publishComicBook);
router.put("/", comicBookArchiveAndUpdate.makeArchiveAndUpdateComicBookData);
router.delete("/", comicBookDelete.deleteComicBook);



module.exports = router;