const UserDailyVisitLogCollection = require("../models/userDailyVisitLog");


/**
 * Save unregistered user's today's visit log,
 * Set visit log id to req if the IP has already accessed before,
 * Or create a new log
 * */
exports.saveUnregUserVisitLog = async (req, res, next) => {
    const ipAddress = req.clientIp;
    const isRegisteredUser = req.session.ACCESS_TOKEN || req.session.REFRESH_TOKEN;

    // Pass if registered user tries to visit
    if (isRegisteredUser) return next();

    const todayVisitLog = await UserDailyVisitLogCollection.findUnregUserTodayVisitLog(ipAddress);

    if (!todayVisitLog) await UserDailyVisitLogCollection.createUnregUserTodayVisitLog(ipAddress);

    return next();
}