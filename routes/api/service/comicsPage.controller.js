const { httpRespond, httpError } = require('../../../utils/http/response');
const ComicsCollection = require("../../../models/comics");
const ComicBookCollection = require("../../../models/comicBook");
const {COMIC_BOOK_STATUS} = require("../../../utils/index/comicBook");
const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {COMICS_STATUS} = require("../../../utils/index/comics");



/**
 * Comics page data
 * */
exports.comicsPage = async (req, res) => {
    const {comicsNumber, comicsId} = req.query;


    if (
        !paramCheck(comicsNumber, paramType.Number) ||
        !paramCheck(comicsId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsData(comicsNumber, comicsId);


    if (
        !comicsData ||
        (comicsData && comicsData.status===COMICS_STATUS.DELETED)
    ) return httpError(res, 404);


    const comicBookList = await ComicBookCollection.findAllComicBookDataWithLinkedComicsDataId(undefined, comicsData._id, COMIC_BOOK_STATUS.PUBLISH);

    comicsData.editions = comicBookList || [];
    console.log(comicsData)

    return httpRespond(res, 200, {
        user: req.userData,
        comics: comicsData
    })
}