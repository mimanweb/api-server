const mongoose = require('mongoose');
const time = require("../utils/time");
const Schema = mongoose.Schema;
const {BEST_COMMENT_COUNT, BEST_COMMENT_MIN_UPVOTE_COUNT_CRIT, COMMENT_STATUS, COMMENT_TYPE} = require("../utils/index/comment");
const _ = require("underscore");



const CommentSchema = new Schema({
    number: Number,
    belongPostNumber: Number,
    repliedCommentNumber: Number,
    type: String,
    isDeleted: Boolean,
    status: String,
    owner: {
        userId: String,
        nickname: String,
        password: String,
        ipAddress: String,
        deviceUUID: String
    },
    content: {
        imageId: String,
        youtubeHTML: String,
        text: String
    },
    record: {
        upvoteCount: {type: Number, default: 0}
    },
    time: {
        created: Date,
        deleted: Date,
    }
})



CommentSchema.index({"number": -1});
CommentSchema.index({"belongPostNumber": -1});




/**
 * Find comment data
 * */
CommentSchema.statics.findComment = async function (postNumber, commentNumber) {
    return await this.findOne({
        belongPostNumber: postNumber,
        number: commentNumber,
        isDeleted: {$exists: false}
    }).sort({
        belongPostNumber: -1,
        number: -1
    })
}

/**
 * Find latest comment data
 * */
CommentSchema.statics.findLatestComment = async function (postNumber) {
    return await this.findOne({
        belongPostNumber: postNumber
    }).sort({"number": -1});
}



/**
 * Find comment count
 * */
CommentSchema.statics.findTotalCommentCount = async function (postNumber) {
    return await this.countDocuments({"belongPostNumber": postNumber});
}


/**
 * Find comment by range
 * */
CommentSchema.statics.findCommentListAndPagingInfo = async function (postNumber, pageToSearch, commentNumberToSearch, itemsPerPage) {
    postNumber = Number(postNumber);
    pageToSearch = Number(pageToSearch);
    commentNumberToSearch = Number(commentNumberToSearch);


    const aggregationRes = await this.aggregate([
        {
            '$match': {
                'belongPostNumber': postNumber,
                'type': 'COMMENT'
            }
        }, {
            '$graphLookup': {
                'from': 'comments',
                'startWith': '$number',
                'connectFromField': 'number',
                'connectToField': 'repliedCommentNumber',
                'as': 'replyComments',
                'restrictSearchWithMatch': {
                    'belongPostNumber': postNumber
                }
            }
        }, {
            '$unwind': {
                'path': '$replyComments',
                'preserveNullAndEmptyArrays': true
            }
        }, {
            '$sort': {
                'replyComments.number': 1
            }
        }, {
            '$group': {
                '_id': '$number',
                'comment': {
                    '$first': '$$ROOT'
                },
                'replyComments': {
                    '$push': '$replyComments'
                }
            }
        }, {
            '$sort': {
                '_id': 1
            }
        }, {
            '$project': {
                'comments': {
                    '$concatArrays': [
                        [
                            '$comment'
                        ], '$replyComments'
                    ]
                }
            }
        }, {
            '$unwind': {
                'path': '$comments'
            }
        }, {
            '$replaceRoot': {
                'newRoot': '$comments'
            }
        }, {
            '$lookup': {
                'from': 'comments',
                'let': {
                    'repliedCommentNumber': '$repliedCommentNumber'
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$and': [
                                    {
                                        '$eq': [
                                            '$belongPostNumber', postNumber
                                        ]
                                    }, {
                                        '$eq': [
                                            '$number', '$$repliedCommentNumber'
                                        ]
                                    }
                                ]
                            }
                        }
                    }, {
                        '$project': {
                            'owner': 1,
                            'status': 1
                        }
                    }
                ],
                'as': 'repliedComment'
            }
        }, {
            '$unwind': {
                'path': '$repliedComment',
                'preserveNullAndEmptyArrays': true
            }
        }, {
            '$project': {
                'belongPostNumber': 0,
                'replyComments': 0,
                'repliedCommentNumber': 0
            }
        }, {
            '$group': {
                '_id': null,
                'all': {
                    '$push': '$$ROOT'
                }
            }
        }, {
            '$project': {
                '_id': 0,
                'all': 1,
                'satisfyUpvoteCrit': {
                    '$filter': {
                        'input': '$all',
                        'as': 'item',
                        'cond': {
                            '$and': [
                                {
                                    '$gte': [
                                        '$$item.record.upvoteCount', BEST_COMMENT_MIN_UPVOTE_COUNT_CRIT
                                    ]
                                }, {
                                    '$ne': [
                                        '$$item.isDeleted', true
                                    ]
                                }
                            ]
                        }
                    }
                }
            }
        }, {
            '$unwind': {
                'path': '$satisfyUpvoteCrit',
                'preserveNullAndEmptyArrays': true
            }
        }, {
            '$sort': {
                'satisfyUpvoteCrit.record.upvoteCount': -1
            }
        }, {
            '$limit': BEST_COMMENT_COUNT
        }, {
            '$group': {
                '_id': null,
                'all': {
                    '$push': '$all'
                },
                'best': {
                    '$push': '$satisfyUpvoteCrit'
                }
            }
        }, {
            '$project': {
                '_id': 0,
                'allComments': {
                    '$first': '$all'
                },
                'bestComments': '$best',
                'itemsPerPage': {
                    '$literal': itemsPerPage
                }
            }
        }, {
            '$project': {
                'allComments': 1,
                'bestComments': 1,
                'totalCommentCount': {
                    '$size': '$allComments'
                },
                'itemsPerPage': 1,
                'totalPageCount': {
                    '$ceil': {
                        '$divide': [
                            {
                                '$size': '$allComments'
                            }, '$itemsPerPage'
                        ]
                    }
                },
                'posOfCommentToSearch': {
                    '$add': [
                        1, {
                            '$indexOfArray': [
                                '$allComments', {
                                    '$first': {
                                        '$filter': {
                                            'input': '$allComments',
                                            'as': 'comment',
                                            'cond': {
                                                '$eq': [
                                                    commentNumberToSearch, '$$comment.number'
                                                ]
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            }
        }, {
            '$project': {
                'allComments': 1,
                'bestComments': 1,
                'totalCommentCount': 1,
                'itemsPerPage': 1,
                'totalPageCount': 1,
                'pageOfCommentToSearch': {
                    '$ceil': {
                        '$divide': [
                            '$posOfCommentToSearch', '$itemsPerPage'
                        ]
                    }
                }
            }
        }, {
            '$project': {
                'allComments': 1,
                'bestComments': 1,
                'totalCommentCount': 1,
                'itemsPerPage': 1,
                'totalPageCount': 1,
                'pageToSearch': {
                    '$cond': {
                        'if': {
                            '$gt': [
                                '$pageOfCommentToSearch', 0
                            ]
                        },
                        'then': '$pageOfCommentToSearch',
                        'else': {
                            '$cond': {
                                'if': {
                                    '$lte': [
                                        pageToSearch, '$totalPageCount'
                                    ]
                                },
                                'then': {
                                    '$literal': pageToSearch
                                },
                                'else': '$totalPageCount'
                            }
                        }
                    }
                }
            }
        }, {
            '$project': {
                'bestComments': 1,
                'totalCommentCount': 1,
                'totalPageCount': 1,
                'searchedPage': '$pageToSearch',
                'pageComments': {
                    '$slice': [
                        '$allComments', {
                            '$multiply': [
                                {
                                    '$subtract': [
                                        '$pageToSearch', 1
                                    ]
                                }, '$itemsPerPage'
                            ]
                        }, '$itemsPerPage'
                    ]
                }
            }
        }
    ]);


    if (_.isEmpty(aggregationRes)) {
        return {
            bestComments: [],
            pageComments: [],
            totalCommentCount: 0,
            searchedPage: null,
            totalPageCount: 0,
        }
    } else {
        const {bestComments, pageComments, totalCommentCount, searchedPage, totalPageCount} = aggregationRes[0];
        return {
            bestComments: bestComments,
            pageComments: pageComments,
            totalCommentCount: totalCommentCount,
            searchedPage: searchedPage,
            totalPageCount: totalPageCount,
        }
    }
}


/**
 * Create comment
 * */
CommentSchema.statics.createComment = async function (commentNumber, userId, nickname, password, ipAddress, deviceUUID, imageId, youtubeHTML, text, belongPostNumber) {
    return await this.create({
        number: commentNumber,
        belongPostNumber: belongPostNumber,
        type: COMMENT_TYPE.COMMENT,
        status: COMMENT_STATUS.PUBLISH,
        owner: {
            userId: userId,
            nickname: nickname,
            password: password,
            ipAddress: ipAddress,
            deviceUUID: deviceUUID
        },
        content: {
            imageId: imageId,
            youtubeHTML: youtubeHTML,
            text: text
        },
        time: {
            created: time.now(),
        }
    })
}


/**
 * Create reply-comment
 * */
CommentSchema.statics.createReplyComment = async function (commentNumber, userId, nickname, password, ipAddress, deviceUUID, imageId, youtubeHTML, text, belongPostNumber, repliedCommentNumber) {
    return await this.create({
        number: commentNumber,
        belongPostNumber: belongPostNumber,
        repliedCommentNumber: repliedCommentNumber,
        type: COMMENT_TYPE.REPLY_COMMENT,
        status: COMMENT_STATUS.PUBLISH,
        owner: {
            userId: userId,
            nickname: nickname,
            password: password,
            ipAddress: ipAddress,
            deviceUUID: deviceUUID
        },
        content: {
            imageId: imageId,
            youtubeHTML: youtubeHTML,
            text: text
        },
        time: {
            created: time.now(),
        }
    })
}


/**
 * Update comment upvote record
 * */
CommentSchema.methods.updateCommentUpvoteRecord = async function(upvoteCount) {
    await this.updateOne({ "record.upvoteCount": upvoteCount })
}


/**
 * Delete comment
 * */
CommentSchema.methods.deleteCommentData = async function (deleteType) {
    await this.updateOne({
        isDeleted: true,
        status: deleteType,
        "time.deleted": time.now()
    })
}



module.exports = mongoose.model('comment', CommentSchema);