const bcrypt = require('bcrypt');
const {httpRespond, httpError} = require('../../../utils/http/response');
const User = require('../../../models/user');
const regex = require("../../../utils/regex")
const {paramCheck, paramType} = require("../../../utils/paramUtil");




/**
 * User registration
 *
 * @param {string} req.body.id
 * @param {string} req.body.password
 *
 * @throws 400 000 Parameter not given
 * @throws 400 001 Id duplicated
 * @throws 400 002 Invalid parameters
 * @throws 500 000 Error while inserting user data into DB
 * */
exports.register = async (req, res) => {
    const {id, password, nickname, email} = req.body;

    // Check necessary parameters
    if (!id || !password || !nickname || !email) {
        return httpError(res, 400, "000");
    }

    // Check if ID is duplicated
    let user;
    try {
        user = await User.findUserById(id);
    } catch (e) {
        return httpError(res, 500)
    }

    if (user) return httpError(res, 400, "001");

    // Parameter validation
    if (!regex.test.id(id) ||
        !regex.test.nickname(nickname) ||
        !regex.test.password(password) ||
        !regex.test.email(email)) {
        return httpError(res, 400, "002");
    }

    // Generate salt, hashed password
    const salt = await bcrypt.genSalt(5);
    const hash = await bcrypt.hash(password, salt);

    // Save user to DB
    let createdUserId;
    try {
        createdUserId = await User.createUser(id, hash, salt, nickname, email);
    } catch (e) {
        // DB insert failed
        return httpError(res, 500, "000");
    }


    return httpRespond(res,
        201,
        {id: createdUserId});
};


/**
 * Check if user param is duplicated
 * */
exports.checkDuplicate = async (req, res) => {
    let user;
    try {
        user = await User.findOneBy(req.query);
    } catch (e) {
        return httpError(res, 500);
    }

    return httpRespond(res, 200, !!user);
};


/**
 * Change password
 * Encrypt the new password with [passwordSalt] and save it
 * If the attempt count did not exceed
 * Then renew the [loginTokenKey] to make the other devices sign out
 *
 * @param {string} req.body.id User id
 * @param {string} req.body.token Password reset token
 *                                that is made at sending a password-reset-request email's sent
 * @param {string} req.body.password New password to apply to the account
 *
 * @throws 400 No params
 * @throws 403 Token expired or any wrong attempt
 * @throws 404 Wrong id
 * @throws 500 DB error
 * */
exports.changeUserPassword = async (req, res) => {
    const {id, token, password} = req.body;

    // No parameter
    if (!id || !token || !password) return httpError(res, 400);


    let userData;
    try {
        userData = await User.findOneBy({id: id});
    } catch (e) {
        return httpError(res, 500);
    }

    // No matching data
    if (!userData) return httpError(res, 404);

    const attemptCount = userData.password.resetAttemptCount;

    // Maximum attempt count exceeded or previously not requested
    if (!attemptCount || attemptCount === 0) return httpError(res, 403);

    // Subtract attempt count
    try {
        await userData.updatePasswordResetAttemptCount();
    } catch (e) {
        return httpError(res, 500);
    }


    // Wrong token
    if (token !== userData.password.resetToken) return httpError(res, 403);

    /** Nothing is wrong, proceed changing password */
    try {
        await userData.changePassword(password);
    } catch (e) {
        return httpError(res, 500);
    }

    return res.end();
};


/**
 * Modify user data
 * */
exports.modifyUserInfo = async (req, res) => {
    const userData = req.userData;
    const {password, newPassword, nickname} = req.body;

    // Login failed, No password given
    if (!req.isRegUser || !password) return httpError(res, 403);

    // Wrong password
    const passwordVerified = await bcrypt.compare(password, userData.password.value);
    if (!passwordVerified) return httpError(res, 403);


    if (nickname && (nickname !== userData.nickname)) {
        // No nickname or wrong one
        if (!regex.test.nickname(nickname)) return httpError(res, 400, "000");

        const nicknameUpdatedTime = new Date(userData.time.nicknameUpdated)
        const today = new Date();
        const thisMonday = new Date();
        thisMonday.setDate(today.getDate() - (7 - today.getDay()) + 1) // +1 day because default week starts from sunday(0)
        thisMonday.setHours(0);
        thisMonday.setMinutes(0);
        thisMonday.setSeconds(0);

        // Nickname changeable period exceeded
        if (!(thisMonday > nicknameUpdatedTime)) return httpError(res, 400, "001")

        try {
            await userData.changeNickname(nickname);
        } catch (e) {
            console.error(e);
            return httpError(res, 500);
        }
    }


    // Change password
    if (newPassword) {
        // Wrong new password
        if (!regex.test.password(newPassword)) return httpError(res, 400, "002");

        try {
            await userData.changePassword(newPassword);
        } catch (e) {
            return httpError(res, 500);
        }
    }


    return httpRespond(res, 200);
};


/**
 * Find id with registered email
 * @param {string} req.query.email
 * @return user id (with last 4 letters masked)
 * */
exports.findUserId = async (req, res) => {
    const {email} = req.query;

    // No email
    if (!email) return httpError(res, 400);

    let user;
    try {
        user = await User.findOneBy({email: email});
    } catch (e) {
        return httpError(res, 500);
    }

    if (!user) return httpError(res, 404);

    const id = user.id
    let masked = "";

    if (id.length <= 5) masked = id.substring(0, id.length - 2) + "**"
    else if (5 < id.length <= 8) masked = id.substring(0, id.length - 3) + "***"
    else masked = id.substring(0, id.length - 4) + "****"

    return httpRespond(res, 200, masked);
};


/**
 * Unregister user
 * */
exports.unregister = async (req, res) => {
    const isRegUser = req.isRegUser;
    const userId = req.userData.id;
    const {password} = req.body;

    if (!paramCheck(password)) return httpError(res, 400);
    if (!isRegUser) return httpError(res, 403);


    const userData = await User.findOneBy({id: userId});

    const isPasswordVerified = await bcrypt.compare(password, userData.password.value);

    if (!isPasswordVerified) return httpError(res, 403);


    await userData.unregisterUserData();


    req.session.ACCESS_TOKEN = null
    req.session.REFRESH_TOKEN = null

    return httpError(res, 200);
}
