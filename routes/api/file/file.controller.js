const fs = require('fs');
const path = require("path");
const {httpError, httpRespond} = require("../../../utils/http/response");
const ImageCollection = require("../../../models/image");
const {IMAGE_TYPE} = require("../../../utils/index/image");





/**
 * Serve Post Image File
 * */
exports.servePostImageFile = async (req, res) => {
  const prefix = req.params[0];
  const id = req.params[1];


  if (!prefix || (prefix && prefix.length<2) || !id) return httpError(res, 403);


  const imgType = IMAGE_TYPE.POST;

  const imgId = `${imgType}/${prefix}/${id}`;


  const imageData = await ImageCollection.findImageById(imgId, imgType);

  if (!imageData) return httpError(res, 404);


  const file = imageData.file;

  const imgFolderDir = path.join(process.env["IMAGES_DIR"], process.env["POST_IMAGES_FOLDER"]);
  const filePath = path.join(imgFolderDir, file.directory, file.name);


  const stream = fs.createReadStream(filePath);

  stream.on("open", () => {
    res.set("Content-Type", file.mime);
    stream.pipe(res);
  })

  stream.on("error", (e) => {
    return httpError(res, 404);
  })
}


/**
 * Serve Comment Image File
 * */
exports.serveCommentImageFile = async (req, res) => {
  const prefix = req.params[0];
  const id = req.params[1];


  if (!prefix || (prefix && prefix.length<2) || !id) return httpError(res, 403);


  const imgType = IMAGE_TYPE.COMMENT;

  const imgId = `${imgType}/${prefix}/${id}`;


  const imageData = await ImageCollection.findImageById(imgId, imgType);

  if (!imageData) return httpError(res, 404);


  const file = imageData.file;

  const imgFolderDir = path.join(process.env["IMAGES_DIR"], process.env["COMMENT_IMAGES_FOLDER"]);
  const filePath = path.join(imgFolderDir, file.directory, file.name);


  const stream = fs.createReadStream(filePath);

  stream.on("open", () => {
    res.set("Content-Type", file.mime);
    stream.pipe(res);
  })

  stream.on("error", (e) => {
    return httpError(res, 404);
  })
}