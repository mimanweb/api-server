const fs = require("fs");
const _ = require("underscore");




const checkPath = async (path) => {
  let isDirExist = true;

  await fs.promises.access(path).catch(async () => {
    isDirExist = false
  })

  return isDirExist;
}


const getLastNumberOfNumItemsInDir = async (dirToSearch) => {
  const items = await readDir(dirToSearch);

  return _.isEmpty(items)? 0: Math.max(...items.map(name => {
    const isFile = /\./g.test(name);
    const numStr = isFile? name.split(".").shift(): name;

    return Number(numStr);
  }));
}


const writeFile = async (path, buffer) => {
  await fs.promises.writeFile(path, buffer);
}


const mkdir = async (dir) => {
  await fs.promises.mkdir(dir, { recursive: true });
}


const mkdirIfNone = async (dir) => {
  if (!await checkPath(dir)) await mkdir(dir);
}


const readDir = async (dir) => {
  return await fs.promises.readdir(dir);
}


const mv = async (oldPath, newPath) => {
  await fs.promises.rename(oldPath, newPath).catch(async (err) => {
    console.error(err);
  });
}

const deleteFile = async (filePath) => {
  await fs.promises.access(filePath).then(async () => {
    await fs.promises.unlink(filePath);
  }).catch(() => {
  })
}




exports.readDir = readDir;
exports.getLastNumberOfNumItemsInDir = getLastNumberOfNumItemsInDir;
exports.writeFile = writeFile;
exports.checkPath = checkPath;
exports.mkdir = mkdir;
exports.mkdirIfNone = mkdirIfNone;
exports.mv = mv;
exports.deleteFile = deleteFile;