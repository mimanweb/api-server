const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const time = require("../utils/time");
const {COMICS_STATUS} = require("../utils/index/comics");
const _ = require("underscore");
const {COMIC_BOOK_STATUS} = require("../utils/index/comicBook");
const {COMICS_SEARCH_ITEMS_PER_PAGE} = require("../utils/index/comics");
const {COMICS_SEARCH_KOR_CHAR_INDEXES} = require("../utils/index/comics");
const {parseDataId} = require("../utils/mongoose");





const ComicsSchema = new Schema({
    number: Number,
    id: String,
    title: {
        korean: String,
        origin: String
    },
    status: String,
    registeredUser: String,
    writers: [Schema.Types.ObjectID],
    artists: [Schema.Types.ObjectID],
    publisher: Schema.Types.ObjectID,
    publishStatus: String,
    genres: [Schema.Types.ObjectID],
    contentDescriptors: [Schema.Types.ObjectID],
    summary: String,
    coverImage: {
        // 128px
        small: String,
        // 256px
        medium: String,
        // 512px
        large: String,
        // 1024px
        xLarge: String,
    },
    time: {
        created: Date,
        updated: Date,
        approved: Date,
        deleted: Date
    }
})




/**
 * Find all comics data count
 * */
ComicsSchema.statics.findAllComicsCount = async function () {
    return await this.find({}).count();
}


/**
 * Find comics data of latest number
 * */
ComicsSchema.statics.findLatestComicsData = async function () {
    return await this.findOne({}).sort({number: -1});
}


/**
 * Find all comics data match comics numbers
 * */
ComicsSchema.statics.findAllComicsAndLatestBookWithComicsNumbers = async function (numberList) {
    return await this.aggregate([
        {
            '$match': {
                'number': {
                    '$in': numberList
                },
                "status": {$ne: COMICS_STATUS.DELETED}
            }
        }, {
            '$lookup': {
                'from': 'comic_books',
                'let': {
                    'comics_data_id': '$_id'
                },
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$and': [
                                    {
                                        '$eq': [
                                            '$index.comicsDataId', '$$comics_data_id'
                                        ]
                                    }, {
                                        '$eq': [
                                            '$status', COMIC_BOOK_STATUS.PUBLISH
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                ],
                'as': 'comicBookList'
            }
        }, {
            '$unwind': {
                'path': '$comicBookList',
                'preserveNullAndEmptyArrays': true
            }
        }, {
            '$sort': {
                'comicBookList.time.publish': -1
            }
        }, {
            '$group': {
                '_id': '$_id',
                'comics': {
                    '$first': '$$ROOT'
                },
                'latestComicBook': {
                    '$first': '$comicBookList'
                }
            }
        }, {
            '$project': {
                'writers': '$comics.writers',
                'artists': '$comics.artists',
                'genres': '$comics.genres',
                'contentDescriptors': '$comics.contentDescriptors',
                'number': '$comics.number',
                'id': '$comics.id',
                'title': '$comics.title',
                'time': '$comics.time',
                'status': '$comics.status',
                'registeredUser': '$comics.registeredUser',
                'latestComicBook': 1
            }
        }
    ]);
}


/**
 * Find comics data with data id
 * */
ComicsSchema.statics.findComicsDataWithDataId = async function (_id) {
    const aggregationRes = await this.aggregate([
        {
            '$match': {
                '_id': parseDataId(_id)
            }
        }, {
            '$lookup': {
                'from': 'comic_books',
                'localField': '_id',
                'foreignField': 'index.comicsDataId',
                'as': 'books'
            }
        }
    ])

    return _.isEmpty(aggregationRes)? null: aggregationRes.shift();
}


/**
 * Find comics data
 * */
ComicsSchema.statics.findComicsData = async function (number, id) {
    const aggregationResult = await this.aggregate([
        {
            '$match': {
                'number': Number(number),
                'id': id
            }
        }, {
            '$lookup': {
                'from': 'comics_writers',
                'localField': 'writers',
                'foreignField': '_id',
                'as': 'writers'
            }
        }, {
            '$lookup': {
                'from': 'comics_artists',
                'localField': 'artists',
                'foreignField': '_id',
                'as': 'artists'
            }
        }, {
            '$lookup': {
                'from': 'comics_publishers',
                'localField': 'publisher',
                'foreignField': '_id',
                'as': 'publisher'
            }
        }, {
            $unwind: {
                path: "$publisher",
                preserveNullAndEmptyArrays: true
            }
        }, {
            '$lookup': {
                'from': 'genres',
                'localField': 'genres',
                'foreignField': '_id',
                'as': 'genres'
            }
        }, {
            '$lookup': {
                'from': 'content_descriptors',
                'localField': 'contentDescriptors',
                'foreignField': '_id',
                'as': 'contentDescriptors'
            }
        }
    ])


    return _.isEmpty(aggregationResult)? null: aggregationResult[0];
}



/**
 * Find comics data with title text
 * */
ComicsSchema.statics.findAllComicsDataWithTitle = async function (titleText, paging=true, itemsPerPage=COMICS_SEARCH_ITEMS_PER_PAGE, pageToSearch=1) {
    if (pageToSearch) pageToSearch = Number(pageToSearch);

    let pattern = titleText.trim().replace(/[\\{}()_\-\s,.\/?!^\[\]*=+;:'"]/g, " ")

    if (!/[\S]/.test(pattern)) return null;

    pattern = pattern.replace(/([\s]+)/g, ".*")

    const param = {
        $or: [
            {
                "title.korean": {
                    $regex: pattern,
                    $options: "is"
                }
            },
            {
                "title.origin": {
                    $regex: pattern,
                    $options: "is"
                }
            }
        ]
    }


    const totalCount = await this.find(param).countDocuments();
    const searchedList = await this.find(param).sort({"title.korean": 1}).skip(itemsPerPage*(pageToSearch-1)).limit(itemsPerPage);

    return [totalCount, itemsPerPage, pageToSearch, searchedList];
}


/**
 * Find comics data with title index character
 * Search korean title only
 * */
ComicsSchema.statics.findAllComicsDataMatchTitleIndexChar = async function (textToSearch="", paging=true, itemsPerPage=COMICS_SEARCH_ITEMS_PER_PAGE, pageToSearch=1) {
    const numbers = [...textToSearch].filter(char => /[0-9]/g.test(char));
    const englishChars = [...textToSearch].filter(char => /[a-zA-Z]/g.test(char));
    const korChars = [...textToSearch].filter(char => /[ㄱ-ㅎ]/g.test(char));

    const isNumbers = !_.isEmpty(numbers);
    const isEngChars = !_.isEmpty(englishChars);
    const isKorChars = !_.isEmpty(korChars);

    const searchAll = !isNumbers && !isEngChars && !isKorChars;


    let pattern = "^[";
    let korPattern = "^[";

    if (isNumbers) {
        numbers.forEach(number => pattern += number);
    }

    if (isEngChars) {
        englishChars.forEach(char => pattern += char);
    }

    if (isKorChars) {
        korChars.forEach(char => {
            const index = COMICS_SEARCH_KOR_CHAR_INDEXES.find(index => index.key===char);

            if (index) korPattern += index.pattern;
        });
    }

    pattern += "]";
    korPattern += "]";



    const alphaNumericParam = {
        // "title.origin": {
        "title.korean": {
            $regex: pattern,
            $options: "is"
        }
    }
    const korParam = {
        "title.korean": {
            $regex: korPattern,
            $options: "is"
        }
    }


    let param;

    if (searchAll) {
        param = {};
    }
    else if ((isNumbers && isKorChars) || (isEngChars && isKorChars)) {
        param = {$or: [alphaNumericParam, korParam]};
    }
    else if (isNumbers || isEngChars) {
        param = alphaNumericParam;
    }
    else if (isKorChars) {
        param = korParam;
    }



    const totalCount = await this.find(param).countDocuments();
    let searchedList;

    if (!paging) {
        searchedList = await this.find(param).sort({"title.korean": 1});
    } else {
        pageToSearch = Number(pageToSearch);

        searchedList = await this.find(param).sort({"title.korean": 1}).skip(itemsPerPage*(pageToSearch-1)).limit(itemsPerPage);
    }

    return [totalCount, itemsPerPage, pageToSearch, searchedList];

}



/**
 * Create comics data
 * */
ComicsSchema.statics.createComicsData = async function (number, dataId, title, registeredUser) {
    return await this.create({
        number: number,
        id: dataId,
        title: {
            korean: title.korean,
            origin: title.origin
        },
        status: COMICS_STATUS.AWAITING_APPROVAL,
        registeredUser: registeredUser,
        time: {
            created: time.now()
        }
    })
}



/**
 * Update comics writer info
 * */
ComicsSchema.statics.updateComicsWriters = async function (comicsDataId, list) {
    await this.updateOne(
      {_id: parseDataId(comicsDataId)},
      {writers: list, "time.updated": time.now()}
      );
}


/**
 * Update comics artist info
 * */
ComicsSchema.statics.updateComicsArtists = async function (comicsDataId, list) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        artists: list,
        "time.updated": time.now()
    });
}


/**
 * Update comics publisher info
 * */
ComicsSchema.statics.updateComicsPublisher = async function (comicsDataId, comicsPublisherDataId) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        publisher: comicsPublisherDataId,
        "time.updated": time.now()
    });
}


/**
 * Update Comics Genre info
 * */
ComicsSchema.statics.updateComicsGenres = async function (comicsDataId, list) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        genres: list,
        "time.updated": time.now()
    });
}


/**
 * Update Comics Content Descriptor info
 * */
ComicsSchema.statics.updateComicsContentDescriptors = async function (comicsDataId, list) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        contentDescriptors: list,
        "time.updated": time.now()
    });
}


/**
 * Update Comics cover images info
 * */
ComicsSchema.statics.updateComicsCoverImageInfo = async function (comicsDataId, smallCoverImgId, mediumCoverImgId, largeCoverImgId, xLargeCoverImgId) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        "coverImage.small": smallCoverImgId,
        "coverImage.medium": mediumCoverImgId,
        "coverImage.large": largeCoverImgId,
        "coverImage.xLarge": xLargeCoverImgId,
        "time.updated": time.now()
    });
}


/**
 * Update comics summary info
 * */
ComicsSchema.statics.updateComicsSummary = async function (comicsDataId, summary) {
    await this.updateOne({_id: parseDataId(comicsDataId)}, {
        summary: summary,
        "time.updated": time.now()
    });
}




module.exports = mongoose.model('comics', ComicsSchema);

