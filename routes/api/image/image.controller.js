const fs = require('fs');
const path = require("path");
const {httpError, httpRespond} = require("../../../utils/http/response");
const PostCollection = require("../../../models/post");
const ImageCollection = require("../../../models/image");
const time = require("../../../utils/time");
const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {POST_STATUS} = require("../../../utils/index/post");
const {IMAGE_TYPE} = require("../../../utils/index/image");
const {writeCommonImgFile, generateCommonImgId} = require("../../../utils/image");



/**
 * Upload post image
 * */
exports.uploadPostImage = async (req, res) => {
  const {postId: postOid, category, board, width: imgWidth, height: imgHeight} = req.body;
  const imgFile = req.file;

  const userData = req.userData;


  if (
      !paramCheck(postOid) ||
      !paramCheck(category, paramType.String) ||
      !paramCheck(board, paramType.String) ||
      !imgFile
  ) return httpError(res, 400);



  // Find current working post
  const postData = await PostCollection.findPostWithPostOid(postOid, category, board);

  // Working post has been deleted
  if (!postData) return httpError(res, 404);

  const {status: postStatus} = postData;

  if (
      !(postStatus===POST_STATUS.PUBLISH ||postStatus===POST_STATUS.WRITING)
  ) return httpError(res, 403);


  /**
   * Write image file
   * */
  const {fileDir, fileName, fileExt, mimeType} = await writeCommonImgFile(imgFile, IMAGE_TYPE.POST);


  /**
   * Save the file data to ImageCollection
   * */
  const latestImageData = await ImageCollection.findLatestImage();
  const newImageNumber = !latestImageData? 0: latestImageData.number+1;

  const imgId = generateCommonImgId(newImageNumber, IMAGE_TYPE.POST, category, board, fileExt);

  await ImageCollection.createImage(
      imgId,
      newImageNumber,
      IMAGE_TYPE.POST,
      userData.id,
      req.clientIp,
      fileDir,
      fileName,
      mimeType,
      imgWidth,
      imgHeight
  )


  /**
   * Register image id to the post data
   * */
  await postData.addImage(imgId);


  return httpRespond(res, 200, {imageId: imgId});
}

