const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const ComicsArchiveLogCollection = require("../../../models/comicsArchiveLog");
const ComicsPublisherCollection = require("../../../models/comicsPublisher");
const ComicsCollection = require("../../../models/comics");
const _ = require("underscore");
const mongoose = require("mongoose");



/**
 * Find all comics publishers data
 * Regex match word
 * */
exports.findComicsPublishers = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {textToSearch} = req.query;

    if (!paramCheck(textToSearch)) return httpError(res, 400);


    const publisherList = await ComicsPublisherCollection.findAllComicsPublishersMatchWord(textToSearch);

    if (_.isEmpty(publisherList)) return httpError(res, 404);


    return httpRespond(res, 200, {publisherList: publisherList});
}


/**
 * Update comics publisher info
 * */
exports.updateComicsPublisher = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, comicsPublisherDataId} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(comicsPublisherDataId)
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    const comicsPublisher = comicsData.publisher;
    const comicsPublisherToReplace = mongoose.Types.ObjectId(comicsPublisherDataId);

    if (comicsPublisher===comicsPublisherToReplace) return httpError(res, 400);

    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);


    await ComicsCollection.updateComicsPublisher(comicsDataId, comicsPublisherToReplace);


    return httpRespond(res, 200);
}


/**
 * Update new comics publisher info
 * */
exports.addNewComicsPublisher = async (req, res) => {
    if (!req.isRegUser) return httpError(res, 403);

    const {comicsDataId, publisherName} = req.body;

    if (
        !paramCheck(comicsDataId) ||
        !paramCheck(publisherName) ||
        (publisherName && !/[\w]/g.test(publisherName))
    ) return httpError(res, 400);


    const comicsData = await ComicsCollection.findComicsDataWithDataId(comicsDataId);

    if (!comicsData) return httpError(res, 404);


    /**
     * Archive data
     * */
    await ComicsArchiveLogCollection.archiveComicsData(req.userData.id, comicsData);

    const newComicsPublisherData = await ComicsPublisherCollection.createComicsPublisherData(publisherName);


    await ComicsCollection.updateComicsPublisher(comicsDataId, newComicsPublisherData._id);


    return httpRespond(res, 200);
}