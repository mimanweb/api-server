const express = require('express');
const router = express.Router();
const user = require('./user.controller');
const notification = require('./notification.controller');


const {verifyLogin} = require('../../../utils/auth/login');


router.post('/register', user.register);
router.delete('/', verifyLogin,  user.unregister);
router.get('/check/duplicate', user.checkDuplicate);

router.get("/id", user.findUserId);
router.put("/password", user.changeUserPassword);
router.put("/info", verifyLogin, user.modifyUserInfo);



router.get("/notification/list", verifyLogin, notification.findAllUserNotifications);
router.put("/notification/checked", verifyLogin, notification.updateUserNotificationDataAsChecked);
router.put("/notification/all/checked", verifyLogin, notification.updateUserNotificationDataAsChecked);



module.exports = router;