exports.SITE_CATEGORY_INDEX = {
  COMMUNITY: {
    code: "community",
    imgId: "cm"
  },
  COMICS: {
    code: "comics",
    imgId: "co"
  },
  ANIMATION: {
    code: "animation",
    imgId: "an"
  }
}


exports.SITE_BOARD_INDEX = {
  COMMUNITY: {
    NOTICE: {
      code: "notice",
      imgId: "no"
    },
    FREEBOARD: {
      code: "freeboard",
      imgId: "fr"
    },
    HUMOR: {
      code: "humor",
      imgId: "hu"
    },
    HORROR: {
      code: "horror",
      imgId: "hr"
    },
    FOOD: {
      code: "food",
      imgId: "fd"
    }
  },
  COMICS: {
    FREEBOARD: {
      code: "freeboard",
      imgId: "fr"
    },
    BOOK: {
      code: "book",
      imgId: "bk"
    }
  },
  ANIMATION: {
    FREEBOARD: {
      code: "freeboard",
      imgId: "fr"
    }
  }
}