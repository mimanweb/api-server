const {paramCheck, paramType} = require("../../../utils/paramUtil");
const {httpRespond, httpError} = require("../../../utils/http/response");
const UserCollection = require("../../../models/user");
const PostCollection = require("../../../models/post");
const ImageCollection = require("../../../models/image");
const CommentCollection = require("../../../models/comment");
const CommentUpvoteCollection = require("../../../models/commentUpvote");
const NotificationCollection = require("../../../models/notification");
const {POST_STATUS} = require("../../../utils/index/post");
const {IMAGE_TYPE} = require("../../../utils/index/image");
const {COMMENT_PER_PAGE_COUNT, COMMENT_TYPE, COMMENT_STATUS, formatBestCommentList, formatCommentList} = require("../../../utils/index/comment");
const time = require("../../../utils/time");
const {NOTIFICATION_TYPE} = require("../../../utils/index/notification");
const {writeCommonImgFile, generateCommonImgId} = require("../../../utils/image");






/**
 * Find comments in specific page
 * */
exports.findCommentsInSpecificPage = async (req, res) => {
    const userData = req.userData;
    const ipAddress = req.clientIp;
    const userId = userData.id;

    const {postNumber, pageToSearch} = req.query;


    if (
      !paramCheck(postNumber, paramType.Number) ||
      !paramCheck(pageToSearch, paramType.Number)
    ) return httpError(res, 400);


    /**
     * Find post comment data and comment paging info
     * */
    const commentAndPagingInfo = await CommentCollection.findCommentListAndPagingInfo(postNumber, pageToSearch, undefined, COMMENT_PER_PAGE_COUNT);


    const {bestComments, pageComments, totalCommentCount, totalPageCount, searchedPage} = commentAndPagingInfo;


    /**
     * Trim ip address and remove password, deviceUUID in comments
     * Fill best comment id info to comment data
     * Fill image url
     * */
    const upvotedUpvoteList = await CommentUpvoteCollection.findUpvotedListByPostAndUser(postNumber, userId, ipAddress);

    formatCommentList(req, pageComments, bestComments, upvotedUpvoteList);
    formatBestCommentList(bestComments, userId, ipAddress, upvotedUpvoteList);



    return httpRespond(res, 200, {
        itemsPerPageCount: COMMENT_PER_PAGE_COUNT,
        totalPageCount: totalPageCount,
        pageComments: pageComments,
        bestComments: bestComments,
        searchedPage: searchedPage,
        totalCommentCount: totalCommentCount,
    });
}


/**
 * Create Comment
 * */
exports.createComment = async (req, res) => {
    const {postNumber, commentNumber: repliedCommentNumber, category, board, youtubeHTML, text, nickname, password} = req.body;
    const imgFile = req.file;
    const userData = req.userData;
    const {id: userId} = userData;
    const ipAddress = req.clientIp;
    const deviceUUID = req.session.UUID;

    const isRegUser = req.isRegUser;
    const reqUserId = isRegUser? userData.id: null;
    const isReplyComment = req.route.path==="/reply";


    if (
        !paramCheck(postNumber, paramType.Number) ||
        !paramCheck(category) ||
        !paramCheck(board) ||
        (!paramCheck(text) && !paramCheck(youtubeHTML) && !imgFile) ||
        (isReplyComment && !paramCheck(repliedCommentNumber, paramType.Number))
    ) return httpError(res, 400);


    // Assume reg user requested after login info is expired
    if (
        (!isRegUser && (!nickname || !password))
    ) return httpError(res, 403);



    const postData = await PostCollection.findPostWithPostNumber(postNumber, undefined, undefined, POST_STATUS.PUBLISH);

    if (!postData) return httpError(res, 404);


    /**
     * Got image file
     * Write the file
     * */
    let imgId;

    if (imgFile) {
        const {fileName, fileDir, fileExt, mimeType} = await writeCommonImgFile(imgFile, IMAGE_TYPE.COMMENT);

        const latestImageData = await ImageCollection.findLatestImage();
        const newImageNumber = !latestImageData? 0: latestImageData.number+1;

        imgId = generateCommonImgId(newImageNumber, IMAGE_TYPE.COMMENT, category, board, fileExt);

        await ImageCollection.createImage(
            imgId,
            newImageNumber,
            IMAGE_TYPE.COMMENT,
            userData.id,
            req.clientIp,
            fileDir,
            fileName,
            mimeType,
            undefined,
            undefined
        )
    }



    const latestCommentData = await CommentCollection.findLatestComment(postNumber);
    const newCommentNumber = latestCommentData? latestCommentData.number+1: 1;



    let commentData
    /**
     * Writing post-comment requested
     * */
    if (!isReplyComment) {
        commentData = await CommentCollection.createComment(
            newCommentNumber,
            userId,
            isRegUser? userData.nickname: nickname,
            isRegUser? undefined: password,
            ipAddress,
            deviceUUID,
            imgId,
            youtubeHTML,
            text,
            postNumber
        );
    }
    /**
     * Writing comment-reply-comment requested
     * */
    else {
        commentData = await CommentCollection.createReplyComment(
            newCommentNumber,
            userId,
            isRegUser? userData.nickname: nickname,
            isRegUser? undefined: password,
            ipAddress,
            deviceUUID,
            imgId,
            youtubeHTML,
            text,
            postNumber,
            repliedCommentNumber
        );
    }



    /**
     * Increase belong post's total comment count
     * */
    await postData.increaseCommentCount();



    const {number: commentNumber, belongPostNumber} = commentData;

    /**
     * Find post comment data and comment paging info
     * */
    const commentAndPagingInfo = await CommentCollection.findCommentListAndPagingInfo(postNumber, undefined, commentNumber, COMMENT_PER_PAGE_COUNT);

    const {bestComments, pageComments, totalCommentCount, totalPageCount, searchedPage} = commentAndPagingInfo;


    /**
     * Trim ip address and remove password, deviceUUID in comments
     * Fill best comment id info to comment data
     * Fill image url
     * */
    const upvotedUpvoteList = await CommentUpvoteCollection.findUpvotedListByPostAndUser(postNumber, userId, ipAddress);

    formatCommentList(req, pageComments, bestComments, upvotedUpvoteList);
    formatBestCommentList(bestComments, userId, ipAddress, upvotedUpvoteList);



    /**
     * Create notification data
     * Change user unread notification status
     * */
    // Comment on post
    if (!isReplyComment) {
        const isRegUserPost = postData.owner.userId;
        const postUserId = isRegUserPost? postData.owner.userId: null;
        const isReqUserOwnPost = isRegUserPost && isRegUser && postUserId===reqUserId;


        if (isRegUserPost && !isReqUserOwnPost) {
            await NotificationCollection.createNotificationData(NOTIFICATION_TYPE.POST.COMMENT, postUserId, commentData._id);
            await UserCollection.setUserUnreadNotificationStatus(postUserId, true);
        }
    }
    // Comment on comment
    else {
        const repliedCommentData = await CommentCollection.findComment(postNumber, repliedCommentNumber);
        const isRegUserRepliedComment = repliedCommentData.owner.userId;
        const repliedCommentUserId = isRegUserRepliedComment? repliedCommentData.owner.userId: null;
        const isReqUserOwnRepliedComment = isRegUserRepliedComment && isRegUser && repliedCommentUserId===reqUserId;

        if (isRegUserRepliedComment && !isReqUserOwnRepliedComment) {
            await NotificationCollection.createNotificationData(NOTIFICATION_TYPE.COMMENT.COMMENT, repliedCommentUserId, commentData._id);
            await UserCollection.setUserUnreadNotificationStatus(repliedCommentUserId, true);
        }
    }




    return httpRespond(res, 200, {
        itemsPerPageCount: COMMENT_PER_PAGE_COUNT,
        totalPageCount: totalPageCount,
        pageComments: pageComments,
        bestComments: bestComments,
        searchedPage: searchedPage,
        totalCommentCount: totalCommentCount,
    });
}


/**
 * Delete comment
 * */
exports.deleteComment = async (req, res) => {
    const {postNumber, commentNumber, password} = req.body;

    const isRegUser = req.isRegUser;
    const {id: userId} = req.userData;
    const ipAddress = req.clientIp;


    if (
        !paramCheck(postNumber, paramType.Number) ||
        !paramCheck(commentNumber, paramType.Number)
    ) return httpError(res, 400);


    const postData = await PostCollection.findPostWithPostNumber(postNumber, undefined, undefined, POST_STATUS.PUBLISH);


    const commentData = await CommentCollection.findComment(postNumber, commentNumber);


    if (
      !postData ||
      !commentData
    ) return httpError(res, 404);


    /**
     * Check if valid owner/password attempting to delete
     * */
    const {userId: commentUserId, password: commentPassword} = commentData.owner;
    const isRegUserComment = commentUserId;

    if (
        (isRegUserComment && (!isRegUser || userId!==commentUserId)) ||
        (!isRegUserComment && (!password || password!==commentPassword))
    ) return httpError(res, 403);


    await commentData.deleteCommentData(COMMENT_STATUS.OWNER_DELETED);


    /**
     * decrease belong post's total comment count
     * */
    await postData.decreaseCommentCount();


    /**
     * Find post comment data and comment paging info
     * */
    const commentListAndPagingInfo = await CommentCollection.findCommentListAndPagingInfo(postNumber, 1, undefined, COMMENT_PER_PAGE_COUNT);

    const {bestComments, pageComments, totalCommentCount, totalPageCount, searchedPage} = commentListAndPagingInfo;


    /**
     * Trim ip address and remove password, deviceUUID in comments
     * Fill best comment id info to comment data
     * Fill image url
     * */
    const upvotedUpvoteList = await CommentUpvoteCollection.findUpvotedListByPostAndUser(postNumber, userId, ipAddress);

    formatCommentList(req, pageComments, bestComments, upvotedUpvoteList);
    formatBestCommentList(bestComments, userId, ipAddress, upvotedUpvoteList);


    return httpRespond(res, 200, {
        itemsPerPageCount: COMMENT_PER_PAGE_COUNT,
        totalPageCount: totalPageCount,
        pageComments: pageComments,
        bestComments: bestComments,
        searchedPage: searchedPage,
        totalCommentCount: totalCommentCount,
    });
}