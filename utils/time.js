const moment = require("moment");

const SEOUL_UTC_OFFSET = 9;


exports.now = () => {
  return moment();
}

exports.todayStart = () => {
  const time = this.now();
  const utcOffsetHour = time.utcOffset()/60;

  time.set({hour: 0, minute: 0, second: 0, millisecond: 0})
  time.subtract(SEOUL_UTC_OFFSET-utcOffsetHour, "hour")

  return time;
}

exports.getYear = () => {
  return this.now().year();
}

exports.getMonthDate = () => {
  const month = this.now().month()+1;
  const date = this.now().date();

  return (month<10? `0${month}`: month) + "-" + (date<10? `0${date}`: date);
}

exports.getTimestamp = () => {
  return this.now().valueOf();
}


exports.getInitHourTime = (time) => {
  time = moment(time);
  time.startOf("hour")

  return time;
}


exports.getHourSubtractedTime = (time, hour) => {
  time = moment(time);

  return time.subtract(hour, "hours");
}


exports.compare = (a, b, includeSame) => {
  a = moment(a);
  b = moment(b);

  return includeSame? a.isSameOrAfter(b): a.isAfter(b);
}


exports.checkIsBeforeTodayTime = (time) => {
  const todayStart = this.todayStart();

  return todayStart.isAfter(moment(time));
}