const cheerio = require('cheerio')
const _ = require("underscore");




exports.validateEditorContent = (content) => {
  const $ = cheerio.load(content);

  const allowedEls = [
    "p", "img", "iframe", "span", "br", "b", "i", "font", "a"
  ]

  const p_list = $("p").toArray();

  const checkInvalidElInList = (list) => {
    return list.some(el => {
      const isText = el.type==="text";

      if (!isText) {
        const hasChild = el.children.length>0;

        if (!allowedEls.includes(el.name)) return true;

        if (hasChild) return checkInvalidElInList(el.children);
      }
    })
  }


  const img_list = $("img").toArray();
  const imgServerUrl = process.env["IMG_SERVER_URL"];


  const checkInvalidImg = (img) => {
    const src = img.attribs.src;

    if (!src || (src && src.substr(0, imgServerUrl.length)!==imgServerUrl)) {
      return true
    }
  }


  const isAllElTypeValid = !checkInvalidElInList(p_list);
  const isAllImgSrcValid = process.env.SERVER_ENV!=="production"? true: !img_list.some(img => checkInvalidImg(img));


  return isAllElTypeValid && isAllImgSrcValid;
}


exports.checkIframeInEditorContent = (content) => {
  const $ = cheerio.load(content);

  return !_.isEmpty($("iframe").toArray());
}


exports.findImgIdListFromEditorContent = (content) => {
  const $ = cheerio.load(content);
  const img_list = $("img").toArray();
  const imgServerUrl = process.env["IMG_SERVER_URL"];

  const validImgList = img_list.filter(el => {
    const src = el.attribs.src;

    return src.substring(0, imgServerUrl.length)===imgServerUrl;
  });

  return validImgList.map(el => {
    const src = el.attribs.src;
    // const split = src.split("/");
    //
    // const index = split[split.length-2];
    // const id = split[split.length-1];
    //
    // return `${index}/${id}`;

    return src.substring((imgServerUrl+"/").length);
  })
}


exports.findTextContentFromHTML = (html) => {
  if (!html) return null;
  else {
    const $ = cheerio.load(html);

    const textContent = $.text();

    return !/[\S]/g.test(textContent)? null: textContent;
  }
}


