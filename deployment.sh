#!/bin/sh

BLUE_CONTAINER_NAME="blue_dev-api-server"
BLUE_SERVICE_NAME="blue_api-server"
GREEN_SERVICE_NAME="green_api-server"
PROXY_SERVICE_NAME="proxy"
NETWORK="miman_api-network"

# Create shared [bridge] network for proxy server and api servers
docker network create -d bridge $NETWORK

# Blue server up
docker-compose up -d --build $BLUE_SERVICE_NAME

# Wait for checking Blue server status
# Checking if the container up and running
# Interval 10 every 3s
i=0

while [ $i -lt 10 ]; do
  ((i++))
  if [ "$( docker container inspect -f '{{.State.Status}}' $BLUE_CONTAINER_NAME )" == "running" ]; then
    echo "container $BLUE_CONTAINER_NAME confirmed running"
    break
  fi
  echo "wait 2s for $BLUE_CONTAINER_NAME responds"
  sleep 3s
done

# Green server up
docker-compose up -d --build $GREEN_SERVICE_NAME

# Proxy server up
docker-compose up -d --build $PROXY_SERVICE_NAME