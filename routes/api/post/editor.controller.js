const axios = require("axios");
const {httpError, httpRespond} = require("../../../utils/http/response");
const cheerio = require('cheerio')
const _ = require("underscore");
const {paramType, paramCheck} = require("../../../utils/paramUtil");



exports.linkTitle = async (req, res) => {
    const {url} = req.body;

    if (
        !paramCheck(url)
    ) return httpError(res, 400);

    try {

        const {data: html} = await axios.get(url);

        if (!html) return httpError(res, 403);


        const $ = cheerio.load(html);

        const title_list = $("title").toArray();

        const title = title_list[0].children[0].data;

        return httpRespond(res, 200, {title: title});
    } catch (e) {
        return httpError(res, 404);
    }

}