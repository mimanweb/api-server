const path = require("path");
const {writeFile} = require("../file");
const {resizeImgBuffer} = require("../image");
const {getLastNumberOfNumItemsInDir} = require("../file");
const {getComicsFolderDir} = require("./comics");
const {mv} = require("../file");
const {mkdirIfNone} = require("../file");
const {readDir} = require("../file");



const MAX_COMIC_BOOK_PAGE_FOLDER_ITEMS_COUNT = 1000;


const COMIC_BOOK_PAGE_TYPE = {
  SINGLE: "SINGLE",
  WIDE: "WIDE",
  PAIRED: "PAIRED"
}


const COMIC_BOOK_STATUS = {
  WRITING: "WRITING",
  PUBLISH: "PUBLISH",
  DELETED: "DELETED",
  BLOCKED: "BLOCKED",
  EDITING: "EDITING",
}

const COMIC_BOOK_IMAGE_STATUS = {
  WRITING: "WRITING",
  PUBLISH: "PUBLISH",
  DELETED: "DELETED",
}

const COMIC_BOOK_CONTENT_TYPE = {
  LINK: "LINK",
  IMAGE: "IMAGE"
}



/**
 * DIRECTORIES
 * */

const getComicBookFolderDir = () => {
  return path.join(getComicsFolderDir(), process.env["COMIC_BOOK_FOLDER"]);
}


const getComicBookPageFolderDir = () => {
  return path.join(getComicBookFolderDir(), process.env["COMIC_BOOK_PAGE_FOLDER"]);
}


const getLastComicBookPageFolderNumber = async () => {
  const bookFolderDir = getComicBookFolderDir();
  const pageFolderDir = getComicBookPageFolderDir();

  await mkdirIfNone(bookFolderDir);
  await mkdirIfNone(pageFolderDir);

  return await getLastNumberOfNumItemsInDir(pageFolderDir);
}


const getAvailableComicBookPageFolder = async () => {
  const pageFolderDir = getComicBookPageFolderDir();
  const lastPageFolder = await getLastComicBookPageFolderNumber();


  /**
   * No folder yet
   * Create one
   * */
  if (lastPageFolder===0) {
    const newFolder = (lastPageFolder+1).toString();
    await mkdirIfNone(path.join(pageFolderDir, newFolder));

    return [newFolder, 0];
  } else {
    const lastNumberInFolder = await getLastNumberOfNumItemsInDir(path.join(pageFolderDir, lastPageFolder.toString()), true);

    if (lastNumberInFolder<MAX_COMIC_BOOK_PAGE_FOLDER_ITEMS_COUNT) {
      return [lastPageFolder.toString(), lastNumberInFolder];
    }
    /**
     * Max item count exceeded
     * Create new folder
     * */
    else {
      const newFolder = (lastPageFolder+1).toString();
      await mkdirIfNone(path.join(pageFolderDir, newFolder));

      return [newFolder, 0];
    }
  }
}


const saveComicBookPageFile = async (file, size) => {
  const coverFolderDir = getComicBookPageFolderDir();
  const [folder, lastNumberInFolder] = await getAvailableComicBookPageFolder();
  const fileExt = file.mimetype.split("/").pop();
  const resizedBuffer = await resizeImgBuffer(file.buffer, size);

  const fileName = `${lastNumberInFolder+1}.${fileExt}`;

  await writeFile(path.join(coverFolderDir, folder, fileName), resizedBuffer);

  return [folder, fileName];
}



exports.COMIC_BOOK_PAGE_TYPE = COMIC_BOOK_PAGE_TYPE;
exports.COMIC_BOOK_STATUS = COMIC_BOOK_STATUS;
exports.COMIC_BOOK_CONTENT_TYPE = COMIC_BOOK_CONTENT_TYPE;
exports.COMIC_BOOK_IMAGE_STATUS = COMIC_BOOK_IMAGE_STATUS;

exports.getComicsFolderDir = getComicsFolderDir;
exports.getComicBookPageFolderDir = getComicBookPageFolderDir;
exports.saveComicBookPageFile = saveComicBookPageFile;