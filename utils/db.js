const mongoose = require('mongoose');

exports.connect = () => {
    mongoose.connect(process.env["DB_CONNECTION_URL"], {useNewUrlParser: true, useUnifiedTopology: true});
    mongoose.set('useCreateIndex', true);

    const db = mongoose.connection;

    db.on('error', console.error.bind(console, 'mongoose connection error:'));
    db.once('open', function() {
        console.info("Successfully established connection to the database")
    });

};