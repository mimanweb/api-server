const bcrypt = require('bcrypt');
const { httpRespond, httpError } = require('../../../utils/http/response');
const { issueLoginAccessToken, issueLoginRefreshToken } = require('../../../utils/auth/token');
const User = require('../../../models/user');
const {USER_STATE} = require("../../../utils/index/user");


/**
 * User login
 *
 * @param {string} req.body.id User id
 * @param {string} req.body.password User password
 *
 * @return 200 Login success
 *             Issue and send access, refresh tokens in Cookie
 *
 * @throws 403 000 Wrong password
 * @throws 404 000 No user matches the id
 * @throws 500
 * */
exports.login = async (req, res, next) => {
    const authHeader = req.headers['authorization']

    if (!authHeader) return httpError(res, 401, '000');

    const loginParamsStr = authHeader.substr("Bearer ".length)
    const loginParams = JSON.parse(loginParamsStr)

    const { id, password, stayLoggedIn } = loginParams;

    // Parameters not given
    if (!id || !password) return httpError(res, 400, '000', "Parameter not given");

    let userData;
    try {
        userData = await User.findUserById(id);
    } catch (e) {
        // DB error
        return httpError(res, 500, '000', "DB Error");
    }


    // User not found
    if (!userData) return httpError(res, 404, '000', "No user found with given id");

    // Verify password
    const match = await bcrypt.compare(password, userData.password.value);

    // Wrong password
    if (!match) return httpError(res, 403, '000', "Wrong password");

    /**
     * Unregistered user account
     * */
    if (userData.status===USER_STATE.DELETED) return httpError(res, 403, '000', "Unregistered user");

    const loginTokenKey = userData.login.tokenKey || await bcrypt.genSalt(5);
    const accessToken = await issueLoginAccessToken(userData.id, loginTokenKey);

    let refreshToken;
    if (stayLoggedIn) {
        refreshToken = await issueLoginRefreshToken(userData.id, loginTokenKey);
    }

    // Failed to issue login tokens for any reason
    if (!accessToken) return httpError(res, 500, '001');
    if (stayLoggedIn && !refreshToken) return httpError(res, 500, '001');

    // Check if the user has ever been logged in
    // by checking a previously issued login token salt(hash) is stored in the user data,
    // And issue one if not
    if (!userData.login.tokenKey) {
        try {
            await userData.registerLoginTokenKey(loginTokenKey);
        } catch (e) {
            return httpError(res, 500, '002');
        }
    }

    // Successfully issued both access, refresh tokens for the user login process
    // Sending through in Cookies
    req.session.ACCESS_TOKEN = accessToken;
    if (stayLoggedIn) req.session.REFRESH_TOKEN = refreshToken;

    return httpRespond(res, 200, {userId: userData.id});
};


/**
 * User logout
 * */
exports.logout = async (req, res, next) => {
    req.session.ACCESS_TOKEN = null;
    req.session.REFRESH_TOKEN = null;
    return res.end();
};


/**
 * Verify login and send user data along
 * */
exports.verify = async (req, res) => {

    if (!req.isRegUser) return httpError(res, 403);

    return httpRespond(res, 200, {user: req.isRegUser? req.userData: null});
};